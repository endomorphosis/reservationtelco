		<div class="row-2-col-1-row-1">
			<a class="voice <?php if ( is_page('voice'))  { echo 'current_page_item'; } ?>" href="<?php bloginfo('url'); ?>/voice">Voice</a>
			<a class="data <?php if ( is_page('data'))  { echo 'current_page_item'; } ?>" href="<?php bloginfo('url'); ?>/data">Data</a>
			<a class="video <?php if ( is_page('video'))  { echo 'current_page_item'; } ?>" href="<?php bloginfo('url'); ?>/video">Video</a>
			<a class="bundle <?php if ( is_page('bundle'))  { echo 'current_page_item'; } ?>" href="<?php bloginfo('url'); ?>/bundle">Bundle</a>
		</div>

		<?php if(is_search() || is_404()) { } else {
		
			if($post->post_parent) {
				$children = wp_list_pages("title_li=&child_of=".$post->post_parent."&echo=0");
			} else {
				$children = wp_list_pages("title_li=&child_of=".$post->ID."&echo=0");
			}

			if ($children) {
			echo '
				<hr />
		
				<div class="row-2-col-1-row-2">
					<h1>' . get_the_title($post->post_parent) . '</h1>
			';
			
				if ($post->post_parent == 0) {
					echo wp_list_pages('child_of=' . $post->ID . '&sort_column=post_title&title_li=');
				} else {
				 	echo wp_list_pages('child_of=' . $post->post_parent . '&sort_column=post_title&title_li=');
				}
			
			echo '</div>';
			
			}
		}
		?>