<?php get_header(); ?>

<div class="page search">



<!-- BEGINNIG OF ROW-2 -->
<!-- BEGINNIG OF ROW-2 -->
<!-- BEGINNIG OF ROW-2 -->
<div class="row-2">

	<div class="row-2-row-2">



	<!-- BEGINNING OF ROW-2-COL-1 -->
	<!-- BEGINNING OF ROW-2-COL-1 -->
	<div class="row-2-col-1">
		<?php get_sidebar(); ?>
	</div>



	<!-- BEGINNING OF ROW-2-COL-2 -->
	<!-- BEGINNING OF ROW-2-COL-2 -->
	<div class="row-2-col-2">
	
		
		<!-- BEGINNING OF ROW-2-COL-2-ROW-1 -->
		<div class="row-2-col-2-row-1"><img src="<?php bloginfo('template_directory'); ?>/images/search-row-2-col-2-background-top.png" alt="search-row-2-col-2-background-top" width="760" height="202" /></div>
	
		
		<!-- BEGINNING OF ROW-2-COL-2-ROW-2 -->
		<div class="row-2-col-2-row-2">
		
		<?php
		if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6.0') !== FALSE) {
			echo '<p>The browser you are using to view this website is <b><i>very</i></b> out of date. Please update your computer briefly by selecting "Start", then "All Programs", then "Windows Update" from the top of the list and following the on screen instructions.</p>';
		}
		?>
			
	<?php if (have_posts()) : ?>

		<h1>Search Results</h1>
		
		<div class="found"><span class="found-text">Found:</span> <i><?php $allsearch = new WP_Query("s=$s&showposts=-1"); $count = $allsearch->post_count; echo $count . ' '; wp_reset_query(); ?></i> matches for &quot;<i><?php $key = wp_specialchars($s, 1); echo $key; ?></i>&quot;</h2>
</div>

		<?php get_search_form(); ?>
		
		<div class="navigation">
			<div class="older-entries"><?php next_posts_link('&laquo; Older Entries') ?></div>
			<div class="newer-entries"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
		</div>
		
		<div class="the-content">
		<?php while (have_posts()) : the_post(); ?>

			<h3><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
			
			<?php the_excerpt() ?>
			
			<hr />
			
		<?php endwhile; ?>
		</div>

		<div class="navigation">
			<div class="older-entries"><?php next_posts_link('&laquo; Older Entries') ?></div>
			<div class="newer-entries"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
		</div>

	<?php else : ?>

		<h1>Search Results</h1>

		<?php get_search_form(); ?>
		
		<br />
		<br />

		<h2>We're sorry, but your search didn't return any results.</h2>

	<?php endif; ?>

		</div>
	
		
		<!-- BEGINNING OF ROW-2-COL-2-ROW-3 -->
		<div class="row-2-col-2-row-3"></div>
	</div>
	</div>

</div>




<!-- BEGINNIG OF ROW-3 -->
<!-- BEGINNIG OF ROW-3 -->
<!-- BEGINNIG OF ROW-3 -->
<div class="row-3"></div>



</div>
<?php get_footer(); ?>