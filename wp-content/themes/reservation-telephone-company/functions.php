<?php
// This is a functions file where custom functions can be written and stored. It acts as a plugin and is automatically loaded by Wordpress if it exists.

$site_url = "http://" . $_SERVER['SERVER_NAME'];

if ( function_exists('register_sidebar') )
register_sidebar();

function new_excerpt_more($post) {
	return '... <a href="'. get_permalink($post->ID) . '">' . 'Read the Rest >>' . '</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');

?>