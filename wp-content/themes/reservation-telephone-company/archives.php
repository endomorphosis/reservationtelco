<?php /* Template Name: Archives */ ?>
<?php get_header(); ?>

<div class="page">



<!-- BEGINNIG OF ROW-2 -->
<!-- BEGINNIG OF ROW-2 -->
<!-- BEGINNIG OF ROW-2 -->
<div class="row-2">

	<div class="row-2-row-2">



	<!-- BEGINNING OF ROW-2-COL-1 -->
	<!-- BEGINNING OF ROW-2-COL-1 -->
	<div class="row-2-col-1">
		<?php get_sidebar(); ?>
	</div>



	<!-- BEGINNING OF ROW-2-COL-2 -->
	<!-- BEGINNING OF ROW-2-COL-2 -->
	<div class="row-2-col-2">
	
		
		<!-- BEGINNING OF ROW-2-COL-2-ROW-1 -->
		<div class="row-2-col-2-row-1"><?php gs_page_banner(); ?></div>
	
		
		<!-- BEGINNING OF ROW-2-COL-2-ROW-2 -->
		<div class="row-2-col-2-row-2">
		<?php
		if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6.0') !== FALSE) {
			echo 'The browser you are using to view this website is <b><i>very</i></b> out of date. Please update your computer briefly by selecting "Start", then "All Programs", then "Windows Update" from the top of the list and following the on screen instructions.';
		}
		?>

<table>
	<tr valign="top">
		<td style="width: 400px;">
			<?php
			$posts_to_show = 100; //Max number of articles to display
			$debut = 0; //The first article to be displayed
			?>

			<?php while(have_posts()) : the_post(); ?>
				<h1><?php the_title(); ?></h1>
				<?php
				$myposts = get_posts('numberposts=$posts_to_show&offset=$debut');
				foreach($myposts as $post) :
				?>
				<p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
				<p><?php the_content(); ?></p>
				<p><small><?php the_time('M d, Y') ?></small></p>
			
				<br />

				<?php endforeach; ?>
			<?php endwhile; ?>
		</td>
		<td style="text-align: right;">
			<?php while(have_posts()) : the_post(); ?>

			<h1>Categories</h1>
			<ul><?php wp_list_cats('sort_column=name&optioncount=1') ?></ul>
			
			<br />
			
			<h1>Monthly Archives</h1>
			<ul><?php wp_get_archives('type=monthly&show_post_count=1') ?></ul>

			<?php endwhile; ?>
		</td>
	</tr>
</table>
		
		</div>
	
		
		<!-- BEGINNING OF ROW-2-COL-2-ROW-3 -->
		<div class="row-2-col-2-row-3"></div>
	</div>
	</div>

</div>




<!-- BEGINNIG OF ROW-3 -->
<!-- BEGINNIG OF ROW-3 -->
<!-- BEGINNIG OF ROW-3 -->
<div class="row-3"></div>



</div>
<?php get_footer(); ?>