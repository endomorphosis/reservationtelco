<?php get_header(); ?>

<div class="page">



<!-- BEGINNIG OF ROW-2 -->
<!-- BEGINNIG OF ROW-2 -->
<!-- BEGINNIG OF ROW-2 -->
<div class="row-2">

	<div class="row-2-row-2">



	<!-- BEGINNING OF ROW-2-COL-1 -->
	<!-- BEGINNING OF ROW-2-COL-1 -->
	<div class="row-2-col-1">
		<?php get_sidebar(); ?>
	</div>



	<!-- BEGINNING OF ROW-2-COL-2 -->
	<!-- BEGINNING OF ROW-2-COL-2 -->
	<div class="row-2-col-2">
	
		
		<!-- BEGINNING OF ROW-2-COL-2-ROW-1 -->
		<div class="row-2-col-2-row-1"><?php gs_page_banner(); ?></div>
	
		
		<!-- BEGINNING OF ROW-2-COL-2-ROW-2 -->
		<div class="row-2-col-2-row-2">
			<?php
			if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6.0') !== FALSE) {
				echo 'The browser you are using to view this website is <b><i>very</i></b> out of date. Please update your computer briefly by selecting "Start", then "All Programs", then "Windows Update" from the top of the list and following the on screen instructions.';
			}
			?>
			
			<h1><?php _e( 'Not Found (404 Error)', 'reservation-telephone-company' ); ?></h1>
			
			<?php get_search_form(); ?>
			
			<p><?php _e( 'Apologies, but the page you requested could not be found. Perhaps searching will help.', 'reservation-telephone-company' ); ?></p>
		</div>
	
		
		<!-- BEGINNING OF ROW-2-COL-2-ROW-3 -->
		<div class="row-2-col-2-row-3"></div>
	</div>
	</div>

</div>




<!-- BEGINNIG OF ROW-3 -->
<!-- BEGINNIG OF ROW-3 -->
<!-- BEGINNIG OF ROW-3 -->
<div class="row-3"></div>



</div>
<?php get_footer(); ?>