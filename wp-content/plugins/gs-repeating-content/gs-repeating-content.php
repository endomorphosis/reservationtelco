<?php

/*
Plugin Name: GoSeese Repeating Content
Plugin URI: http://www.goseese.com/
Description: A custom to allow admin users to configure content that can be repeated through to the site/theme by including a function call or shortcode. Depends on GoSeese Basic Functions
Version: alpha 1.0
Author: Jeff Seese
Author URI: http://www.goseese.com
License: Purchase only
*/

/*
 This plugin is not free and can only be used after it has been purchased from
 jeff@goseese.com
 
*/

/*
	==================================================================
		Constants
	==================================================================  */
	$this_plugin_name	= 'gs-presentation';
	if (!defined('PLUGIN_PATH')) { define('PLUGIN_PATH',dirname(__FILE__).'/../');}
	if (!defined('GS_PRE_PREFIX')) { global $wpdb; define('GS_PRE_PREFIX',$wpdb->prefix . "gsp_");}
	
/*
	==================================================================
		Enqued styles and scripts for the user facing pages.
	==================================================================  */
	wp_enqueue_style( $this_plugin_name, WP_PLUGIN_URL.'/'.$this_plugin_name.'/css/'.$this_plugin_name.'.css');
	wp_enqueue_script($this_plugin_name,WP_PLUGIN_URL.'/'.$this_plugin_name.'/js/'.$this_plugin_name.'.js', array('jquery'));

/*
	==================================================================
		includes.
	==================================================================  */
	include("includes/functions.php");					// function to interact with the class
	include("includes/GsRepeatingContent.php");			// main class		

	
/*
	==================================================================
		Admin Menu && resources
	==================================================================  */
if (is_admin())
	{
	wp_enqueue_style( $this_plugin_name.'-admin', WP_PLUGIN_URL.'/'.$this_plugin_name.'/css/'.$this_plugin_name.'-admin.css');
	wp_enqueue_script($this_plugin_name.'-admin',WP_PLUGIN_URL.'/'.$this_plugin_name.'/js/'.$this_plugin_name.'-admin.js', array('jquery'));
	
	}

function gs_repeating_content_admin() {
	add_submenu_page('tools.php', 'Repeating Content', 'Repeating Content',5, __file__,'show_gs_repeating_content_admin');
}	


/*
	==================================================================
		Actions && Hooks.
	==================================================================  */

add_action('admin_menu', 'gs_repeating_content_admin',11);


/*
	==================================================================
		Short Codes
	==================================================================  */

add_shortcode('gs-repeating-content', 'gs_repeating_content');

