<?php

	$gs_rpc	= new GsRepeatingContent();
	
	if (isset($_POST['do_gsrpc_content']))
		{
			$gs_rpc->update($_POST);
		}
	
	print gs_basic_que_tinymce();
	
?>
<div style ="display:none;">
		<br>view:<?php print __FILE__;?>
		<br>this is only to force the comment to dislpay. This div serves no purpose.
</div>

<div class="wrap">
<h2>Repeating content Settings</h2>
<p>To insert repeating content in your theme use this short code <strong>[gs-repeating-content]</strong> or this function call <strong>&lt;?php echo gs_repeating_content(); ?&gt;</strong></p>
	<form method = "post" name = "rpc_content_form" >
		<input type ="hidden" name = "do_gsrpc_content" value = "do_gsrpc_content">
		
		<select id = "gs_rpc_source" name = "gs_rpc_content_source" onChange = "show_gsrpc_content_source(this)"><?php echo gs_build_option_selection($gs_rpc->gs_rpc_content_source,$gs_rpc->content_sources); ?></select>
	
		<div class = "gs_rpc_content_sources text" style = "display: none;" ><textarea id = "gs_rpc_content" name = "gs_rpc_text_content" ><?php echo $gs_rpc->gs_rpc_text_content; ?></textarea>
		<?php echo gs_basic_tinymce('gs_rpc_content'); ?>
		</div>
	
		<div class = "gs_rpc_content_sources post" style = "display: none;"><?php echo gs_wp_post_selector($gs_rpc->gs_rpc_post_ID,"gs_rpc_post_ID"); ?></div>
	
		<div class = "gs_rpc_content_sources image" style = "display: none;">
			
			<div class = "gswpimage_add_image_trigger" id = "gs_rpc_image_preview" title = "click to change" style = "cursor: pointer; padding: 0; margin: 5px 0; border: 1px solid #CCC; float: left; clear: both; text-align: center;">
				<?php
					if (strlen($gs_rpc->gs_rpc_image_ID) < 1 )
						{?>
						<p style = "border: 1px solid black; padding: 10px; margin: 10px; text-align: center;">Click to configure image</p>
					<?php }
						else
						{
							echo $gs_rpc->gs_rpc_image_html;
						}
				?>
			</div>
			<input style = "float:left; clear: both;" type="hidden" id = "gs_rpc_image_ID_src" name="gs_rpc_image_ID" value="<?php echo $gs_rpc->gs_rpc_image_ID; ?>" />
		
		</div>
		
	</form>
</div><!-- end of wrap -->
<div class = "clear"></div>
<input type="button" class="button-primary" style = "margin-top: 10px;" value="<?php _e('Save Changes') ?>" onclick = "do_gsrpc_content();"/>
<script>

	jQuery(document).ready( function () {
		
		jQuery('#gs_rpc_source').trigger('change');
		
		init_gswpimage_image_uploader('Select an Image');
		jQuery('.gswpimage_add_image_trigger').bind('img_update', function (e,data) { update_gs_rpc_image(this,data);} );
		
		});
	
	function show_gsrpc_content_source(o)
		{
			jQuery('.gs_rpc_content_sources').css({'display' : 'none' });
			
			jQuery('.gs_rpc_content_sources.'+jQuery(o).val() ).css({'display' : 'block' });
		}
		
	function do_gsrpc_content()
		{
			tinyMCE.triggerSave();
			
			document.forms['rpc_content_form'].submit();
		}
		
		
	function update_gs_rpc_image(o,data)
		{
			if (typeof(data.wp_image) != 'undefined')
				{
					jQuery('#gs_rpc_image_ID_src').val(data.wp_image);
				}
			else
				{
					jQuery('#gs_rpc_image_ID_src').val(data.url);
				}
					
			jQuery('#gs_rpc_image_preview').html('<img src = "'+data.url+'" />');
				
		}
		
</script>

