<?php

/*
	==================================================================
		Functions to interact with the main class.
		
	==================================================================  */
	
	function show_gs_repeating_content_admin()
		{
			include(PLUGIN_PATH."gs-repeating-content/views/admin/repeating_content_configure.php");
		}
		
	function gs_repeating_content()
		{
			
			$gs_rpc	= new GsRepeatingContent();
			
			switch ($gs_rpc->gs_rpc_content_source)
				{
					case 'text':
						{
							return $gs_rpc->gs_rpc_text_content;
						}break;
					
					case 'post':
						{
							$rpc_post	= get_post($gs_rpc->gs_rpc_post_ID);
							return $rpc_post->post_content;
						}break;
						
					case 'image':
						{
							return $gs_rpc->gs_rpc_image_html;
						}
				}
		}