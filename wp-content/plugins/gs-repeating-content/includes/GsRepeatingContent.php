<?php

class GsRepeatingContent {
	
	public $content_sources =	array(
		'text'		=> 'WYSISYG editor',
		'post'		=> 'Post Body',
		'image'		=> 'Image'
	);
	
	private $_post_vars = array('gs_rpc_content_source','gs_rpc_text_content','gs_rpc_post_ID','gs_rpc_image_ID');
	
	/*
	==================================================================
			Main Function.
	==================================================================  */
	public function GsRepeatingContent()
		{
			foreach ($this->_post_vars as $option_name)
				{
					$this->$option_name 	= 	get_option($option_name);
				}
				
			$this->gs_rpc_image_html = $this->_get_image($this->gs_rpc_image_ID);
		}
		
		
	public function update($args)
		{
			foreach ($args as $option_name => $option_value)
				{
					if (!in_array($option_name, $this->_post_vars ) ) { continue; }
					
					update_option( $option_name, stripslashes($option_value) );
					
					$this->$option_name 	= stripslashes($option_value);
				}
				
			$this->gs_rpc_image_html = $this->_get_image($this->gs_rpc_image_ID);
		}
		
	private function _get_image($ID = null)
		{
			if (!is_numeric($ID)) { return; }
			
			$image_resource = gs_basic_get_wp_image($ID);
			
			return $image_resource->html;
			
		}
	
}
