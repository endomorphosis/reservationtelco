	$(document).ready(function() {
		var $dialog = $('<div></div>')
			.html('This dialog will show every time!')
			.dialog({
				autoOpen: false,
				title: 'Basic Dialog'
			});

		$('#opener').click(function() {
			$dialog.dialog('open');
		});
	});