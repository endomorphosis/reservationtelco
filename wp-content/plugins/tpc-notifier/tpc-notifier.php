<?php
/*
Plugin Name: The Portland Company Notifier
Plugin URI: http://plugins.theportlandco.com
Description: This plugin allows back end users to create a message that appears upon users entering a website. It displays it using the traditional lightbox effect and can be enabled or disable. <a href="http://plugins.theportlandco.com/tpc-notifier" target="_blank">Click here</a> if you would like to view a demonstration.
Version: 0.1
Author: The Portland Company
Author URI: http://www.ThePortlandCo.com
License: GPL2

Copyright 2010  THE PORTLAND COMPANY  (email : Us@ThePortlandCo.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as 
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

$tpc_notifier_version = "0.1";

$table_name = $wpdb->prefix . "tpc_notifier"; // THIS VARIABLE IS USE TO CREATE A TABLE IN THE DATABASE CALLED WP_TPC_NOTIFIER

$save_and_publish = $_POST['save-and-publish'];
$unpublish = $_POST['unpublish'];

$posted_notification_title = $_POST['title'];
$posted_message = $_POST['message'];
$posted_status = $_POST['status'];
	
if ( isset($save_and_publish) ) {

	$wpdb->update( $table_name, array( 'title' => $posted_notification_title, 'message' => $posted_message, 'status' => 'published'), array('id' => 1) );
	
} elseif ( isset($unpublish) ) {

	$wpdb->update( $table_name, array( 'title' => $posted_notification_title, 'message' => $posted_message, 'status' => 'unpublished'), array('id' => 1) );
		
}

$show_results = $wpdb->get_row('SELECT * FROM ' . $table_name . ' WHERE id = 1 ', ARRAY_A);
	
$notification_title = $show_results['title'];
$message = $show_results['message'];
$status = $show_results['status'];


function tpc_notifier_create_database() {

   global $wpdb; // THIS GETS THE WORDPRESS DATABASE CREDENTIALS THEN CONNECTS TO THE DATABASE 
   
   global $table_name;
   
    if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) { // THIS ASKS IF THE TABLE ALREADY EXISTS. ESSENTIALLY MEANING: HAS THIS PLUGIN BEEN ACTIVATED BEFORE? 
    
    // THIS ACTUALLY CREATES THE TABLE IN THE DATABASE. KEEP IN MIND THOUGH IT'S STORING IT IN A VARIABLE CALLED $SQL MEANING THIS CODE WON'T BE EXECUTED UNTIL THAT VARIABLE IS USED (WHICH IS IS A FEW LINES BELOW THIS IN DBDELTA($SQL); 
	$sql = "
		CREATE TABLE " . $table_name . " (
			id mediumint(9) NOT NULL AUTO_INCREMENT,
			title VARCHAR(200) NOT NULL,
			message text NOT NULL,
			status VARCHAR(7) NOT NULL,
			UNIQUE KEY id (id)
		);
	";
	
	add_option("tpc_notifier-version", $tpc_notifier_version);
    	
	}
	
	
	// THE DBDELTA FUNCTION REQUIRES UPGRADE.PHP TO BE INCLUDED
	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	dbDelta($sql);

}

register_activation_hook(__FILE__,'tpc_notifier_create_database'); // THIS TELLS WORDPRESS TO EXECUTE THIS FUNCTION UPON THE USER ACTIVATING THE PLUGIN. 




function tpc_notifier_add_options_page() {

	add_options_page('TPC Notifier Options', 'TPC Notifier', 'manage_options', 'tpc-notifier', 'tpc_notifier_admin_page');
	
	wp_enqueue_style('style', '/wp-content/plugins/tpc-notifier/css/admin.css', '', '1.0', 'screen');

}

add_action('admin_menu', 'tpc_notifier_add_options_page');



function tpc_notifier_admin_page() {
	wp_tiny_mce( false , array( "editor_selector" => "message" ) ); // THIS CALLS TINYMCE AND APPLIES IT TO ANY ELEMENT WITH A CLASS OF "MESSAGE", YOU CAN CHANGE THAT TO WHATEVER YOU LIKE OF COURSE.
	
	global $wpdb;
	global $table_name;
	
	global $notification_title;
	global $message;
	global $status;
	
	global $save_and_publish;
	global $unpublish;
	
	echo '
		<div class="wrap tpc-notifier">
		<h2>The Portland Company Notifier Plugin</h2>
	';

	if ( isset($save_and_publish) ) {
		echo '<div class="updated"><p>You&#39;re notification has been saved and <b>published</b>.</p></div>';
	} elseif ( isset($unpublish) ) {
		echo '<div class="updated"><p>You&#39;re notification has been <b>unpublished</b>.</p></div>';
	}	
	
	echo '
		<form method="post" action="">
	';
	
	settings_fields( 'myoption-group' );
	
	echo '
			<p>The Portland Company Notifier Plugin allows you to display a message, in a lightbox presentation format, to users who are visiting your website for the first time. <a href="http://plugins.theportlandco.com/tpc-notifier" target="_blank">Click here</a> if you would like to view a demonstration.</p>
			
			<br />
			
			<h3>The Notification</h3>
			<p>Status: <b>' . ucwords($status) . '</b>.
			<p>Title: <input class="title" type="text" name="title" value="' . $notification_title . '" maxlength="200" />
			<p>Enter the message you would like displayed in your notification into the text area below:</p>
			<textarea name="message"  class="message">' . $message . '</textarea>
			
			<br />
			
			<button class="button-primary" name="save-and-publish">Save and Publish Notification</button>
			<button class="button-secondary" name="unpublish">Unpublish Notification</button>
			
			<br />
			<br />
			
			<div id="sm_rebuild" class="postbox">
				<h3 class="hndle"><span>Shortcode</span></h3>
				<div class="inside">
					<p>To display this message on a particular page, copy the following "shortcode" and paste it into the page you want it to display on:</p>
					<p class="code">[tpc_notifier]</p>
				</div>
			</div>
			
			<br />
			
			<h3>Developer Notes</h3>
			
			<div id="sm_rebuild" class="postbox">
				<h3 class="hndle"><span>PHP Code</span></h3>
				<div class="inside">
					<p>If you are a developer you may use the following PHP code:</p>
					<p class="code">&#60;?php tpc_notifier_code(); ?&#62;</p>
				</div>
			</div>
			
			<div id="sm_rebuild" class="postbox">
				<h3 class="hndle"><span>Styling and Themeing</span></h3>
				<div class="inside">
					<p>If you want to change the default style of the notifier by adding or creating your own, you may deposit it into the <span class="code">wp-content/plugins/tpc-notifier/css</span> directory. And changing the URL to the stylesheet in <span class="code">wp-content/plugins/tpc-notifier/tpc-notifier.php</span> around line 218 (within the <span class="code">tpc_notifier_externals()</span> PHP function).</p>
					<p>Pre-built <a href="http://jqueryui.com/themeroller">themes can be downloaded</a> from the <a href="http://jqueryui.com/home">jQuery UI</a> website (which is the Javascript framework this plugin is built off of).</p>
				</div>
			</div>

			
			<div id="sm_rebuild" class="postbox">
				<h3 class="hndle"><span>Roadmap</span></h3>
				<div class="inside">
					<p>For those who are fans of this plugin here is a <i>"roadmap"</i> of our plans to enhance this plugin:</p>
					<p>1. Implement a theme manager allows back end users to upload new themes or navigate an activate existing themes.</p>
					
					<br />
					
					<p>Have an idea or feature request? <a href="mailto:us@theportlandco.com">Tell us!</a></p>
				</div>
			</div>

		</div>
	';

}


// SHORTCODE API 
function tpc_notifier_code() {
	
	global $notification_title;
	global $message;
	global $status;

	if ( $status == 'published' ) {
	
		echo '
		<script type="text/javascript">
		$(function() {
			$("#dialog").dialog("destroy");
		
			$("#dialog-message").dialog({
				modal: true,
				buttons: {
					Ok: function() {
						$(this).dialog("close");
					}
				}
			});
		});
		</script>
	
		<div id="dialog-message" title="' . $notification_title . '">' . $message . '</div>
		';
	
	} elseif ( $status == 'unpublished' ) {
	}
	
}

add_shortcode('tpc_notifier', 'tpc_notifier_code');


// FRONT END STYLES
function tpc_notifier_externals() {
	
	// GET THE PLUGIN DIRECTORY	
	$plugin_path = WP_PLUGIN_URL.'/'.str_replace(basename( __FILE__),"",plugin_basename(__FILE__));
	
	echo '
		<link rel="stylesheet" type="text/css" href="' . $plugin_path . '/css/rtc-emergency-notification/styles.css">

		<script type="text/javascript" src="' . $plugin_path . '/scripts/jquery-ui-1.7.1.custom.min.js"></script>
	';
}

add_action('wp_head', 'tpc_notifier_externals'); // THIS ADDS THE CODE WITHIN THE TPC_NOTIFIER_STYLES FUNCTION INTO THE HEAD OF THE WEBSITES THEME.

?>