<?php
/*
Plugin Name: GoSeese Page Banner
Plugin URI: http://www.goseese.com/
Description: A custom plugin to allow banner images to be configured for each page, or a default banner to be displayed.
Version: alpha 1.1
Author: Jeff Seese
Author URI: http://www.goseese.com
License: Purchase only
*/

/*
	==================================================================
		Usage - short code
		
		[gs_page_banner]
		
		or by direct function call.
		
		<?php print gs_page_banner(id,rtn,size); ?>
		id = force a banner for specific page id. otherwise
		it assumes the current page, or page parent.
		rtn (url,img)
			url = image url only.
			img = full html for the image.
			
		size = From wp_get_attachment_image_src()
		(string|array) Size of the image shown for an image attachment:
		either a string keyword (thumbnail, medium, large or full)
		or a 2-item array representing width and height in pixels, e.g. array(32,32)
		
		as a bacground image.
		<div id="header" role="banner" style = "margin: 0; padding: 0; background-image: url(<?php gs_page_banner(null,'url') ?>);" >
	==================================================================  */

/*
	==================================================================
		Constants
	==================================================================  */

	//local variable for plugin name.
	$this_plugin_name	= 'gs-page-banner';
	if (!defined('PLUGIN_PATH')) { define('PLUGIN_PATH',dirname(__FILE__).'/../');}

/*
	==================================================================
		includes.
	==================================================================  */
	include("includes/functions.php");					// function to interact with the class
	include("includes/GsPageBanner.php");				// main class
	
/*
	==================================================================
		Admin Menu && resources
	==================================================================  */
if (is_admin())
	{
	//wp_enqueue_style( $this_plugin_name, WP_PLUGIN_URL.'/'.$this_plugin_name.'/css/'.$this_plugin_name.'.css');
	//wp_enqueue_script($this_plugin_name,WP_PLUGIN_URL.'/'.$this_plugin_name.'/js/'.$this_plugin_name.'.js', array('jquery'));
	}
	
function gs_page_banner_admin() {
	add_submenu_page('options-general.php', 'Page Banners', 'Page Banner',5, __file__,'show_page_banner_settings');
}	


/*
	==================================================================
		Actions && Hooks.
	==================================================================  */

add_action('admin_menu', 'gs_page_banner_admin',11);
add_action('admin_init', 'register_gs_page_banner_settings' );
add_filter('edit_page_form', 'show_page_banner_config');
add_action('save_post', 'save_page_banner_config');


/*
	==================================================================
		Short Codes
	==================================================================  */

add_shortcode('gs_page_banner', 'gs_page_banner_shortcode');
