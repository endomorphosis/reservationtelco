<?php

		if (!is_admin()) { die(); }
		$gsbanner	= new GsPageBanner();
		
		$ID = $_REQUEST['post'];
		?>
		<span class = "meta-box-sortables">
		<div id="pagebannerdiv" class="postbox ">
		<div class="handlediv" title="Click to toggle"><br></div><h3 class="hndle"><span>Page Banner</span></h3>
		<div class="inside">
			<div class = "gswpimage_add_image_trigger" id = "default_img_preview" title = "click to change" style = "cursor: pointer; padding: 0; margin: 5px 0; float: left; clear: both; text-align: center;">
				<?php
					$banner_img	=	$gsbanner->show($ID);
					$is_banner	=	get_option('gs_page_banner-'.$ID);
					if (strlen($banner_img) < 1 || strlen($is_banner) < 1)
						{?>
						<p style = "border: 1px solid black; padding: 10px; margin: 10px; text-align: center;">Click to configure banner image</p>
					<?php }
						else
						{
							print $banner_img;
						}
				?>
			</div>
			<input style = "float:left; clear: both;" type="hidden" id = "gs_page_banner_img_src" name="gs_page_banner_img" value="<?php echo $is_banner; ?>" />
			<div style = "clear:both;"></div>
			<input type = "button" value = "Clear Image" onclick = "page_banner_clear_image()">
		</div>
		</div>
		</span>
		<script>jQuery(document).ready(
									function()
									   {
											init_gswpimage_image_uploader('Select a Default Image');
											jQuery('.gswpimage_add_image_trigger').bind('img_update', function (e,data) { update_page_banner_default_image(this,data);} );
									   }
									);
									   
									   
				function update_page_banner_default_image(o,data)
					{
						if (typeof(data.wp_image) != 'undefined')
							{
								jQuery('#gs_page_banner_img_src').val(data.wp_image);
							}
						else
							{
								jQuery('#gs_page_banner_img_src').val(data.url);
							}
							
						jQuery('#default_img_preview').html('<img src = "'+data.url+'" />');
						
					}
					
				function page_banner_clear_image()
						{
							jQuery('#gs_page_banner_img_src').val('');
							jQuery('#default_img_preview').html('<p style = "border: 1px solid black; padding: 10px; margin: 10px; text-align: center;">Click to configure banner image</p>');
						}
		</script>