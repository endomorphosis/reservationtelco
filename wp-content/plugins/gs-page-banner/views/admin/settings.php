<?php

	if (!is_admin()) { die(); }
	$gsbanner	= new GsPageBanner();
?>

<div class="wrap">
<h2>Page Banner Settings</h2>

<form method="post" action="options.php">
    <?php settings_fields( 'gs-page-banner-settings-group' ); ?>
	<p><i>If your website's theme support's the page banner plugin: you can add one here. If you are <i>not</i> a developer and aren't sure if your theme supports this plugin, you should <a href="mailto:us@theprotlandco.com">contact us</a>.</i></p>
	
	<br />
	
	<p>After you've selected a default banner image simply copy and paste the shortcode below into any page.</p>
	<p><b>Shortcode: [gs_page_banner]</b></p>
	<p><b>PHP: &lt;?php gs_page_banner(null,'url'); ?&gt;</b></p>

	<br />

	<h3>Default Banner Image</h3>
	<?php
		$banner_img	= $gsbanner->show();

		if (strlen($banner_img) > 1 ) {
			echo "<p>Click the image below to change it.</p>";
		}
	?>
    <div class="gswpimage_add_image_trigger" id="default_img_preview" title="click to change" style="cursor: pointer; padding: 0; margin: 5px 0; border: 1px solid #CCC; float: left; clear: both; text-align: center;">
		<?php
			$banner_img	= $gsbanner->show();

			if (strlen($banner_img) < 1 ) {
				echo '<p style="border: 1px solid black; padding: 10px; margin: 10px; text-align: center;">Click to configure default image</p>';
			} else {
				print $banner_img;
			}
		?>
	</div>
	
	<input type="hidden" id="default_img_src" name="default_banner_img" value="<?php echo get_option('default_banner_img'); ?>" />
    
    <input class="button-primary" style="float: left; clear: both;" type="submit" value="<?php _e('Save Changes') ?>" />

</form>
</div>
<script>
	jQuery(document).ready(
		function() {
			init_gswpimage_image_uploader('Select a Default Image');
			jQuery('.gswpimage_add_image_trigger').bind('img_update', function (e,data) { update_page_banner_default_image(this,data);} );
		}
	);
	
	function update_page_banner_default_image(o,data) {
		if (typeof(data.wp_image) != 'undefined') {
			jQuery('#default_img_src').val(data.wp_image);
		} else {
			jQuery('#default_img_src').val(data.url);
		}
					
		jQuery('#default_img_preview').html('<img src = "'+data.url+'" />');
				
	}
</script>