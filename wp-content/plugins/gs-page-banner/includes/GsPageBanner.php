<?php

class GsPageBanner {
	
	public function show($ID = null,$rtn = 'img',$size = 'full')
		{
			
			// if no ID assume current page.
			if (is_null($ID))
				{
					global $post;
					$ID = $post->ID;
				}
				
			// recurse through pages until there is one with a
			// configured image.
			
			do {
				$page 	= get_page($ID);
				if ($idx++ > 100) { print "recursion loop ".__file__." : ".__line__; break; }
				$banner_src 	= get_option('gs_page_banner-'.$ID);
				
				$ID = $page->post_parent;
			} while ( strlen($banner_src) < 1 && $page->post_parent != 0 );
			
			// sense no banners configured.
			// get the default banner.
			if 		(strlen($banner_src) < 1 && get_option('default_banner_img') > 1 ) { $banner_src	= get_option('default_banner_img'); }
			elseif 	(strlen($banner_src) < 1 && get_option('default_banner_img') < 1 ) { return; }
			
			$image	= $this->get_image($banner_src,$size,'gs-page-banner-img');
				
			/*
				==================================================================
					Return the type of data requested.
					
				==================================================================  */
			switch($rtn)
				{
					case 'url':
						{
							return $image->url;
						}break;
						
					case 'img':
					default:
						{
							return $image->html;
						}break;
				}
			
		}
		
	/*
		==================================================================
			Build image, takes an argument, and determines if it
			is either a attatchment ID, or a URL.
			
			Formats the image to the correct size, returns the
			url and full html.
			
		==================================================================  */
	private function get_image($img_resource, $size, $class = '')
		{
			if (preg_match("/^\d+$/",$img_resource))
				{
					list($img_url, $w, $h) = wp_get_attachment_image_src( $img_resource,$size,true);
				}
			else
				{
					$img_url	= $img_resource;
				}
			
			$img_html	= '<img src = "'.$img_url.'" class = "'.$class.'" '.(is_numeric($w)? "width='$w'" : '').(is_numeric($h)? "height='$h'" : '').' />';
			
			$image->url				= $img_url;
			$image->html			= $img_html;
			$image->w				= $w;
			$image->h				= $h;
			return $image;
		}

}
