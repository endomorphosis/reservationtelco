<?php

	function gs_page_banner_shortcode($args)
		{
			
			$gspb	= new GsPageBanner();
			return $gspb->show($args['ID'],$args['rtn'],$args['size']);
		}
	function gs_page_banner($ID = null,$rtn = null,$size = null)
		{
			// ID defaults to current page id.
			// rtn defaults to full image html (url,img)
			// size  see wp_get_attachment_image_src()
			
			$gspb	= new GsPageBanner();
			print $gspb->show($ID,$rtn,$size);
		}
		
	function register_gs_page_banner_settings() {
		register_setting( 'gs-page-banner-settings-group', 'default_banner_img' );
	}

	/*
		==================================================================
			Show the default banner settings.
		==================================================================  */
	function show_page_banner_settings() {
		include(PLUGIN_PATH."gs-page-banner/views/admin/settings.php");
	}


	/*
		==================================================================
			Show the admin page config
		==================================================================  */
	function show_page_banner_config() {
			//ob_start();
			include(PLUGIN_PATH."gs-page-banner/views/admin/page_admin.php");
			//$contents = ob_get_contents();
			//ob_end_clean();
			
			//return $contents;
		}
		
	/*
		==================================================================
			Save Page/Post permissions.
		==================================================================  */
	function save_page_banner_config($ID = null)
		{
			if (is_null($ID)) { $ID = $_POST['post_ID']; }
			update_option( 'gs_page_banner-'.$ID, $_POST['gs_page_banner_img'] );
		}
