<?php
/*
Plugin Name: GoSeese Wordpress Image Manager
Plugin URI: http://www.goseese.com/
Description: A custom plugin to allow easy use of the wordpress image uploader/manager in other plugins
Version: alpha 1.0
Author: Jeff Seese
Author URI: http://www.goseese.com
License: Purchase only
*/

/*
	==================================================================
		usage
		
		create an html object, and give it this class gswpimage_add_image_trigger
		
		then init the listeners
		<script>init_goc_image_uploader('select an image')</script>
		
		bind the event "img_update" to the target object. 
		
	==================================================================  */

function wp_ajax_gswpimage_controller()
	{
		switch($_POST['a'])
			{
				case 'parse_src':
					{
						
						$img_src	= stripcslashes($_POST['img_src']);
						$args		= array();
						// test for image currently in wp-image-library
						if (preg_match("/wp-image-\d+/", $img_src))
							{
								preg_match_all("/<a href=.(.+).>(<img.+width=.(\d+).+height=.(\d+).+wp-image-(\d+).+>)</", $img_src, $matches);
								$args['url']		= $matches[1][0];
								$args['img']		= $matches[2][0];
								$args['width']		= $matches[3][0];
								$args['height']		= $matches[4][0];
								$args['wp_image']	= $matches[5][0];
							}
						else
							{
								preg_match_all("/<img.+ src=.(.+)\".class.+width=.(\d+).+height=.(\d+).+>/", $img_src, $matches);
								$args['url']		= $matches[1][0];
								$args['img']		= $matches[0][0];
								$args['width']		= $matches[3][0];
								$args['height']		= $matches[4][0];
							}
						echo json_encode ($args);
					}break;
					
				default:
					{
						echo json_encode( array('status'	=> 0, 'message' => 'That function does not exist in '.basename(__file__) ) );
					}break;
			}
		die();
	}


if (is_admin())
	{
	wp_enqueue_script('media-upload');
	wp_enqueue_script('thickbox');
	wp_enqueue_script('gs-wp-image-manager',WP_PLUGIN_URL.'/gs-wp-image-manager/js/gs-wp-image-manager.js', array('jquery'));
	wp_enqueue_style('thickbox');
	}

add_action('wp_ajax_gswpimage', 'wp_ajax_gswpimage_controller',11);


/*
 <a href="http://wpdev.goseese.com/wp-content/uploads/2010/05/trailer-park-boys.jpg">
 <img src="http://wpdev.goseese.com/wp-content/uploads/2010/05/trailer-park-boys-300x158.jpg" alt="" title="trailer-park-boys" width="300" height="158" class="alignnone size-medium wp-image-4" />
 </a>
*/