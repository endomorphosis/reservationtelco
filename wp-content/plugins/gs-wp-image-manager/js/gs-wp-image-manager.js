/*
	==================================================================
		media uploader
	==================================================================  */
	
	var gswpimage_target = '';
	
	/*
		==================================================================
			Show the iFrame uploader
		==================================================================  */
	function init_gswpimage_image_uploader(message)
	{
		// listen for an "add image button to be clicked
		jQuery('.gswpimage_add_image_trigger').click(function() {
			
		gswpimage_target	=	jQuery(this).get();
		
		// load the iframe media manager.
		 tb_show(message, 'media-upload.php?type=image&amp;TB_iframe=true');
		 return false;
		});
		
		// on complete process the result
		window.send_to_editor = function(img_src) {
			
		gswpimage_parse_src(gswpimage_target,img_src);
		
		 tb_remove();
		}

	};
		
	/*
		==================================================================
			return the json object of the image. 
		==================================================================  */
	function gswpimage_parse_src(t,img_src)
		{
		var post = {
				action: 'gswpimage', // this never changes.
				a: 'parse_src',
				img_src:img_src
			};

		jQuery.post(ajaxurl, post,
					function(data)
						{
							jQuery(t).trigger('img_update',data); // you must build something to listen to this img_update trigger.
						},"json"
				);
		}