/*
	==================================================================
		Javascript controls for the gs pop window	
	==================================================================  */
	
	function gs_pop_window(post)
		{
			gs_pop_window_destroy();
			var action = 'gs_pop_window';
			var content = '';
			/*
				==================================================================
					post is an array of args
					title: abc
					contents: def
					mainclass: ddd
					buttons:ddd
				==================================================================  */
			switch (typeof(post))
				{
					case('undefined'):
						{
							post = { action: action}
						}break;
						
					case('string'):
						{
							content = post;
							post = { action: action}
						}break;
					case('object'):
						{
							content = post.content;
							post['action']	= action;
							if (typeof(post['ajaxurl']) != 'undefined' ) { ajaxurl = post['ajaxurl']; delete post['ajaxurl']; }
						}
					default:
						{
							post['action']	= action;
							delete post['content'];
						}
				}
				
			//alert(typeof(post) + "\n" + post.toSource());
			
			jQuery.post(ajaxurl, post,
						function(data)
							{
								jQuery('body').append(data);
								jQuery('.gs_pop_window_bg .pop_content').html(content);
							}
					);
		}
		
	function gs_pop_window_destroy()
		{
			
			jQuery('.gs_pop_window_bg').remove();
		}