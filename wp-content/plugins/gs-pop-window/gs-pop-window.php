<?php
/*
Plugin Name: GoSeese Pop Up Window
Plugin URI: http://www.goseese.com/
Description: A custom plugin to allow a cleaner pop up message window, this plugin supports other GS plugins, and has no direct user access. Simple usage is gs_pop_window('some content'); assumes ajaxurl is configured in wp_head
Version: alpha 1.0
Author: Jeff Seese
Author URI: http://www.goseese.com
License: Purchase only
*/

/*
	==================================================================
		A function to display a popup window.
		
	==================================================================  */


//if (is_admin()) {
	wp_enqueue_style( 'gs-pop-window', WP_PLUGIN_URL.'/gs-pop-window/css/gs-pop-window.css');
	wp_enqueue_script('gs-pop-window',WP_PLUGIN_URL.'/gs-pop-window/js/gs-pop-window.js', array('jquery'));
//	}

class GSPopWindow {
	
	public	$mainclass	= '';
	public	$title		= '';
	public	$buttons	= '';
	public	$contents	= '<p>Loading.....</p>';
	
	public	function build()
				{
					?>
					<div class = "gs_pop_window_bg <?php echo $this->mainclass; ?>" >
					<table class = "gs_pop_main_table" align = "center">
						<tr><td colspan = "3"><div class = "gs_pop_close_icon" onclick = "gs_pop_window_destroy(this);" style = "margin-right: 5px;"></div></td></tr>
						<tr class = "gs_pop_head_row">
							<td class = "top_left"><td class = "pop_title pop_window_color" valign = "middle"><h3><?php echo $this->title; ?></h3></td><td class = "top_right"></td></tr>
						<tr><td class = "pop_window_color" >&nbsp;</td><td class = "pop_content pop_window_color" ><?php echo $this->contents; ?></td><td class = "pop_window_color">&nbsp;</td></tr>
						<tr><td class = "pop_window_color" >&nbsp;</td><td class = "pop_button_row pop_window_color"><?php echo $this->buttons; ?></td><td class = "pop_window_color">&nbsp;</td></tr>
						<tr><td class = "bot_left"></td><td class = "pop_window_color" >&nbsp;</td><td class = "bot_right"></td></tr>
					</table>
					</div>
					<?php
				}
}

function wp_ajax_gs_pop_window()
	{
		$gspw	= new GSPopWindow;
		
		if (is_array($_REQUEST))
			{
				foreach ($_REQUEST as $key => $value)
					{
						$gspw->$key	= stripcslashes($value);
					}
			}
		
		$gspw->build();
		
		die();
	}
add_action('wp_ajax_gs_pop_window', 'wp_ajax_gs_pop_window',11);
add_action('wp_ajax_nopriv_gs_pop_window', 'wp_ajax_gs_pop_window',11);

