<?php
/*
Plugin Name: GoSeese Presentation
Plugin URI: http://www.goseese.com/
Description: A plugin to allow you to create slide show presentations and insert them in your blog/site. This plugin is dependent on the gs-basic-functions, gs-wp-imagemanager and jquery 1.4.x
Version: alpha 1.2
Author: Jeff Seese
Author URI: http://www.goseese.com
License: Purchase only
*/

/*
	==================================================================
		Constants
	==================================================================  */
	$this_plugin_name	= 'gs-presentation';
	if (!defined('PLUGIN_PATH')) { define('PLUGIN_PATH',dirname(__FILE__).'/../');}
	if (!defined('GS_PRE_PREFIX')) { global $wpdb; define('GS_PRE_PREFIX',$wpdb->prefix . "gsp_");}
	
/*
	==================================================================
		Enqued styles and scripts for the user facing pages.
	==================================================================  */
	wp_enqueue_style( $this_plugin_name, WP_PLUGIN_URL.'/'.$this_plugin_name.'/css/'.$this_plugin_name.'.css');
	wp_enqueue_script($this_plugin_name,WP_PLUGIN_URL.'/'.$this_plugin_name.'/js/'.$this_plugin_name.'.js', array('jquery'));

/*
	==================================================================
		includes.
	==================================================================  */
	include("includes/functions.php");					// function to interact with the class
	include("includes/GsPresentation.php");				// main class
	include("includes/activate-plugin.php");			// db schema
	include("includes/ajax-controller-admin.php");		

	
/*
	==================================================================
		Admin Menu && resources
	==================================================================  */
if (is_admin())
	{
	wp_enqueue_style( $this_plugin_name.'-admin', WP_PLUGIN_URL.'/'.$this_plugin_name.'/css/'.$this_plugin_name.'-admin.css');
	wp_enqueue_script($this_plugin_name.'-admin',WP_PLUGIN_URL.'/'.$this_plugin_name.'/js/'.$this_plugin_name.'-admin.js', array('jquery'));
	
	}
	//<script type='text/javascript' src='http://wpdev.goseese.com/wp-admin/load-scripts.php?c=1&amp;load=utils,editor,quicktags&amp;ver=d652578b8499672dc5f8695841c1aa48'></script>

	
function gs_presentation_admin() {
	add_menu_page('Presentation', 'Presentation', 5, __FILE__, 'show_gs_presentation_docs');
	add_submenu_page(__file__, 'Documentation', 'Documentation', 5, __file__,'show_gs_presentation_docs');
	add_submenu_page(__file__, 'Presentation', 'Editor', 5, 'presentation-editor','show_gs_presentation_editor');
	add_submenu_page(__file__, '', '', 5, 'scene-editor','show_gs_scene_editor');

	//add_submenu_page(__file__, 'Brands', 'Brands', 5, 'brand-manager','show_gocart_brand_manager');
}	


/*
	==================================================================
		Actions && Hooks.
	==================================================================  */
register_activation_hook( __FILE__, 'gs_presentation_activate' );
//add_action( 'wp_head', current_jquery( '1.4.2' ),0 );
add_action('admin_menu', 'gs_presentation_admin',11);
add_action('init', 'gs_start_session',1);  // used by admin functions to prevent reload button from re-posting form data.
add_action("wp_ajax_gs_presentation_admin",'wp_ajax_gs_presentation_admin',11);


/*
	==================================================================
		Short Codes
	==================================================================  */

add_shortcode('gs-presentation', 'gs_presentation_short_code');

