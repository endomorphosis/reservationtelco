<?php

?>
<div class="wrap gsp_editor-wrap" style = "width: 700px;">
<div id="icon-options-general" class="icon32"></div><h2>Presentation Documentation</h2>

<h2>Getting Started.</h2>
<blockquote><i>Note: through out the presentation configuration pages there are additional notes for some configuration values. This is indicated by a dotted green line under the label. Hover over the line to see the additional info for that configuration.</i>
</blockquote>
<p>To create your presentation, go to the "Presentation" -> "Editor" tab located on the lower left of your browser. Click "Create New Presentation" to begin.</p>
<p>A new presentation will be displayed along with the short code, and php code necessary to install it into your page or theme.</p>
<p>Click on the magnifying glass icon to view/edit the main details of your presentation. Click the red X icon to delete the presentation and all scenes associated with it.</p>
<p>Once in the main view you will notice a black border square with dotted red horizontal and vertical cursors. The cursors indicate that all measurements on this page are taken from the center point of the presentation.</p>

<br />

<h3>Main Settings</h3>
<p>Start by Customizing your presentation comment. This is only for your display to help you keep track of your presentation, it is never displayed to the web viewer.</p>
<p>Dimensions an border are pretty self explanatory. The width and height of the presentation must be configured here, but all other css settings can be overridden by custom CSS if you wish to format it in that way.</p>

<br />

<h3>Main Background</h3>
<p>It&#8217;s best to have either a solid CSS color or 1 non repeating image. Internet explorer has problems with animations over repeating image backgrounds so try to avoid that.</p>
<p>To choose a background image, click on the blank field next to the image label. This will bring up word press image manager. Select an image from your computer or that is already in your media library. No position or size info will be used from the manager.</p>
<p>You can configure the background position by editing the x position and y position values. Acceptable values are offsets in pixels, or other CSS strings like center, top left.</p>

<br />

<h3>Prev and Next Button Settings</h3>
<p>Prev and next buttons are optional configurations. By not configuring the settings, they will not be displayed.</p>
<p>The Prev and Next button dimension refers to the width and height of 1 button not the pair of buttons.</p>
<p>X offset and Y offset relate to the distance from the center of the button to the X,Y Zero point on the presentation. The Main X, Y zero point is indicated by the crossing of the red dotted lines in the center of the presentation. For example a value of Y offset = 100, would place the horizontal centerline of the buttons 100 pixels below the center of the presentation. A value of X offset = 100 would shift the vertical center line of the buttons to the right 100 pixels.</p>
<p>The next value is the Dx and Dy. This stands for Distance X and Distance Y. This value is the Distance between center lines of the buttons. A value of DX = 100 would push horizontally centers 100 pixels apart from each other&#8230; so 200 pixels total distance from button to button. To make buttons appear on opposite sides of your presentation, enter a DX value that is greater then half of the distance of the main presentation width.</p>

<table>
	<tr><td>Opposite </td><td><img src = "<?php echo WP_PLUGIN_URL; ?>/gs-presentation/images/buttons_opposite.jpg" /></td></tr>
	<tr><td>Internal </td><td><img src = "<?php echo WP_PLUGIN_URL; ?>/gs-presentation/images/buttons_internal.jpg" /></td></tr>
	<tr><td>Stacked </td><td><img src = "<?php echo WP_PLUGIN_URL; ?>/gs-presentation/images/buttons_stacked.jpg" /></td></tr>
</table>

<p>Prev Next button background images.</p>

<p>The background images of the prev next buttons must be constructed so that the off/non-hover state of the bottons are on the top of the image, and the on/hover state are at the bottom of the image. Both the left and right versions of the image must be in the same file. The left off button needs to be in the upper right corner of the image, and the right off image needs to be in the upper right of the image. The On state for each button need to be vertically on center with the Off state buttons, and placed at the bottom of the button template image.</p>
<p>When a button is hovered, or in the on state the background position of that button is changed from top to bottom. For example, the left button in the off state has the background position set to background-position: left top; In the on or hover state it&#8217;s set to background-position: left bottom;</p>

<br />

<h3>Current slide area</h3>
<p>The current slide indicator can be place anywhere on the presentation, or browser as well. The Height must be configured, but the width of the indicator area is determined by the number of scenes that are being displayed.</p>
<p>A background image for the indicator area can also be configured.</p>
<p>The left and right side of the indicator will be taken from the left and right portions of the background image. The center of the tray area will be a repeating section of the center of the tray background. The tray background only has to be slightly wider then the width of your scene indicator image + the margin that you add to the indicator.</p>

<br />

<h3>Current Slide Indicator</h3>
<p>The current slide indicator must have the width and height configured in order for it to display. The margin top and margin left/right allow you to shift the placement of the the indicators in the indicator area.</p>
<p>The background image must be designed so the top of the image is the indicator in the off state, and the bottom of the image is the indicator in the on state. When the indicator is off it has the css background-position: top center. When the indicator is on/hovered it has the position bottom center.</p>

<br />

<h2>Scene Settings</h2>
<p>click the scenes tab from the main settings view to see a list of current scenes configured in the presentation. Once there are several scenes created you will notice they can be moved in the display order by clicking the up/down arrows and deleted by clicking the X.</p>
<p>Edit a scene by clicking on the magnifying glass.</p>
<p>In the scene editor note that the horizontal and vertical cursors are not in the upper left hand corner. This indicates that all x,y positions on the scene are in reference to the upper left corner of the presentation.</p>
<p>Also note, all scenes default to not active. This means they will not display in your presentation. Select &#8220;Active&#8221; from the drop down menu in the scene editor to include that scene in your presentation.</p>

<br />

<h3>Scene Settings</h3>
<p>Effect In: This is the animation or effect that will be used to bring this scene into view. The Effect In time, is the amount of time in seconds that it will take for the effect to finish.</p>
<p>Display time is the amount of time in seconds that the scene will be displayed before the Effect Out will be started. At this point, the next scene&#8217;s effect in will also be started.</p>
<p>Effect out, and Effect out Time are identical in function to the Effect In and Effect In Time settings.</p>

<br />

<h3>Scene Background</h3>
<p>The scene background defaults to CSS #FFFFFF. This can be changed to any CSS color, or background image using the background settings. You can achieve a transparent background effect by using a semitransparent PNG for the background image. However, be sure to use as large (width/height) of an image as possible to help keep animations smooth. As stated before, repeating images cause Internet explorer animations to be jumpy.</p>
<p>Scene Link</p>
<p>The scene link is the url/location that the user will be directed to when they click on the currently active scene</p>
<p>Scene Callback</p>

<p>Scene Callback is a javascript function that will be evaluated when a viewer clicks on that scene. This is useful to connect viewer interactions with the presentation to google analytics. For example. If you have a scene called, &#8220;Act Now Discount&#8221; and you wanted to track how man people click on that scene put something like this in the call back.</p>
<p><span style="font-family: Courier New,Courier,mono;">pageTracker._trackPageview('/gs-presentation/act-now-discount');</span></p>
<p>You can put any value in there for a directory and page name. For more info read this posting on <a href="http://www.google.com/support/googleanalytics/bin/answer.py?hl=en&amp;answer=55521">tracking javascript events with google analytics</a>.</p>

</div>