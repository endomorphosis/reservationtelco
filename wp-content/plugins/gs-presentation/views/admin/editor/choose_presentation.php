<?php
	if (!is_admin()) { die(); }
	
	$gsp	= new GsPresentation();

	if (isset($_POST['new_gsp']) &&  !gs_prevent_repost($_POST['post_once']) ) {
	
			global $userdata;
			
			$gsp->add_new($userdata->user_login."'s new presentation ".Date('Y-m-d'));
	}
		
	if (isset($_POST['a']) &&  !gs_prevent_repost($_POST['post_once']) ) {
	
		switch($_POST['a']) {
			case 'do_drop_pres': {
				$gsp->drop_pres($_POST['gsp_UID']);
			}
		}
			
	}
	
	
	$all_presentations = $gsp->getall();
?>

<div class="wrap gsp_editor-wrap">

<div style ="display:none;">
	<p>View:<?php print __FILE__;?></p>
	<p>This is only to force the comment to display. This div serves no purpose.</p>
</div>

<div id="icon-options-general" class="icon32"></div><h2>Presentation Editor</h2>

<br />

<form method="post">
	<input type="hidden" name="post_once" value="<?php echo microtime(); ?>" />
	<input type="submit" class="button-primary" name="new_gsp" value="<?php _e('Create New Presentation') ?>" />
</form>

<br />

<table style="width: 100%;" class="gs_basic_datatable">
	<tr class="labelrow">
		<td>Comment/Name</td><td>Short Code</td><td>PHP function call</td><td>Action</td></tr>
	
	<?php
		if (!is_array($all_presentations) || count ($all_presentations) < 1) {
			print '<tr><td colspan = "5"><p style="text-align: center;">No Presentations Configured</p></td></tr>';
		} else {
			foreach ($all_presentations as $presentation) {
				print '<tr class="'. ($rc++ % 2 == 0? ' alt': '') .'"><td>'.htmlentities($presentation->gsp_comment).'</td><td>[gs-presentation '.$presentation->gsp_UID.']</td><td>&lt;?php gs_presentation('.$presentation->gsp_UID.'); ?&gt;</td><td><a href = "'. $_SERVER['REQUEST_URI'] .'&gsp_UID='.$presentation->gsp_UID.'" title="View/Edit" ><div class="gs_basic_aic mag"></div></a><input type="button" title="Delete presentation" class="gs_basic_aic close" style="margin-left: 2px;" onclick = "gs_pres_delete('.$presentation->gsp_UID.')" /></td></tr>';
			}
		}
	?>
</table>
</div><!-- end of editor-wrap -->