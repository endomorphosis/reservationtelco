<?php

	if (!is_admin()) { die(); }
	$gsp	= new GsPresentation();
	
	$scene_UID	= $_REQUEST['scene_UID'];
	
	$gs_scene 	= $gsp->get_scene($scene_UID);
	$gsp_UID	= $gs_scene->gsp_UID;
	$gs_main 	= $gsp->get_main($gsp_UID);
	
	$w		=	$gs_main->meta['m_width'];
	$h		=	$gs_main->meta['m_height'];
	$cx		= 	$gs_main->m_cx;
	$cy		=   $gs_main->m_cy;


?>
<div style ="display:none;">
		<br>view:<?php print __FILE__;?>
		<br>this is only to force the comment to dislpay. This div serves no purpose.
</div>
	<style>
	#gsp_presentation_main_wrap .gsp_scene {
		width:				<?php echo $w.'px;';?>
		height:				<?php echo $h.'px;';?>
		
	}
	</style>
	<div id = "gsp_presentation_main_wrap" style="<?php echo ' margin: 20px 0 50px -'.$cx.'px; left: 50%;'?>">
		<div id = "gsp_main_demo" style="<?php echo $gs_main->m_style; ?>">
			<!-- this is the holder for al the scenes -->
			
			<div class="gsp_scene scene_UID-<?php echo $scene_UID;?>" style="<?php echo $gs_scene->scene_style; ?> visibility: visible;" >
				<div class="gsp_scene_content_wrap <?php echo $gs_scene->meta['content_class_names']; ?>" style="<?php echo $gs_scene->content_style.' '.$gs_scene->meta['content_custom_style']; ?>">
					<div class="gsp_scene_caption <?php echo $gs_scene->meta['content_1_class_names']; ?>" style="<?php echo $gs_scene->caption_1_style; ?>"><?php echo $gs_scene->meta['caption_1']; ?></div>
				</div><!-- end of gsp_scene_content_wrap -->
				<div class="gsp_scene_link" style="<?php echo $gs_scene->link_style; ?> display: block;"></div>
			</div><!-- end of gsp_scene --> 
			
			<!-- end of scenes -->
			
			<div id = "hcursor" style="top: 0px; left: 0px; width: <?php echo $w;?>px;">0px</div>
			<div id = "vcursor" style="top: 0px; left: 0px; height: <?php echo $h;?>px;">0px</div>
			
		</div><!-- end of gs_main -->
		<div class="gsp_prev_next_btn L" style="<?php echo $gs_main->pre_next_left_style;?>"></div><div class="gsp_prev_next_btn R" style="<?php echo $gs_main->pre_next_right_style;?>" ></div>
		<div class="slide_tray_wrap" style="<?php echo $gs_main->slide_tray_style; ?>">
		
		<table class="slide_tray"><tr>
					<td class="L" style="<?php echo $gs_main->slide_tray_L_style;?>">&nbsp;</td>
					
					<?php $all_scenes = $gsp->getall_secenes($gsp_UID,true);
						foreach ($all_scenes as $scene)
						{?>
							<td class="M"  style="<?php echo $gs_main->slide_tray_M_style;?>">
								<div class="indicator <?php echo ($scene->scene_UID == $scene_UID? 'is_active' : ''); ?>" style ="<?php echo $gs_main->curr_slide_ind_style; ?>"></div>
							</td>
					<?php } ?>
					
					<td class="R"  style="<?php echo $gs_main->slide_tray_R_style;?>">&nbsp;</td></tr>
		</table>
		
		</div><!-- curr_slide_tray_wrap -->
	</div><!-- end of gsp_presentation_main_wrap -->
<div class="clear"></div>
<input type="button" class="button-primary" style="margin-top: 10px;" value="<?php _e('Save Changes') ?>" onclick = "tinyMCE.triggerSave();jQuery(this).parents('.result_target').trigger('do_update');"/>
<div class="clear"></div>
<table><tr><td style="width: 400px; border-right: 2px dotted #CCC; padding: 5px;" valign="top"><!-- main settings -->

	<table class="gs_basic_datatable" style="margin-top: 10px;">
		<tr class="labelrow"><td colspan="3">Scene Settings</td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign="middle">Is Active Scene:</td><td><select name="is_active"><?php echo gs_build_option_selection($gs_scene->is_active,$gsp->status_options); ?></select></td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign="middle">Comment:</td><td ><input type="text" class="px300" name="scene_comment"	value="<?php echo htmlentities($gs_scene->scene_comment); ?>"></td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign="middle">Effect In:</td><td ><select name="meta[s_effect_in]"><?php echo gs_build_option_selection($gs_scene->meta['s_effect_in'],$gsp->effect_in_options); ?></select></td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign="middle">Effect&nbsp;In&nbsp;Time&nbsp;(s):</td><td ><input type="text" class="px90" name="meta[s_effect_in_time]"	value="<?php echo htmlentities($gs_scene->meta['s_effect_in_time']); ?>"></td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign="middle">Display&nbsp;Time&nbsp;(s):</td><td ><input type="text" class="px90" name="meta[s_display_time]"	value="<?php echo htmlentities($gs_scene->meta['s_display_time']); ?>"></td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign="middle">Effect Out:</td><td ><select name="meta[s_effect_out]"><?php echo gs_build_option_selection($gs_scene->meta['s_effect_out'],$gsp->effect_out_options); ?></select></td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign="middle">Effect&nbsp;Out&nbsp;Time&nbsp;(s):</td><td ><input type="text" class="px90" name="meta[s_effect_out_time]"	value="<?php echo htmlentities($gs_scene->meta['s_effect_out_time']); ?>"></td></tr>

</table>
	
	<table class="gs_basic_datatable" style="margin-top: 10px;">
		<tr class="labelrow"><td colspan="2">Scene Background<small><br>Repeating images cause Internet Explorer animations to be jumpy. The background should at the very least be a solid color, or and image that does not repeat, or repeats as little as possible</small></td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign="middle">color:</td><td><input type="radio" name="meta[s_bgc_transparent]" value="0" <?php echo ($gs_scene->meta['s_bgc_transparent'] != 1? 'checked = "checkeck"':'');?><input id ="s_bgc" name="meta[s_bgc]" type="text" class="px90" value="<?php echo $gs_scene->meta['s_bgc']; ?>"> <label for="s_bgc_transparent">&nbsp;Transparent&nbsp;</label><input type="radio" name="meta[s_bgc_transparent]" value="1" <?php echo ($gs_scene->meta['s_bgc_transparent'] == 1? 'checked = "checkeck"':'');?>/></td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign="middle">image:</td><td><input class="button-secondary" type="button" value="Clear Image" onclick = "jQuery('#s_bgsrc').trigger('clear_image');"/><input type="text" id = "s_bgsrc" name="meta[s_bgsrc]" class="gswpimage_add_image_trigger"	role="s_bgsrc" value="<?php echo htmlentities($gs_scene->meta['s_bgsrc']); ?>"></div></td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign="middle">x position: <small><span class="gs_info" title="pixels or left, right, center">?</span></small>:</td><td><input type="text" class="px90" name="meta[s_bgx]"	value="<?php echo htmlentities($gs_scene->meta['s_bgx']); ?>"></td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign="middle">y position: <small><span class="gs_info" title="pixels or top, bottom, center">?</span></small></td><td><input type="text" class="px90" name="meta[s_bgy]"	value="<?php echo htmlentities($gs_scene->meta['s_bgy']); ?>"></td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign="middle">repeat:</td><td><select name="meta[s_bgrepeat]"><?php echo gs_build_option_selection($gs_scene->meta['s_bgrepeat'],$gsp->repeat_options); ?></select></td></tr>
			
	</table>
	
	<table class="gs_basic_datatable" style="margin-top: 10px;">
		<tr class="labelrow"><td colspan="3">Scene Link<small><br>This is where you configure the where the user will be directed when they click on this scene. You can also configure a custom link image to encourage the user to click on the scene</small></td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign="middle">Link Type:</td><td colspan="2"><select name="meta[s_link_type]" id = "s_link_type" onChange="gs_presentation_show_link_types(this,'s_link_src')"><?php echo gs_build_option_selection($gs_scene->meta['s_link_type'],$gsp->link_types); ?></select></td></tr>
		
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?> gs_link_src_selector s_link_src is_url"><td valign="middle" >URL: </td><td colspan="2"><input type="text" class="px150" name="meta[s_linkurl]"	value="<?php echo htmlentities($gs_scene->meta['s_linkurl']); ?>"></td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?> gs_link_src_selector s_link_src is_page"><td valign="middle">Page: </td><td colspan="2"><?php echo gs_wp_page_selector($gs_scene->meta['s_linkpage'],"meta[s_linkpage]"); ?></td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?> gs_link_src_selector s_link_src is_post"><td valign="middle">Post: </td><td colspan="2"><?php echo gs_wp_post_selector($gs_scene->meta['s_linkpost'],"meta[s_linkpost]"); ?></td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?> gs_link_src_selector s_link_src is_cat"><td valign="middle">Category: </td><td colspan="2"><?php echo gs_wp_category_selector($gs_scene->meta['s_linkcat'],"meta[s_linkcat]"); ?></td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign="middle">Dimensions:</td><td align="center">Width (px):<br><input type="text" class="px90" name="meta[s_link_width]" 		value="<?php echo htmlentities($gs_scene->meta['s_link_width']); ?>"></td>
												<td align="center">Height (px):<br><input type="text" class="px90" name="meta[s_link_height]"	value="<?php echo htmlentities($gs_scene->meta['s_link_height']); ?>"></td></tr>

		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign="middle">Link Image:</td><td colspan="2"><input class="button-secondary" type="button" value="Clear Image" onclick = "jQuery('#s_linkimg_src').trigger('clear_image');"/><input type="text" id = "s_linkimg_src" name="meta[s_linkimg_src]" class="gswpimage_add_image_trigger"	role="s_linkimg_src" value="<?php echo htmlentities($gs_scene->meta['s_linkimg_src']); ?>"></div></td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign="middle">x (px): </td><td colspan="2"><input type="text" class="px90" name="meta[s_linkimgx]"	value="<?php echo htmlentities($gs_scene->meta['s_linkimgx']); ?>"></td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign="middle">y (px)</td><td colspan="2"><input type="text" class="px90" name="meta[s_linkimgy]"	value="<?php echo htmlentities($gs_scene->meta['s_linkimgy']); ?>"></td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign="middle" ><span class="gs_info" title="This is a javascript function that will be evaluated/called when the scene is clicked. Use this track clicks on your presentation to google analytics, or to trigger other functionality on your page">Link Callback:</span></td><td colspan="2"><input type="text" class="px300" name="meta[s_linkcallback]"	value="<?php echo htmlentities($gs_scene->meta['s_linkcallback']); ?>"></td></tr>
			
	</table>
</td><!-- end of main settings col -->
<td style="width: 400px; border-right: 2px dotted #CCC; padding: 5px;" valign="top">
	<table class="gs_basic_datatable" style="margin-top: 10px;" width = "100%">
		<tr class="labelrow"><td colspan="3">Content Area</td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign="middle">Display:</td><td colspan="2"><select name="meta[content_display]"><?php echo gs_build_option_selection($gs_scene->meta['content_display'],$gsp->display_options); ?></select></td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign="middle">Dimensions:</td><td align="center">Width (px):<br><input type="text" class="px90" name="meta[content_width]" 		value="<?php echo htmlentities($gs_scene->meta['content_width']); ?>"></td>
												<td align="center">Height (px):<br><input type="text" class="px90" name="meta[content_height]"	value="<?php echo htmlentities($gs_scene->meta['content_height']); ?>"></td></tr>

		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign="middle">x (px):</td><td colspan="2"><input type="text" class="px90" name="meta[content_x]"	value="<?php echo htmlentities($gs_scene->meta['content_x']); ?>"></td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign="middle">y (px):</td><td colspan="2"><input type="text" class="px90" name="meta[content_y]"	value="<?php echo htmlentities($gs_scene->meta['content_y']); ?>"></td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign="middle">pading (px)</td><td colspan="2"><input type="text" class="px90" name="meta[content_padding]"	value="<?php echo htmlentities($gs_scene->meta['content_padding']); ?>"></td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>" ><td valign="middle">Border:</td>	<td colspan="2"><select name="meta[content_borderw]"><?php $idx = -1; while ($idx++ < 100){echo '<option value="'.$idx.'" '. ($idx == $gs_scene->meta['content_borderw']? 'selected = "selected"' : '') .'>'.$idx.'px</option>';}?></select><select name="meta[content_bordert]"><?php echo gs_build_option_selection($gs_scene->meta['content_bordert'],$gsp->border_options); ?></select><input id ="content_borderc" name="meta[content_borderc]" type="text" class="px90" value="<?php echo $gs_scene->meta['content_borderc']; ?>"></td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>" ><td valign="middle">Border Radius:</td><td colspan="2"><select name="meta[content_borderr]"><?php $idx = -1; while ($idx++ < 200){echo '<option value="'.$idx.'" '. ($idx == $gs_scene->meta['content_borderr']? 'selected = "selected"' : '') .'>'.$idx.'px</option>';}?></select></td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign="middle"><span class="gs_info" title="Add as many additional class names as you want, seperated by spaces">Additional Class Names</span></td><td colspan="2"><input type="text" class="px150" name="meta[content_class_names]"	value="<?php echo htmlentities($gs_scene->meta['content_class_names']); ?>"></td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign="middle" colspan="3"><span class="gs_info" title="Valid CSS style info that will be inserted into the objects style tag">Custom Styles</span><br><small>This must be valid CSS or it may break your entire scene</small><br><textarea class="px300" name="meta[content_custom_style]"><?php echo htmlentities($gs_scene->meta['content_custom_style']); ?></textarea></td></tr>
			
	</table>
	
	<table class="gs_basic_datatable" style="margin-top: 10px;">
		<tr class="labelrow"><td colspan="3">Content Area Background<small><br>Repeating images cause Internet Explorer animations to be jumpy. The background should at the very least be a solid color, or and image that does not repeat, or repeats as little as possible</small></td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign="middle">color:</td><td colspan="2"><input type="radio" name="meta[content_bgc_transparent]" value="0" <?php echo ($gs_scene->meta['content_bgc_transparent'] != 1? 'checked = "checkeck"':'');?><input id ="content_bgc" name="meta[content_bgc]" type="text" class="px90" value="<?php echo $gs_scene->meta['content_bgc']; ?>"> <label for="content_bgc_transparent">&nbsp;Transparent&nbsp;</label><input type="radio" name="meta[content_bgc_transparent]" value="1" <?php echo ($gs_scene->meta['content_bgc_transparent'] == 1? 'checked = "checkeck"':'');?>/></td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign="middle">image:</td><td colspan="2"><input class="button-secondary" type="button" value="Clear Image" onclick = "jQuery('#content_bgsrc').trigger('clear_image');"/><input type="text" id = "content_bgsrc" name="meta[content_bgsrc]" class="gswpimage_add_image_trigger"	role="content_bgsrc" value="<?php echo htmlentities($gs_scene->meta['content_bgsrc']); ?>"></div></td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign="middle">x position: <small><span class="gs_info" title="pixels or left, right, center">?</span></small>:</td><td colspan="2"><input type="text" class="px90" name="meta[content_bgx]"	value="<?php echo htmlentities($gs_scene->meta['content_bgx']); ?>"></td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign="middle">y position: <small><span class="gs_info" title="pixels or top, bottom, center">?</span></small></td><td colspan="2"><input type="text" class="px90" name="meta[content_bgy]"	value="<?php echo htmlentities($gs_scene->meta['content_bgy']); ?>"></td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign="middle">repeat:</td><td colspan="2"><select name="meta[content_bgrepeat]"><?php echo gs_build_option_selection($gs_scene->meta['content_bgrepeat'],$gsp->repeat_options); ?></select></td></tr>
			
	</table>

</td><td style="width: 400px; padding: 5px;" valign="top"><!-- page indicator settings -->

	<table class="gs_basic_datatable" style="margin-top: 10px;" >
		<tr class="labelrow"><td colspan="3">Caption</td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign="middle">Display:</td><td colspan="2"><select name="meta[caption_1_display]"><?php echo gs_build_option_selection($gs_scene->meta['caption_1_display'],$gsp->display_options); ?></select></td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign="middle">Dimensions:</td><td align="center">Width (px):<br><input type="text" class="px90" name="meta[caption_1_width]" 		value="<?php echo htmlentities($gs_scene->meta['caption_1_width']); ?>"></td>
															<td align="center">Height (px):<br><input type="text" class="px90" name="meta[caption_1_height]"	value="<?php echo htmlentities($gs_scene->meta['caption_1_height']); ?>"></td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign="middle">margin (px)</td><td colspan="2"><input type="text" class="px90" name="meta[caption_1_margin]"	value="<?php echo htmlentities($gs_scene->meta['caption_1_margin']); ?>"></td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign="middle">pading (px)</td><td colspan="2"><input type="text" class="px90" name="meta[caption_1_padding]"	value="<?php echo htmlentities($gs_scene->meta['caption_1_padding']); ?>"></td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign="middle"><span class="gs_info" title="Add as many additional class names as you want, seperated by spaces">Additional Class Names</span></td><td colspan="2"><input type="text" class="px150" name="meta[caption_1_class_names]"	value="<?php echo htmlentities($gs_scene->meta['caption_1_class_names']); ?>"></td></tr>
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign="middle" colspan="3"><span class="gs_info" title="Valid CSS style info that will be inserted into the style tag of the div wrapping the content area">Custom Styles</span><br><small>This must be valid CSS or it may break your entire scene</small><br><textarea class="px300" name="meta[caption_1_custom_style]"><?php echo htmlentities($gs_scene->meta['caption_1_custom_style']); ?></textarea></td></tr>
	</table>
	<table class="gs_basic_datatable" style="margin-top: 10px;" width = "100%">
		<tr class="<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign="middle">
		Caption Content
		
		<textarea class="px300" title ="click to edit" id = "caption_1" name="meta[caption_1]" ><?php echo htmlentities($gs_scene->meta['caption_1']); ?></textarea></td></tr><!-- onclick = "show_mytinymce(this);" -->
		<?php print gs_basic_tinymce('caption_1','advanced'); ?>
	</table>


</td>
</tr></table>






<div class="clear"></div>
<input type="button" class="button-primary" style="margin-top: 10px;" value="<?php _e('Save Changes') ?>" onclick = "tinyMCE.triggerSave();jQuery(this).parents('.result_target').trigger('do_update');"/>

<script>
jQuery(document).ready( function () {
	
	var s_bgc 			= new jscolor.color(jQuery('#s_bgc').get(0),true);
	var content_bgc 	= new jscolor.color(jQuery('#content_bgc').get(0),true);
	var content_borderc = new jscolor.color(jQuery('#content_borderc').get(0),true);
	
	init_gswpimage_image_uploader('Select a An Image');
	jQuery('.gswpimage_add_image_trigger').bind('img_update', function (e,data) { update_main_pres_image(this,data);} );
	jQuery('.gswpimage_add_image_trigger').bind('clear_image', function () { clear_main_pres_image(this);} );


	function update_main_pres_image(o,data)
		{
			jQuery('#' + jQuery(o).attr("role") ).val()
			if (typeof(data.wp_image) != 'undefined')
				{
					jQuery('#' + jQuery(o).attr("role") ).val(data.wp_image);
				}
			else
				{
					jQuery('#' + jQuery(o).attr("role") ).val(data.url);
				}
				
			jQuery(o).html('<img src = "'+data.url+'"/>');
		}
		
	function clear_main_pres_image(o)
		{
			jQuery('#' + jQuery(o).attr("role") ).val('');
			jQuery(o).html('<?php echo $clear_image_string; ?>');
		}
		
	// link type selector.
	jQuery('#s_link_type').trigger('change');
		
});
</script>

<script>
function show_mytinymce(t)
	{
		if (typeof(t) != 'object')
		{
			t = jQuery('#'+t).get(0);
		}
		
		jQuery.data( jQuery('body').get(0) ,'the_content' ,jQuery(t).val() );
		jQuery('body').bind('tiny_mce_result', function (e,the_content) { jQuery(t).val(the_content); } );
		gs_pop_window('<iframe style="width: 600px; height: 400px; overflow: hidden;" src = "<?php echo ADMIN_COOKIE_PATH; ?>/admin.php?page=gs_tiny_mce"></iframe>');
	}
</script>
<?php
//_pre ($gs_scene);

//print_r(get_defined_constants());