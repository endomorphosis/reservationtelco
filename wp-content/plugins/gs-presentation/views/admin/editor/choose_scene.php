<?php

	if (!is_admin()) { die(); }
	
	$gsp_UID	= $_REQUEST['gsp_UID'];
	
	if (!is_admin()) { die(); }
	
	$gsp	= new GsPresentation();

	if (isset($_POST['new_scene']) &&  !gs_prevent_repost($_POST['post_once']) )
		{
			$new_scene_UID = $gsp->add_scene($gsp_UID,"New Scene ".Date('Y-m-d'));
			if (is_numeric($_POST['copy_from']) ) { $gsp->copy_scene($_POST['copy_from'],$new_scene_UID); }
		}
		
	if (isset($_POST['a']) &&  !gs_prevent_repost($_POST['post_once']) )
		{
			switch($_POST['a'])
				{
					case 'do_drop_scene':
						{
							$gsp->drop_scene($_POST['scene_UID']);
						}
						
					case 'gs_scene_ord':
						{
							$gsp->scene_order($_POST['dir'],$_POST['scene_UID']);
						}
				}
			
		}

	$post_once	= microtime();
	$all_scenes = $gsp->getall_secenes($gsp_UID);

?>	


<div style ="display:none;">
		<br>view:<?php print __FILE__;?>
		<br>this is only to force the comment to display. This div serves no purpose.
</div>

<div class="wrap gsp_editor-wrap">
	<div id="icon-options-general" class="icon32"></div><h2>Scene Editor</h2>

	<br />

	<form method = "POST"><input type="hidden" name="post_once" value="<?php echo $post_once ?>" /><input type="hidden" name="gsp_UID" value="<?php echo $gsp_UID ?>" /><input type="submit" class="button-primary" name="new_scene" value="<?php _e('Create New Scene') ?>" />

	<?php
		
		if (is_array($all_scenes))
			{
				print '<select name="copy_from"><option value="">Copy from Existing Scene</option>';
				foreach ($all_scenes as $scene)
					{
						print "\n".'<option value="'.$scene->scene_UID.'">'.htmlentities($scene->scene_comment).'</option>';
					}
				print '</select>';
			}
	?>

	</form>
	
	<br />
	
	<table class="gs_basic_datatable" style="width: 100%;">
	<tr class="labelrow">
		<td>Comment/Name</td><td>Active/Not Active</td><td>Action</td></tr>
	
	<?php
		
		if (!is_array($all_scenes) || count ($all_scenes) < 1)
			{
				print '<tr><td colspan = "3"><p style="text-align: center;">No Scenes Configured</p></td></tr>';
			}
		else
			{
				foreach ($all_scenes as $scene)
					{
						print '<tr class="'. ($rc++ % 2 == 0? ' alt': '') .'"><td >'.htmlentities($scene->scene_comment).'</td><td>' . ($scene->is_active == 1? 'Active' : 'Not Active') . '</td><td style="width: 100px;white-space: nowrap;" title="view/edit" ><a href = "'.$_SERVER['REQUEST_URI'].'&scene_UID='.$scene->scene_UID.'"><div class="gs_basic_aic mag"></div></a><input type="button" class="gs_basic_aic up" title="Move up in display order" style="margin-left: 2px;" onclick = "gs_scene_ord(-1,'.$scene->scene_UID.',\''.$post_once.'\')" /><input type="button" class="gs_basic_aic down" title="Move down in display order" style="margin-left: 2px;" onclick = "gs_scene_ord(1,'.$scene->scene_UID.',\''.$post_once.'\')" /><input type="button" title="Delete scene" class="gs_basic_aic close" style="margin-left: 2px;" onclick = "gs_pres_delete_scene('.$scene->scene_UID.')" /></div></td></tr>';
					}
			}
	?>
	</table>
</div>