<?php

	if (!is_admin()) { die(); }
	
	$gsp	= new GsPresentation();
	
	$scene_UID	= $_REQUEST['scene_UID'];
	$gsp_UID	= $_REQUEST['gsp_UID'];
	
	$gs_main = $gsp->get_main($gsp_UID);
	$gs_scene = $gsp->get_scene($scene_UID);
	
	print gs_basic_que_tinymce();
	
?>

<div style ="display:none;">
		<br>view:<?php print __FILE__;?>
		<br>this is only to force the comment to dislpay. This div serves no purpose.
</div>

<div class="wrap gsp_editor-wrap">
<div id="icon-options-general" class="icon32"></div><h2>Scene Editor</h2>
<p>Use these tabs to configure your presentation</p>

	<ul class = "gs_basic_nav gs_presentation_admin">
		<li class = "nav-tab" role = "<?php echo get_bloginfo( 'url') .'/wp-admin/admin.php?page=presentation-editor&gsp_UID='.$gsp_UID; ?>"><?php echo htmlentities($gs_main->gsp_comment); ?></li>
		<li class = "nav-tab" role = "<?php echo get_bloginfo( 'url') .'/wp-admin/admin.php?page=scene-editor&gsp_UID='.$gsp_UID; ?>">Scenes</li>
		<li class = "nav-tab is_init" role = "scene"><?php echo htmlentities($gs_scene->scene_comment); ?></li>
	</ul>

<div style = "clear: both;"></div>
<div class = "gs_presentation_admin flash_target" ><!-- this is the target for status/result messages.--></div>
<div style = "clear: both;"></div>
<div class = "gs_presentation_admin result_target" role = '{"scene_UID":"<?php echo $scene_UID; ?>"}'>
	<p>loading.....</p>
	
</div><!-- end of gsp_admin_ajax_target -->

</div><!-- end of gsp_editor-wrap -->
<script>
	jQuery(document).ready( function () { var gsp_nav	= new GsBasciSimpleAjaxNav( 'gs_presentation_admin' ); gsp_nav.listen();});
</script>
