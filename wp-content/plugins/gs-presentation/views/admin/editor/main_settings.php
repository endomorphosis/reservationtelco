<?php

	if (!is_admin()) { die(); }
	
	$gsp_UID =	$_POST['gsp_UID'];
	$gs_main = $gsp->get_main($gsp_UID);
	
	$w		=	$gs_main->meta['m_width'];
	$h		=	$gs_main->meta['m_height'];
	$cx		= 	$gs_main->m_cx;
	$cy		=   $gs_main->m_cy;
	
	$clear_image_string	=	'<button class="button-primary">Add Image</button>';

?> 

<div style ="display:none;">
		<br>view:<?php print __FILE__;?>
		<br>this is only to force the comment to dislpay. This div serves no purpose.
</div>

	<div id="gsp_presentation_main_wrap" style="<?php echo ' margin: 20px 0 50px -'.$cx.'px; left: 50%;'?>">
		<div id="gsp_main_demo" style="<?php echo $gs_main->m_style; ?>">
			
		<div id="hcursor" style="top: <?php echo $cy;?>px; left: 0px; width: <?php echo $w;?>px;">0px</div>
		<div id="vcursor" style="top: 0px; left: <?php echo $cx; ?>px; height: <?php echo $h;?>px;">0px</div>
			
		</div><!-- end of gsp_main_demo -->
		
		<div class = "gsp_prev_next_btn L" style="<?php echo $gs_main->pre_next_left_style;?>"></div><div class = "gsp_prev_next_btn R" style="<?php echo $gs_main->pre_next_right_style;?>" ></div>
		<div class = "slide_tray_wrap" style="<?php echo $gs_main->slide_tray_style; ?>">
		
		<table class = "slide_tray"><tr>
					<td class = "L" style="<?php echo $gs_main->slide_tray_L_style;?>">&nbsp;</td>
					
					<?php $all_scenes = $gsp->getall_secenes($gsp_UID,true);
						$scene_count = (count($all_scenes) > 0? count($all_scenes) : 1 );
						$is_active = 'is_active';
						while ($scene_count-- > 0)
						{?>
							<td class = "M"  style="<?php echo $gs_main->slide_tray_M_style;?>">
								<div class = "indicator <?php echo $is_active; ?>" style ="<?php echo $gs_main->curr_slide_ind_style; ?>"></div>
							</td>
					<?php
						$is_active = '';
						} ?>
					
					<td class = "R"  style="<?php echo $gs_main->slide_tray_R_style;?>">&nbsp;</td></tr>
		</table>
		
		</div><!-- curr_slide_tray_wrap -->
	</div><!-- end of gsp_presentation_main_wrap -->
<div class = "clear"></div>
<input type="button" class="button-primary" style="margin-top: 10px;" value="<?php _e('Save Changes') ?>" onclick="jQuery(this).parents('.result_target').trigger('do_update');"/>
<div class = "clear"></div>
<table><tr><td style="width: 400px; border-right: 2px dotted #CCC; padding: 5px;" valign = "top"><!-- main settings -->

	<table class = "gs_basic_datatable" style="margin-top: 10px;">
		<tr class = "labelrow"><td colspan = "3">Main Settings</td></tr>
		<tr class = "<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign = "middle">Comment:</td><td colspan = "2"><input type="text" class = "px300" name="gsp_comment"	value="<?php echo htmlentities($gs_main->gsp_comment); ?>"></td></tr>
		<tr class = "<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign = "middle">Dimensions:</td>	<td align = "center">Width (px):<br><input type="text" class = "px90" name="meta[m_width]" 		value="<?php echo htmlentities($gs_main->meta['m_width']); ?>"></td>
												<td align = "center">Height (px):<br><input type="text" class = "px90" name="meta[m_height]"	value="<?php echo htmlentities($gs_main->meta['m_height']); ?>"></td></tr>
		<tr class = "<?php echo ($alt++ % 2 == 0? ' alt':''); ?>" ><td valign = "middle">Border:</td>	<td colspan = "2"><select name="meta[m_borderw]"><?php $idx = -1; while ($idx++ < 100){echo '<option value="'.$idx.'" '. ($idx == $gs_main->meta['m_borderw']? 'selected="selected"' : '') .'>'.$idx.'px</option>';}?></select><select name="meta[m_bordert]"><?php echo gs_build_option_selection($gs_main->meta['m_bordert'],$gsp->border_options); ?></select><input id ="m_borderc" name="meta[m_borderc]" type="text" class="px90" value="<?php echo $gs_main->meta['m_borderc']; ?>"></td></tr>
	</table>
	
	<table class = "gs_basic_datatable" style="margin-top: 10px;">
		<tr class = "labelrow"><td colspan = "2">Main Background<small><br>Repeating images cause Internet Explorer animations to be jumpy. The background should at the very least be a solid color, or and image that does not repeat, or repeats as little as possible</small></td></tr>
		<tr class = "<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign = "middle">color:</td><td><input type="radio" name="meta[m_bgc_transparent]" value="0" <?php echo ($gs_main->meta['m_bgc_transparent'] != 1? 'checked="checkeck"':'');?><input id ="m_bgc" name="meta[m_bgc]" type="text" class="px90" value="<?php echo $gs_main->meta['m_bgc']; ?>"> <label for="m_bgc_transparent">&nbsp;Transparent&nbsp;</label><input type="radio" name="meta[m_bgc_transparent]" value="1" <?php echo ($gs_main->meta['m_bgc_transparent'] == 1? 'checked="checkeck"':'');?>/></td></tr>
		<tr class = "<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign = "middle">image:</td><td><input class="button-secondary" type="button" value="Clear Image" onclick="jQuery('#m_bgsrc').trigger('clear_image');"/><input type="text" id="m_bgsrc" name="meta[m_bgsrc]" class = "gswpimage_add_image_trigger"	role="m_bgsrc" value="<?php echo htmlentities($gs_main->meta['m_bgsrc']); ?>"></div></td></tr>
		<tr class = "<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign = "middle">x position: <small><span class = "gs_info" title="pixels or left, right, center">?</span></small>:</td><td><input type="text" class = "px90" name="meta[m_bgx]"	value="<?php echo htmlentities($gs_main->meta[m_bgx]); ?>"></td></tr>
		<tr class = "<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign = "middle">y position: <small><span class = "gs_info" title="pixels or top, bottom, center">?</span></small></td><td><input type="text" class = "px90" name="meta[m_bgy]"	value="<?php echo htmlentities($gs_main->meta[m_bgy]); ?>"></td></tr>
		<tr class = "<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign = "middle">repeat:</td><td><select name="meta[m_bgrepeat]"><?php echo gs_build_option_selection($gs_main->meta['m_bgrepeat'],$gsp->repeat_options); ?></select></td></tr>
			
	</table>
</td><!-- end of main settings col -->

<td style="width: 400px; border-right: 2px dotted #CCC; padding: 5px;" valign = "top"><!-- prev/next settings -->
	<table class = "gs_basic_datatable" style="margin-top: 10px;" width = "100%">
		<tr class = "labelrow"><td colspan = "3">Prev &amp; Next Button Settings</td></tr>
		<tr class = "<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign = "middle">Dimensions:</td>	<td align = "center">Width (px):<br><input type="text" class = "px90" name="meta[prev_next_width]" 		value="<?php echo htmlentities($gs_main->meta['prev_next_width']); ?>"></td>
												<td align = "center">Height (px):<br><input type="text" class = "px90" name="meta[prev_next_height]"	value="<?php echo htmlentities($gs_main->meta['prev_next_height']); ?>"></td></tr>
		
		<tr class = "<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign = "middle"><span class = "gs_info" title="An offset in pixels to move the x centerline away from the main centerline. Use this to line the buttons up vertically to the left or right">x,0 Offset (px):</span></td><td colspan = "2"><input type="text" class = "px90" name="meta[prev_next_x0offset]"	value="<?php echo htmlentities($gs_main->meta['prev_next_x0offset']); ?>"></td></tr>
		<tr class = "<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign = "middle"><span class = "gs_info" title="An offset in pixels to move the y centerline away from the main centerline. Use this make the buttons on the same x line, but off to the left or right corner, etc">y,0 Offset (px):</span></td><td colspan = "2"><input type="text" class = "px90" name="meta[prev_next_y0offset]"	value="<?php echo htmlentities($gs_main->meta['prev_next_y0offset']); ?>"></td></tr>
		<tr class = "<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign = "middle"><span class = "gs_info" title="the absolute distance from the center of the button to the x 0 line. This is half of the horizontal distance between the two buttons">Dx (px):</span></td><td colspan = "2"><input type="text" class = "px90" name="meta[prev_next_dx]"	value="<?php echo htmlentities($gs_main->meta['prev_next_dx']); ?>"></td></tr>
		<tr class = "<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign = "middle"><span class = "gs_info" title="the absolute distance from the center of the button to the y 0 line. This is half of the vertical distance between the two buttons">Dy (px):</td><td colspan = "2"><input type="text" class = "px90" name="meta[prev_next_dy]"	value="<?php echo htmlentities($gs_main->meta['prev_next_dy']); ?>"></td></tr>
		<tr class = "<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign = "middle">Previous/Next Image:<br><input class="button-secondary" type="button" value="Clear Image" onclick="jQuery('#prev_next_img').trigger('clear_image');"/></td><td colspan = "2"><input type="hidden" id="prev_next_src" name="meta[prev_next_src]"	value="<?php echo htmlentities($gs_main->meta['prev_next_src']); ?>"><div style="float: right;" title="Click here to change the image." id="prev_next_img" class = "gswpimage_add_image_trigger" role="prev_next_src"><?php echo (strlen($gs_main->meta['prev_next_url']) > 0? '<img src = "' . $gs_main->meta['prev_next_url'] .'">' :  $clear_image_string);?></div></td></tr>
	</table>
</td><td style="width: 400px; padding: 5px;" valign = "top"><!-- page indicator settings -->

	<table class = "gs_basic_datatable" style="margin-top: 10px;" width = "100%">
		<tr class = "labelrow"><td colspan = "3">Current Slide Area</td></tr>
		<tr class = "<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign = "middle">Dimensions:</td>	<td align = "center">Width (px):<br>(automatic)</td>
												<td align = "center">Height (px):<br><input type="text" class = "px90" name="meta[slide_tray_height]"	value="<?php echo htmlentities($gs_main->meta['slide_tray_height']); ?>"></td></tr>
		<tr class = "<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign = "middle">x (px):</td><td colspan = "2"><input type="text" class = "px90" name="meta[slide_tray_x]"	value="<?php echo htmlentities($gs_main->meta['slide_tray_x']); ?>"></td></tr>
		<tr class = "<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign = "middle">y (px):</td><td colspan = "2"><input type="text" class = "px90" name="meta[slide_tray_y]"	value="<?php echo htmlentities($gs_main->meta['slide_tray_y']); ?>"></td></tr>
		<tr class = "<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign = "middle">padding (left/right px)</td><td colspan = "2"><input type="text" class = "px90" name="meta[slide_tray_end_padding]"	value="<?php echo htmlentities($gs_main->meta['slide_tray_end_padding']); ?>"></td></tr>
		<tr class = "<?php echo ($alt++ % 2 == 0? ' alt':''); ?>">
			<td valign = "middle" colspan = "3">
				Current Slide Area Background:&nbsp;
				<input type="hidden" id="slide_tray_src" name="meta[slide_tray_src]" value="<?php echo htmlentities($gs_main->meta['slide_tray_src']); ?>" />
				<div id="slide_tray_img" style="float: right;" title="Click here to change the image." class = "gswpimage_add_image_trigger" role="slide_tray_src">
					<?php echo (strlen($gs_main->meta['slide_tray_url']) > 0? '<img src = "' . $gs_main->meta['slide_tray_url'] .'">' :  $clear_image_string);?>
				</div>
				<input type="button" class="button-secondary" value="Clear Image" onclick="jQuery('#slide_tray_img').trigger('clear_image');"/>
		</td>
		</tr>
	</table>
	
	<table class = "gs_basic_datatable" style="margin-top: 10px;" width = "100%">
		<tr class = "labelrow"><td colspan = "3">Current Slide Indicator</td></tr>
		<tr class = "<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign = "middle">Dimensions:</td>	<td align = "center">Width (px):<br><input type="text" class = "px90" name="meta[curr_slide_ind_width]" 		value="<?php echo htmlentities($gs_main->meta['curr_slide_ind_width']); ?>"></td>
												<td align = "center">Height (px):<br><input type="text" class = "px90" name="meta[curr_slide_ind_height]"	value="<?php echo htmlentities($gs_main->meta['curr_slide_ind_height']); ?>"></td></tr>
		<tr class = "<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign = "middle">Margin-top: (px)</td><td colspan = "2"><input type="text" class = "px90" name="meta[curr_slide_ind_margin_top]"	value="<?php echo htmlentities($gs_main->meta['curr_slide_ind_margin_top']); ?>"></td></tr>
		<tr class = "<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign = "middle">Margin-left/right: (px)</td><td colspan = "2"><input type="text" class = "px90" name="meta[curr_slide_ind_margin_left]"	value="<?php echo htmlentities($gs_main->meta['curr_slide_ind_margin_left']); ?>"></td></tr>
		<tr class = "<?php echo ($alt++ % 2 == 0? ' alt':''); ?>"><td valign = "middle">Current Slide Indicator Image:<br><input type="button" class="button-secondary" value="Clear Image" onclick="jQuery('#slide_ind_img').trigger('clear_image');"/></td><td colspan = "2"><input type="hidden" id="slide_ind_src" name="meta[slide_ind_src]"	value="<?php echo htmlentities($gs_main->meta['slide_ind_src']); ?>"><div id="slide_ind_img" style="float: right;" title="Click here to change the image." class = "gswpimage_add_image_trigger" role="slide_ind_src"><?php echo (strlen($gs_main->meta['slide_ind_url']) > 0? '<img src = "' . $gs_main->meta['slide_ind_url'] .'">' :  $clear_image_string);?></div></td></tr>
	</table>

</td>
</tr></table>







<div class = "clear"></div>
<input type="button" class="button-primary" style="margin-top: 10px;" value="<?php _e('Save Changes') ?>" onclick="jQuery(this).parents('.result_target').trigger('do_update');"/>

<script>
jQuery(document).ready( function () {
	
	var m_borderc 	= new jscolor.color(jQuery('#m_borderc').get(0),true);
	var m_bgc 		= new jscolor.color(jQuery('#m_bgc').get(0),true);
	
	
	init_gswpimage_image_uploader('Select a An Image');
	jQuery('.gswpimage_add_image_trigger').bind('img_update', function (e,data) { update_main_pres_image(this,data);} );
	jQuery('.gswpimage_add_image_trigger').bind('clear_image', function () { clear_main_pres_image(this);} );

	} );

	function update_main_pres_image(o,data)
		{
			jQuery('#' + jQuery(o).attr("role") ).val()
			if (typeof(data.wp_image) != 'undefined')
				{
					jQuery('#' + jQuery(o).attr("role") ).val(data.wp_image);
				}
			else
				{
					jQuery('#' + jQuery(o).attr("role") ).val(data.url);
				}
				
			jQuery(o).html('<img src = "'+data.url+'"/>');
		}
		
	function clear_main_pres_image(o)
		{
			jQuery('#' + jQuery(o).attr("role") ).val('');
			jQuery(o).html('<?php echo $clear_image_string; ?>');
		}
</script>
<?php //_pre($gs_main); 
