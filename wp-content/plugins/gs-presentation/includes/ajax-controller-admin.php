<?php

/*
	==================================================================
		Admin Ajax Controller Functions.
		
	==================================================================  */
	
function wp_ajax_gs_presentation_admin()
	{
		if ( !is_admin() ) { die; }
		$gsp			= new GsPresentation();
		$flash_message	= '';
		switch($_POST['a'])
			{
				
				/*
					==================================================================
						Admin views Edit function
						
					==================================================================  */
					case'do_main_settings':
						{
							$gsp->update_m($_POST['gsp_UID'],array_merge(array('gsp_comment' => $_POST['gsp_comment']),$_POST['meta']) );
							$flash_message = gs_basic_flash_updated('Saved');//."<br>"._pre($_POST,1);
						} // no break, goes on to display presentation. . 
					case'main_settings':
						{
							ob_start();
							include_once(PLUGIN_PATH.'/gs-presentation/views/admin/editor/main_settings.php');
							$return	= ob_get_contents();
							ob_clean();
							echo json_encode(array( 'result' => $return, 'flash_message' => $flash_message));
						}break;
						
						
					case'do_scene':
						{
							if ($gsp->update_s($_POST['scene_UID'],array_merge(array('scene_comment' => $_POST['scene_comment'],'is_active' => $_POST['is_active']),$_POST['meta']) ))
								{ $flash_message = gs_basic_flash_updated('Saved'); }
							else
								{
								 $flash_message = gs_basic_flash_error('Nothing has been saved or changed.');
								}
						}// no break goes on to see scene.
					case'scene':
						{
							ob_start();
							include_once(PLUGIN_PATH.'/gs-presentation/views/admin/editor/scene_editor.php');
							$return	= ob_get_contents();
							ob_clean();
							echo json_encode(array( 'result' => $return, 'flash_message' => $flash_message));
						}break;
						
					case'tiny_mce':
						{
							ob_start();
							the_editor("test");
							$return = ob_get_contents();
							ob_clean();
							print $return;
							die();
						}
						
						
						
						
			/*
				==================================================================
					Default, no function exist.
				==================================================================  */
				default:
				{
					echo json_encode(array( 'flash_message' => gs_basic_flash_error("No function ".$_POST['a']." exists in ".basename(__file__)." line ".__line__)));
				}break;
			}
		die();
	}