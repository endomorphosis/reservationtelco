<?php

/*
	==================================================================
		Main presentation editor
	==================================================================  */
	function show_gs_presentation_editor()
		{
			if (isset($_GET['gsp_UID']) && is_numeric($_GET['gsp_UID']) )
				{
					include_once(PLUGIN_PATH.'/gs-presentation/views/admin/editor/index.php');
				}
			else
				{
					include_once(PLUGIN_PATH.'/gs-presentation/views/admin/editor/choose_presentation.php');
				}
		}
		
	/*
	==================================================================
			The Docs Viewer
	==================================================================  */
	function show_gs_presentation_docs()
		{
			include_once(PLUGIN_PATH.'/gs-presentation/views/admin/docs/docs.php');
		}
		
/*

	==================================================================
		Scene  editor
	==================================================================*/
	function show_gs_scene_editor()
		{
			if (isset($_GET['gsp_UID']) && is_numeric($_GET['gsp_UID']) && is_numeric($_GET['scene_UID']))
				{
					include_once(PLUGIN_PATH.'/gs-presentation/views/admin/editor/scene_index.php');
				}
			else
				{
					include_once(PLUGIN_PATH.'/gs-presentation/views/admin/editor/choose_scene.php');
				}
		}
/*	
function ShowTinyMCE() {
		// conditions here
		if ($_REQUEST['page'] !== 'gs_tiny_mce') { return; }
		
		wp_enqueue_script( 'common' );
		wp_enqueue_script( 'jquery-color' );
		if (function_exists('add_thickbox')) add_thickbox();
		wp_print_scripts('media-upload');
		if (function_exists('wp_tiny_mce')) wp_tiny_mce();
		wp_admin_css();
		wp_enqueue_script('utils');
		wp_print_scripts('editor');
		do_action("admin_print_styles-post-php");
		do_action('admin_print_styles');
		}

	function do_tinymce() {
		ob_start();
		the_editor("loading");
		$result = ob_get_contents();
		ob_clean();
		
		print '<div style = "z-index: 10;  position: fixed; left: 0px; top: 0px; width: 100%; background-color: #FFF; height: 100%;">
					<div id = "poststuff" style = "position: absolute; left: 0; top: 0; width: 600px; height: 400px;">'.$result.'
					<input type="button" class="button-primary" style = "margin-top: 10px;" value="Save" onclick = "tiny_mce_save_send_close(this);" />
					</div>
				</div>';
				?>
				<script>
				jQuery('[name=content]').val( parent.jQuery.data(parent.jQuery('body').get(0),'the_content') );
				function tiny_mce_save_send_close(o)
					{
						tinyMCE.triggerSave();
						parent.jQuery('body', top.document).trigger('tiny_mce_result', jQuery('[name=content]').val() );
						parent.gs_pop_window_destroy();
					}
				</script>
				<?php 
	}
	
*/
	
	
/*
	==================================================================
		Short code method
	==================================================================  */
	function gs_presentation_short_code($args)
		{
			if (!is_numeric($args[0])) { return gs_basic_flash_error("no valid presentation id in short code"); }
			
			$presentation = gs_presentation($args[0],0);
			
			if (!$presentation || strlen($presentation) < 1 ) { return gs_basic_flash_error("no valid presentation returned by short code"); }
			
			return $presentation;
		}
		
/*
	==================================================================
		direct call method
	==================================================================  */
	function gs_presentation($gsp_UID,$print_output = 1)
		{
			if (!is_numeric($gsp_UID)) { return false; }
			
			$gsp	= new GsPresentation();
			
			$presentation	= $gsp->build($gsp_UID);
			
			if ($print_output == 0) { return $presentation; }
			
			print $presentation;
			
		}