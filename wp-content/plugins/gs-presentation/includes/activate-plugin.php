<?php

function gs_presentation_activate()
{

		   require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		   global $wpdb;
		   $results = $wpdb->get_results("show tables like '" . GS_PRE_PREFIX . "%'");
		   $table_hash = array(
						'presentation' =>
											'CREATE TABLE `'.GS_PRE_PREFIX.'presentation` (
											`gsp_UID` int(11) NOT NULL auto_increment,
											`gsp_comment` varchar(45) NOT NULL default "New Presentation",
											PRIMARY KEY  (`gsp_UID`)
											) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1000 ;',
											
						'presentatino_meta'	=>
											'CREATE TABLE `'.GS_PRE_PREFIX.'presentationmeta` (
												`gspmeta_UID` int(11) NOT NULL auto_increment,
												`gsp_UID` int(11) NOT NULL,
												`meta_key` varchar(45) NOT NULL,
												`meta_value` text NOT NULL,
												PRIMARY KEY  (`gspmeta_UID`),
												KEY `presentation_UID` (`gsp_UID`)
										  ) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;',
										  
						'scene' =>
											'CREATE TABLE `'.GS_PRE_PREFIX.'scene` (
											`scene_UID` int(11) NOT NULL auto_increment,
											`gsp_UID` int(11) NOT NULL,
											`scene_order` int(11) NOT NULL,
											`scene_comment` varchar(45) NOT NULL,
											`is_active` int(11) NOT NULL default 0,
											PRIMARY KEY  (`scene_UID`),
											KEY `gsp_UID` (`gsp_UID`)
										  ) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;',
										  
						'scenemeta' =>
											'CREATE TABLE `'.GS_PRE_PREFIX.'scenemeta` (
											  `scenemeta_UID` int(11) NOT NULL auto_increment,
												`scene_UID` int(11) NOT NULL,
												`meta_key` varchar(45) NOT NULL,
												`meta_value` text NOT NULL,
												PRIMARY KEY  (`scenemeta_UID`),
												KEY `scene_UID` (`scene_UID`)
											  ) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;'
					  
		   );
		   
		   foreach($table_hash as $table => $sql)
					  {
								 print "check for table $table exist: ".$results->$table;
								 //if (!is_null($results->$table)) { continue; }

								 dbDelta($sql);
					  }

}