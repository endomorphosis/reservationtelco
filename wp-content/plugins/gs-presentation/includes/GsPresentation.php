<?php

class GsPresentation {
	
/*
	==================================================================
		Class Functions
		
	==================================================================  */

	/*
		==================================================================
			vars
		==================================================================  */
	
	public $default_presentation = array(
		'm_width'	=> 300,
		'm_height'	=> 200,
		'm_borderw'	=> 2,
		'm_bordert'	=> 'solid',
		'm_borderc'	=> 'ffffff'
	);
	
	public $border_options = array(
		'solid'	=> 'solid',
		'dotted' => 'dotted',
		'dashed' => 'dashed'
	);
	
	public $repeat_options = array(
		'repeat'	=> 'repeat',
		'repeat-x'	=> 'repeat-x',
		'repeat-y'	=> 'repeat-y',
		'no-repeat'	=> 'no repeat'
	);
	
	public	$effect_in_options	=	array(
		'slide_BL'			=>	'From  Bottom Left ',
		'slide_L'			=>	'From  Left ',
		'slide_TL'			=>	'From  Top Left ',
		'slide_T'			=>	'From  Top ',
		'slide_TR'			=>	'From  Top Right',
		'slide_R'			=>	'From  Right ',
		'slide_BR'			=>	'From  Bottom Right ',
		'slide_B'			=>	'From  Bottom ',
		'blindDown'			=>	'Blind Down',
		'fade'				=>	'Fade'
	);
	
	public	$effect_out_options	=	array(
		'slide_BL'			=>	'To  Bottom Left ',
		'slide_L'			=>	'To  Left ',
		'slide_TL'			=>	'To  Top Left ',
		'slide_T'			=>	'To  Top ',
		'slide_TR'			=>	'To  Top Right',
		'slide_R'			=>	'To  Right ',
		'slide_BR'			=>	'To  Bottom Right ',
		'slide_B'			=>	'To  Bottom ',
		'blindUp'			=>	'Blind Up',
		'fade'				=>	'Fade'
	);
	
	public $status_options = array(
		0					=> 'not active',
		1					=> 'active'
	);
	
	public $display_options = array(
		0					=> 'do not display',
		1					=> 'display'
	);
	
	public $tab_options = array(
		'' => 'no tag', 'a' => 'a', 'b' => 'b','div' => 'div',
		'h1' => 'h1', 'h2' => 'h2', 'h3' => 'h3', 'h4' => 'h4',
		'p' => 'p', 'span' => 'span', 'small' => 'small'
	);
	
	public $link_types = array(
		'url'				=> 'Custom URL',
		'page'				=> 'Page',
		'post'				=> 'Post',
		'cat'				=> 'Category'
	);
	
	/*
	==================================================================
			Function to buld the presentation and return html. 
	==================================================================  */
	public function build($gsp_UID = null)
		{
			if (!is_numeric($gsp_UID)) { return false; }
			
			// get the main data
			$gs_main 	= $this->get_main($gsp_UID);
			if (!is_numeric($gs_main->gsp_UID)) { return false; }
			
			// get a list of all active scenes
			$all_scenes	= $this->getall_secenes($gsp_UID,true);
			$scenes	= array();
			
			if (is_array($all_scenes) && count($all_scenes) > 0 )
				{
					ob_start();
					$is_init = 1;
					foreach ($all_scenes as $scene)
						{
						  $gs_scene			= $this->get_scene($scene->scene_UID);
							//$gs_scene->is_init	= $is_init;
							
							$this->_scene_template($gs_scene);
							$scenes[$scene->scene_UID]	= ob_get_contents();
							ob_clean();
							$is_init = 0;
						}
					ob_flush();
				}
			
			ob_start();
			$this->_main_template($gs_main,$scenes);
			$presentation = ob_get_contents();
			ob_clean();
			return $presentation;
		}
		
		
/*
	==================================================================
		main init function. 
	==================================================================  */
	public function GsPresentation() {
		// main init
	}
	
	
	/*
		==================================================================
			List All Presentations, usually for the admin only. 
		==================================================================  */
		public function getall()
			{
				global $wpdb;
				$sql = "select * from ".GS_PRE_PREFIX."presentation order by gsp_comment";
				
				return $wpdb->get_results($sql);
				
			}
			
	/*
		==================================================================
			list all scenes of a specific presentation
		==================================================================  */
		public function getall_secenes($gsp_UID = null,$is_active = null)
			{
				if (!is_numeric($gsp_UID)) { return false; }
				global $wpdb;
				$sql = "select * from ".GS_PRE_PREFIX."scene where gsp_UID = %d ".($is_active === true? 'and is_active = 1 ' : '')."order by scene_order";
				return $wpdb->get_results($wpdb->prepare($sql,$gsp_UID));
			}
			
	/*
		==================================================================
			add new presentation
		==================================================================  */
		public function add_new($comment = 'Default Comment')
			{
				global $wpdb;
				$sql = "insert into ".GS_PRE_PREFIX."presentation (gsp_comment) values (%s)";
				$wpdb->query($wpdb->prepare($sql,$comment));
				
				$gs_UID =	$wpdb->insert_id;
				
				$this->update_m($gs_UID,$this->default_presentation);
			}
			
	/*
		==================================================================
			Add a new scene to existing presentation
		==================================================================  */
		public function add_scene($gsp_UID,$comment = 'New Scene')
			{
				$all_scenes = $this->getall_secenes($gsp_UID);
				if (is_array($all_scenes)) { $scene_order = count($all_scenes) - 1; }
				if (!is_array($all_scenes) || $scene_order < 1){ $scene_order = 1; }
				
				global $wpdb;
				$sql = "insert into ".GS_PRE_PREFIX."scene (scene_comment,gsp_UID,scene_order) values (%s,%d,%d)";
				$wpdb->query($wpdb->prepare($sql,$comment,$gsp_UID,$scene_order));
				return $wpdb->insert_id;
			}
		public function copy_scene($source_UID,$target_UID)
			{
				global $wpdb;
				$sql 	=	"select * from ".GS_PRE_PREFIX."scenemeta where scene_UID	= %d";
				$results	= $wpdb->get_results($wpdb->prepare($sql,$source_UID));
				if (!is_array($results) ) { return; }
				
				$sql = "insert into ".GS_PRE_PREFIX."scenemeta (scene_UID,meta_key,meta_value) values (%d,%s,%s)";
				foreach ($results as $scene_meta)
					{
						$wpdb->query($wpdb->prepare($sql,$target_UID,$scene_meta->meta_key,$scene_meta->meta_value));
					}
			}
			
	/*
		==================================================================
			get the contents of a presentation
		==================================================================  */
		public function get_main($gsp_UID = null)
			{
				if (!is_numeric($gsp_UID)) { return false; }
				
				global $wpdb;
				
				$sql		=	"select * from ".GS_PRE_PREFIX."presentation where gsp_UID = %d";
				$pres		=	$wpdb->get_row($wpdb->prepare($sql,$gsp_UID));
				
				$sql 		=	"select * from ".GS_PRE_PREFIX."presentationmeta where gsp_UID	= %d";
				$results	=	$wpdb->get_results($wpdb->prepare($sql,$gsp_UID));
				
				if (!is_array($results)) { return $pres; }
				
				foreach($results as $result)
					{
						$pres->meta[$result->meta_key]	= $result->meta_value;
					}
					
				if (strlen($pres->meta['m_bgsrc']))
					{
						$img_resource	= gs_basic_get_wp_image($pres->meta['m_bgsrc']);
						$pres->meta['m_bg_url']	= $img_resource->url;
					}
					
				// build the m_style
				
				$pres->m_style	=	" width: ".(int)($pres->meta['m_width'] > 1? $pres->meta['m_width'] : 100).'px;';
				$pres->m_style	.= 	" height: ".(int)($pres->meta['m_height'] > 1? $pres->meta['m_height'] : 100) + 100 .'px;';
				$pres->m_style	.= 	" border: ".($pres->meta['m_borderw'] > 0? (int)$pres->meta['m_borderw'].'px '.(strlen($pres->meta['m_bordert']) > 1? $pres->meta['m_bordert'] : 'solid').' #'.(strlen($pres->meta['m_borderc']) == 6? $pres->meta['m_borderc'] : 'CCCCCC') : '').';';
				$pres->m_style	.= 	" background-color: ".(strlen($pres->meta['m_bgc']) > 0 && $pres->meta['m_bgc_transparent'] != 1? '#'.$pres->meta['m_bgc'] : '').';';
				$pres->m_style	.= 	" background-image: ".(strlen($pres->meta['m_bg_url']) > 0? 'url('.$pres->meta['m_bg_url'].')' : 'none').';';
				$pres->m_style	.= 	" background-position: ".(is_numeric( $pres->meta['m_bgx'] )? $pres->meta['m_bgx'].'px ' : $pres->meta['m_bgx'].' ').(is_numeric( $pres->meta['m_bgy'] )? $pres->meta['m_bgy'].'px ' : $pres->meta['m_bgy'].' ').';';
				$pres->m_style	.= 	" background-repeat: ".$pres->meta['m_bgrepeat'].';';
				
				// calculate cx and cy
				$pres->m_cx		= 	(int)(  $pres->meta['m_width']/ 2);
				$pres->m_cy		=   (int)(  $pres->meta['m_height']/ 2);
	
				/*
					==================================================================
						Prev Next Images and calculattions.
					==================================================================  */
				if (strlen($pres->meta['prev_next_src']))
					{
						$img_resource	= gs_basic_get_wp_image($pres->meta['prev_next_src']);
						$pres->meta['prev_next_url']	= $img_resource->url;
					}
					
				// buld the style for the next prev image.
				// only if all variables are present.
				$pres->meta['prev_next_x0offset']	= (is_numeric($pres->meta['prev_next_x0offset'])? $pres->meta['prev_next_x0offset'] : 0);
				$pres->meta['prev_next_y0offset']	= (is_numeric($pres->meta['prev_next_y0offset'])? $pres->meta['prev_next_y0offset'] : 0);
				
				if (	$pres->meta['prev_next_width'] 			> 0
					&&	$pres->meta['prev_next_height'] 		> 0
					&&	strlen($pres->meta['prev_next_dx']) 	> 0
					&&	strlen($pres->meta['prev_next_dy']) 	> 0
					&&	strlen($pres->meta['prev_next_url'])	> 0
					)
					{
						
						$x0offest	= $pres->meta['prev_next_x0offset'];
						$y0offest	= $pres->meta['prev_next_y0offset'];
						
						$pres->pre_next_left_style	= " width: ".(int)$pres->meta['prev_next_width'].'px;';
						$pres->pre_next_left_style	.= " height: ".(int)$pres->meta['prev_next_height'].'px;';
						$pres->pre_next_left_style	.= " left: ".(int)((-1 * $pres->meta['prev_next_dx']) + $pres->m_cx - ((int)($pres->meta['prev_next_width'] / 2) ) + $x0offest).'px;';
						$pres->pre_next_left_style	.= " top: ".(int)((-1 * $pres->meta['prev_next_dy']) + $pres->m_cy - ((int)($pres->meta['prev_next_height'] / 2) ) + $y0offest).'px;';
						$pres->pre_next_left_style	.= " background-image: url(".$pres->meta['prev_next_url'].");";
						
						$pres->pre_next_right_style	= " width: ".$pres->meta['prev_next_width'].'px;';
						$pres->pre_next_right_style	.= " height: ".(int)$pres->meta['prev_next_height'].'px;';
						$pres->pre_next_right_style	.= " left: ".(int)($pres->meta['prev_next_dx'] + $pres->m_cx - ((int)($pres->meta['prev_next_width'] / 2) ) + $x0offest).'px;';
						$pres->pre_next_right_style	.= " top: ".(int)($pres->meta['prev_next_dy'] + $pres->m_cy - ((int)($pres->meta['prev_next_height'] / 2) ) + $y0offest).'px;';
						$pres->pre_next_right_style	.= " background-image: url(".$pres->meta['prev_next_url'].");";
						
					}
					else
					{
						$pres->pre_next_left_style	= "display: none;";
						$pres->pre_next_right_style	= "display: none;";
					}
					
				/*
				==================================================================
					slide tray calculations.
				==================================================================  */
				if (strlen($pres->meta['slide_tray_src']))
					{
						$img_resource	= gs_basic_get_wp_image($pres->meta['slide_tray_src']);
						$pres->meta['slide_tray_url']	= $img_resource->url;
					}
					
				if (	strlen($pres->meta['slide_tray_x']) 	> 0
					&&	strlen($pres->meta['slide_tray_y']) 	> 0
					&&	$pres->meta['slide_tray_height']		> 0
					)
					{
						$pres->slide_tray_style	 = " left: ".(int)($pres->meta['slide_tray_x'] + $pres->m_cx).'px;';
						$pres->slide_tray_style	.= " top: ".(int)($pres->meta['slide_tray_y'] + $pres->m_cy).'px;';
						$pres->slide_tray_style	.= " height: ".(int)$pres->meta['slide_tray_height'].'px;';
						
						$pres->slide_tray_L_style	 = " padding: 0 0 0 ".(int)$pres->meta['slide_tray_end_padding']."px;";
						$pres->slide_tray_L_style	.= " background-image: url(".$pres->meta['slide_tray_url'].");";
						$pres->slide_tray_L_style	.= " height: ".(int)$pres->meta['slide_tray_height'].'px;';
						
						$pres->slide_tray_R_style	 = " padding: 0 ".(int)$pres->meta['slide_tray_end_padding']."px 0 0;";
						$pres->slide_tray_R_style	.= " background-image: url(".$pres->meta['slide_tray_url'].");";
						$pres->slide_tray_R_style	.= " height: ".(int)$pres->meta['slide_tray_height'].'px;';
						
						$pres->slide_tray_M_style	 = " background-image: url(".$pres->meta['slide_tray_url'].");";
						$pres->slide_tray_M_style	.= " height: ".(int)$pres->meta['slide_tray_height'].'px;';
					}
					else
					{
						$pres->slide_tray_style	= "display: none;";
					}
					
				/*
				==================================================================
					current slide indicator
				==================================================================  */
				if (strlen($pres->meta['slide_ind_src']))
					{
						$img_resource	= gs_basic_get_wp_image($pres->meta['slide_ind_src']);
						$pres->meta['slide_ind_url']	= $img_resource->url;
					}
					
				if (	$pres->meta['curr_slide_ind_width'] 	> 0
					&&	$pres->meta['curr_slide_ind_height'] 	> 0
					)
					{
						$pres->curr_slide_ind_style	 = " width: ".(int)($pres->meta['curr_slide_ind_width']).'px;';
						$pres->curr_slide_ind_style	.= " height: ".(int)($pres->meta['curr_slide_ind_height']).'px;';
						$pres->curr_slide_ind_style	.= " margin: ".(int)$pres->meta['curr_slide_ind_margin_top']."px ".(int)$pres->meta['curr_slide_ind_margin_left']."px; ";
						$pres->curr_slide_ind_style	.= " background-image: url(".$pres->meta['slide_ind_url'].");";
					}
					else
					{
						$pres->curr_slide_ind_style	= "display: none;";
					}
				
				return $pres;
				
			}
			
			
	/*
		==================================================================
			Update Main Presentation 
		==================================================================  */
		public function update_m($gsp_UID = null,$args)
			{
				if (!is_numeric($gsp_UID) ) { return; }
				
				if (!is_array($args)) { return; }
				
				global $wpdb;
				foreach ($args as $meta_key	=> $meta_value)
					{
						if ($meta_key === 'gsp_comment') { continue; }
						$sql = "update ".GS_PRE_PREFIX."presentationmeta set meta_value = %s where meta_key = %s and gsp_UID = %d";
						if (! $wpdb->query($wpdb->prepare($sql,stripcslashes($meta_value),$meta_key,$gsp_UID)) )
						{
							// do an insert.
							$sql = "select count(*) as count from ".GS_PRE_PREFIX."presentationmeta where meta_key = %s and gsp_UID = %d";
							$row	= $wpdb->get_row($wpdb->prepare($sql,$meta_key,$gsp_UID));
							if ($row->count > 0 ) { continue; }
								
							$sql = "insert into ".GS_PRE_PREFIX."presentationmeta (meta_key,meta_value,gsp_UID) values (%s, %s, %s)";
							$wpdb->query($wpdb->prepare($sql,$meta_key,stripcslashes($meta_value),$gsp_UID));
						}
					}
					
				if (isset($args['gsp_comment']))
					{
						$sql = "update ".GS_PRE_PREFIX."presentation set gsp_comment = %s where gsp_UID = %d";
						$wpdb->query($wpdb->prepare($sql,stripcslashes($args['gsp_comment']),$gsp_UID));
					}
			}
			
		/*
			==================================================================
				A function to delete an entire presentation.
			==================================================================  */
		public function drop_pres($gsp_UID = null)
			{
				if (!is_numeric($gsp_UID) ) { return; }
				global $wpdb;
				$sql = "delete from ".GS_PRE_PREFIX."presentation where gsp_UID = %d";
				$wpdb->query($wpdb->prepare($sql,$gsp_UID) );
				
				$sql = "delete from ".GS_PRE_PREFIX."presentationmeta where gsp_UID = %d";
				$wpdb->query($wpdb->prepare($sql,$gsp_UID) );
				
				$all_scenes = $this->getall_secenes($gsp_UID);
				if (!is_array($all_scenes)) { return; }
				
				foreach($all_scenes as $scene)
					{
						$this->drop_scene($scene->scene_UID);
					}
			}
			
	/*
		==================================================================
			Get_a scene by id. 
		==================================================================  */
		public function get_scene($scene_UID = null)
			{
				if (!is_numeric($scene_UID)) { return false; }
				
				global $wpdb;
				
				$sql = "select * from ".GS_PRE_PREFIX."scene where scene_UID = %d";
				$scene = $wpdb->get_row($wpdb->prepare($sql,$scene_UID));
				
				$sql 		=	"select * from ".GS_PRE_PREFIX."scenemeta where scene_UID	= %d";
				$results	=	$wpdb->get_results($wpdb->prepare($sql,$scene_UID));
				
				if (!is_array($results)) { return $scene; }
				
				foreach($results as $result)
					{
						$scene->meta[$result->meta_key]	= $result->meta_value;
					}
					
				if (strlen($scene->meta['s_bgsrc']))
					{
						$img_resource	= gs_basic_get_wp_image($scene->meta['s_bgsrc']);
						$scene->meta['s_bg_url']	= $img_resource->url;
					}
					
				/*
				==================================================================
					Scene Style. 
				==================================================================  */
				$scene->scene_style	.= 	((strlen($scene->meta['s_bgc']) == 6 && $scene->meta['s_bgc_transparent'] != 1)? ' background-color: #'.$scene->meta['s_bgc'].';' : '');
				$scene->scene_style	.= 	(strlen($scene->meta['s_bg_url']) > 0? " background-image: ".'url('.$scene->meta['s_bg_url'].');'." background-repeat: ".$scene->meta['s_bgrepeat'].';' : '');
				$scene->scene_style	.= 	((strlen($scene->meta['s_bgx']) > 0 && strlen($scene->meta['s_bgy']) > 0)? " background-position: ".(is_numeric( $scene->meta['s_bgx'] )? $scene->meta['s_bgx'].'px ' : $scene->meta['s_bgx'].' ').(is_numeric( $scene->meta['s_bgy'] )? $scene->meta['s_bgy'].'px ' : $scene->meta['s_bgy'].' ').';' : '' );

				/*
				==================================================================
					Content style
				==================================================================  */
				if (strlen($scene->meta['content_bgsrc']))
					{
						$img_resource	= gs_basic_get_wp_image($scene->meta['content_bgsrc']);
						$scene->meta['content_bg_url']	= $img_resource->url;
					}
				
				$scene->content_style	 = ((int)($scene->meta['content_width']) > 0? " width: ".(int)($scene->meta['content_width']).'px;' : '');
				$scene->content_style	.= ((int)($scene->meta['content_height']) > 0? " height: ".(int)($scene->meta['content_height']).'px;' : '');
				$scene->content_style	.= ((int)($scene->meta['content_padding']) > 0? " padding: ".(int)($scene->meta['content_padding']).'px;' : '');
				$scene->content_style	.= (is_numeric((int)($scene->meta['content_x']))?" left: ".(int)($scene->meta['content_x']).'px;' : '');
				$scene->content_style	.= (is_numeric((int)($scene->meta['content_y']))?" top: ".(int)($scene->meta['content_y']).'px;' : '');
				$scene->content_style	.= 	(strlen($scene->meta['content_bgc']) == 6 && $scene->meta['content_bgc_transparent'] != 1? " background-color: #".$scene->meta['content_bgc'].';' : '');
				$scene->content_style	.= 	(strlen($scene->meta['content_bg_url']) > 0? ' background-image: url('.$scene->meta['content_bg_url'].');' : '');
				$scene->content_style	.= 	(is_numeric( $scene->meta['content_bgx'] )? " background-position: ".$scene->meta['content_bgx'].'px ' : $scene->meta['content_bgx'].' ').(is_numeric( $scene->meta['content_bgy'] )? $scene->meta['content_bgy'].'px ' : $scene->meta['content_bgy'].' ').';';
				$scene->content_style	.= 	(strlen($scene->meta['content_bgrepeat']) >0? " background-repeat: ".$scene->meta['content_bgrepeat'].';' : '');
				$scene->content_style	.= 	((int)$scene->meta['content_borderw'] > 0? " border: ".(int)$scene->meta['content_borderw'].'px '.(strlen($scene->meta['content_bordert']) > 1? $scene->meta['content_bordert'] : 'solid').' #'.(strlen($scene->meta['content_borderc']) == 6? $scene->meta['content_borderc'] : 'CCCCCC').';' : '');
				$scene->content_style	.= 	((int)$scene->meta['content_borderr'] > 0? ' -moz-border-radius: '.(int)$scene->meta['content_borderr'].'px; -webkit-border-radius: '.(int)$scene->meta['content_borderr'].'px;' : '');
				if ($scene->meta['content_display'] == 0) {$scene->content_style = "display: none;";}
				
				/*
				==================================================================
						caption style caption_1_display
				==================================================================  */
				$scene->caption_1_style	 = ((int)($scene->meta['caption_1_width']) > 0? " width: ".(int)($scene->meta['caption_1_width']).'px;' : '');
				$scene->caption_1_style	.= ((int)($scene->meta['caption_1_height']) > 0? " height: ".(int)($scene->meta['caption_1_height']).'px;' : '');
				$scene->caption_1_style	.= (strlen($scene->meta['caption_1_padding']) > 0? " padding: ".(int)($scene->meta['caption_1_padding']).'px;' : '');
				$scene->caption_1_style	.= (strlen($scene->meta['caption_1_margin']) > 0? " margin: ".(int)($scene->meta['caption_1_margin']).'px;' : '');
				$scene->caption_1_style .= ' '.$scene->meta['caption_1_custom_style'];
				if ($scene->meta['caption_1_display'] == 0) {$scene->caption_1_style = "display: none;";}
				
				/*
				==================================================================
						Scene Link
				==================================================================  */
				if (strlen($scene->meta['s_linkimg_src']))
					{
						$img_resource	= gs_basic_get_wp_image($scene->meta['s_linkimg_src']);
						$scene->meta['s_linkimg_url']	= $img_resource->url;
					}
				$scene->link_style	 = (is_numeric((int)($scene->meta['s_linkimgx']))?" left: ".(int)($scene->meta['s_linkimgx']).'px;' : '');
				$scene->link_style	.= (is_numeric((int)($scene->meta['s_linkimgy']))?" top: ".(int)($scene->meta['s_linkimgy']).'px;' : '');
				$scene->link_style	.= (is_numeric((int)($scene->meta['s_link_width']))?" width: ".(int)($scene->meta['s_link_width']).'px;' : '');
				$scene->link_style	.= (is_numeric((int)($scene->meta['s_link_height']))?" height: ".(int)($scene->meta['s_link_height']).'px;' : '');
				$scene->link_style	.= (strlen($scene->meta['s_linkimg_url'])? ' background: transparent url('.$scene->meta['s_linkimg_url'].') top left no-repeat;' : '');
				
				switch($scene->meta['s_link_type'])
					{
						case 'page': {$scene->scene_link = get_permalink($scene->meta['s_linkpage']);}break;
						case 'post': {$scene->scene_link = get_permalink($scene->meta['s_linkpost']);}break;
						case 'cat':  {$scene->scene_link = get_category_link($scene->meta['s_linkcat']);}break;
						case 'url':  {$scene->scene_link = $scene->meta['s_linkurl'];}break;
					}
				
            //[s_linkurl] => 1
            //[s_linkpage] => 66
            //[s_linkpost] => 27
            //[s_linkcat] => 3


	
				
				return $scene;
			}
			
		/*
		==================================================================
			Update Scene
		==================================================================  */
		public function update_s($scene_UID = null,$args)
			{
				if (!is_numeric($scene_UID) ) { return false; }
				
				if (!is_array($args)) { return false; }
				
				$non_meta_variables = array ('scene_order', 'scene_comment', 'is_active');
				
				global $wpdb;
				foreach ($args as $meta_key	=> $meta_value)
					{
						if (in_array($meta_key,$non_meta_variables))
							{
								switch ($meta_key)
									{
										case 'scene_order':
										case 'is_active':
											{
												$sql = "update ".GS_PRE_PREFIX."scene set ".$meta_key." = %d where scene_UID = %d";
											}break;
										case 'scene_comment':
											{
												$sql = "update ".GS_PRE_PREFIX."scene set ".$meta_key." = %s where scene_UID = %d";
											}break;
									}
									$wpdb->query($wpdb->prepare($sql,$meta_value,$scene_UID));
								continue;
							}
						$sql = "update ".GS_PRE_PREFIX."scenemeta set meta_value = %s where meta_key = %s and scene_UID = %d";
						if (!$wpdb->query($wpdb->prepare($sql,stripcslashes($meta_value),$meta_key,$scene_UID)) )
							{
								// do an insert.
								$sql = "select count(*) as count from ".GS_PRE_PREFIX."scenemeta where meta_key = %s and scene_UID = %d";
								$row	= $wpdb->get_row($wpdb->prepare($sql,$meta_key,$scene_UID));
								if ($row->count > 0 ) { continue; }
								
								$sql = "insert into ".GS_PRE_PREFIX."scenemeta (meta_key,meta_value,scene_UID) values (%s, %s, %s)";
								$wpdb->query($wpdb->prepare($sql,$meta_key,stripcslashes($meta_value),$scene_UID));
							}
					}
				return true;
					
			}
			
		/*
		==================================================================
				delete a scene
		==================================================================  */
		public function drop_scene($scene_UID = null)
			{
				if (!is_numeric($scene_UID) ) { return; }
				
				global $wpdb;
				$sql = "delete from ".GS_PRE_PREFIX."scene where scene_UID = %d";
				$wpdb->query($wpdb->prepare($sql,$scene_UID));
				
				$sql = "delete from ".GS_PRE_PREFIX."scenemeta where scene_UID = %d";
				$wpdb->query($wpdb->prepare($sql,$scene_UID));
				
			}
			
		/*
			==================================================================
				Re-order scenes
			==================================================================  */
		public function scene_order($dir,$scene_UID)
			{
				if (!is_numeric($scene_UID) ) { return; }
				if (!is_numeric($dir) ) { return; }
				
				global $wpdb;
				$sql = "select gsp_UID from ".GS_PRE_PREFIX."scene where scene_UID = %d";
				$row = $wpdb->get_row($wpdb->prepare($sql,$scene_UID));
				$gsp_UID	= $row->gsp_UID;
				if (!is_numeric($gsp_UID) ) { return; }
				
				$sql = "select scene_UID, scene_order from ".GS_PRE_PREFIX."scene where gsp_UID = %d order by scene_order";
				$all_scenes = $wpdb->get_results($wpdb->prepare($sql,$gsp_UID));
				
				if (!is_array($all_scenes)) { return; }
				
				foreach ($all_scenes as $scene)
					{
						$order	= ($scene_UID == $scene->scene_UID? $scene->scene_order + $dir : $scene->scene_order);
						
						$order_ary[$scene->scene_UID]['order']	= $order;
						$order_ary[$scene->scene_UID]['dir']	= ($scene_UID == $scene->scene_UID?  $dir : 0);
					}
					
				$scene_sort = create_function('$a,$b','return ($a["order"] - $b["order"]) + $a["dir"];');
				uasort($order_ary,$scene_sort);
				
				$order	= 1;
				foreach($order_ary as $scene_UID => $scene_ary)
					{
						$this->update_s($scene_UID,array('scene_order' => $order++ ));
					}
			}

			
			
/*
	==================================================================
		Main and Content Templates. 
	==================================================================  */
	
	private function _main_template($gs_main,$scenes)
		{?>
			<style>
				#gsp_presentation_main_wrap .gsp_scene {
					width:				<?php echo $gs_main->meta['m_width'].'px;';?>
					height:				<?php echo $gs_main->meta['m_height'].'px;';?>
					
				}
			</style>

			<div id = "gsp_presentation_main_wrap" class = "gs_presentation_UID-<?php echo $gs_main->gsp_UID; ?>">
				<div id = "gsp_main" style = "<?php echo $gs_main->m_style; ?>">
		
				<!-- scenes --> 
				<?php if (count($scenes) > 0) { print "\n".join("\n",array_values($scenes) ); } ?>
				
				<!-- end scenes -->
		
				</div><!-- end of gsp_main_demo -->
				
				<!-- prev next buttons -->
				<div class = "gsp_prev_next_btn L" style = "<?php echo $gs_main->pre_next_left_style;?>" role = "L"></div>
				<div class = "gsp_prev_next_btn R" style = "<?php echo $gs_main->pre_next_right_style;?>" role = "R"></div>
				<!-- end prev next buttons -->
				
				<!-- slide tray wrapper -->
				<div class = "slide_tray_wrap" style = "<?php echo $gs_main->slide_tray_style; ?>">
				
				<table class = "slide_tray"><tr>
							<td class = "L" style = "<?php echo $gs_main->slide_tray_L_style;?>">&nbsp;</td>
							
							<?php
								if (count($scenes) > 0)
								{
									foreach ($scenes as $scene_UID => $scene)
								{	?>
									<td class = "M"  style = "<?php echo $gs_main->slide_tray_M_style;?>">
										<div class = "indicator scene_UID-<?php echo $scene_UID; ?>" style ="<?php echo $gs_main->curr_slide_ind_style; ?>" role = "<?php echo $scene_UID; ?>"></div>
									</td>
						<?php	}
								}
									?>
							
							<td class = "R"  style = "<?php echo $gs_main->slide_tray_R_style;?>">&nbsp;</td></tr>
				</table>
				</div><!-- curr_slide_tray_wrap -->
				<!-- end of slide tray wrapper -->
				
			</div><!-- end of gsp_presentation_main_wrap -->
		<!--	<script>jQuery(document).ready( function () { var gs_presentation = new GsPresentation('gsp_presentation_main_wrap'); gs_presentation.start(); });</script>  -->

<?php	}



/*
	==================================================================
		Scene Templates. 
	==================================================================  */
	private function _scene_template($gs_scene)
		{ 
//print_r($gs_scene);
    ?>
		<img src="<?echo $gs_scene->meta['content_bg_url']; ?>" title="<? echo $gs_scene->meta['caption_1']; ?>"/>
		<!---	<div class = "gsp_scene scene_UID-<?php /*echo $gs_scene->scene_UID.' '.($gs_scene->is_init == 1? 'is_init' : ''); ?>" style = "<?php echo $gs_scene->scene_style; ?>" role = '{"s_effect_in" : "<?php echo $gs_scene->meta['s_effect_in'];?>", "s_effect_in_time" : "<?php echo $gs_scene->meta['s_effect_in_time'];?>", "s_display_time" : "<?php echo $gs_scene->meta['s_display_time'];?>", "s_effect_out" : "<?php echo $gs_scene->meta['s_effect_out'];?>", "s_effect_out_time" : "<?php echo $gs_scene->meta['s_effect_out_time'];?>", "scene_UID" : "<?php echo $gs_scene->scene_UID;?>", "scene_link" : "<?php echo $gs_scene->scene_link;?>","scene_callback" : "<?php echo rawurlencode($gs_scene->meta['s_linkcallback']);?>"}'>
				<div class = "gsp_scene_content_wrap <?php echo $gs_scene->meta['content_class_names']; ?>" style = "<?php echo $gs_scene->content_style.' '.$gs_scene->meta['content_custom_style']; ?>">
					<div class = "gsp_scene_caption <?php echo $gs_scene->meta['content_1_class_names']; ?>" style = "<?php echo $gs_scene->caption_1_style; ?>"><?php echo $gs_scene->meta['caption_1']; ?></div>
				</div><!-- end of gsp_scene_content_wrap -->
		<!--		<div class = "gsp_scene_link" style = "<?php echo $gs_scene->link_style; */?>"></div>
			</div><!-- end of gsp_scene -->  
<?php	}
			

}