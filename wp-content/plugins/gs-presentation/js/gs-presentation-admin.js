/*
	==================================================================
		javascript to control the building of
		presentaions and scenes.
		
	==================================================================  */

	/*
		==================================================================
			delete presentation. 
		==================================================================  */
		function gs_pres_delete(gsp_UID)
			{
				if (!confirm("Are you sure you want to delete this entire presentaion. This presentations scenes will also be deleted. This can not be undone.")) { return; }
				
				var post = {
					gsp_UID: gsp_UID,
					a: 'do_drop_pres'
				}
				GsBasicPost(post);
			}
	
	/*
		==================================================================
			delete scene. 
		==================================================================  */
		function gs_pres_delete_scene(scene_UID)
			{
				if (!confirm("Are you sure you want to delete this scene. This can not be undone.")) { return; }
				
				var post = {
					scene_UID: scene_UID,
					a: 'do_drop_scene'
				}
				GsBasicPost(post);
			}
			
	/*
		==================================================================
			Scene order
		==================================================================  */
		function gs_scene_ord(dir,scene_UID,post_once)
			{
				var post = {
					scene_UID: scene_UID,
					a: 'gs_scene_ord',
					dir: dir,
					post_once: post_once
				}
				GsBasicPost(post);
			}
			
	/*
		==================================================================
			Section Name
		==================================================================  */
		function gs_presentation_show_link_types(o,link_class)
			{
				var link_src = jQuery(o).val();
				jQuery('.gs_link_src_selector' + '.'+link_class ).css({'display' : 'none'});
				jQuery('.'+link_class + '.is_'+link_src).css({'display' : 'table-row' } );
			}