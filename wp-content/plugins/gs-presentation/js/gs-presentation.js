	/*
	==================================================================
		Javascript to control the gs presentation. 
	==================================================================  */
	
	function GsPresentation(presentation_id)
	{
		
	var gsp_pres	= jQuery('#'+presentation_id).get(0);
	var gsp_main	= jQuery('#'+presentation_id+' #gsp_main').get(0);
	
	var active_o	= ''; //holds the currently displayed objed.
	
	var t			= ''; // holds the current timer.
	
	// listen for click of prev next buttons.
	jQuery(gsp_pres).find('.gsp_prev_next_btn').bind('click', function () { handle_prev_next_click(this); } );
	
	//listen for click on page indicator.
	jQuery(gsp_pres).find('.indicator').bind('click', function () { handle_indicator_click(this); } );
	
	//listen for click on a scene
	jQuery(gsp_main).bind('click', function () { handle_scene_click(this); } );
		
	this.start = function () { 
//start_animation();
$('#gsp_main').nivoSlider();

 }
$(window).load(function() {
	$('#gsp_main').nivoSlider();
});
	
		function start_animation(current_o,direction)
			{
				if ( (typeof(current_o) == 'undefined' || jQuery(current_o).next('.gsp_scene').length == 0) && typeof (direction) == 'undefined' )
					{
						var next_o	=	jQuery(gsp_main).find('.gsp_scene.is_init').get(0);
					}
				else if (direction == -1 && jQuery(current_o).prev('.gsp_scene').length == 0)
					{
						var next_o	=	jQuery(gsp_main).find('.gsp_scene').last();
						jQuery(next_o).stop();
					}
				else if (direction == -1)
					{
						var next_o	=	jQuery(current_o).prev('.gsp_scene');
						jQuery(next_o).stop();	
					}
				else if (direction == 0)
					{
						var next_o = current_o;
					}
				else
					{
						var next_o	=	jQuery(current_o).next('.gsp_scene');
					}
					
				// set the active object before it starts moving.
				active_o	= next_o;
					
				// pre set the next slide
				pre_effect(next_o);
				
				// do the  animation.
				if (typeof(current_o) != 'undefined' && direction != 0) { effect_out(current_o); }
				effect_in(next_o);
				
				//var delay	= _effect.pause_time * 1000;
				//setInterval(function() { alert(main_title + ' timeout - ' + delay);},delay);
			};
			
		/*
		==================================================================
				Preload effects and times into the slide object. 
		==================================================================  */
		function pre_effect(o)
			{
				// parse the effect args into the jQuery data struct for this object. 
				jQuery.data(o, jQuery.parseJSON( jQuery(o).attr("role") ) );
				
				switch(jQuery.data(o).s_effect_in)
					{
						case 'slide_BL':
							{
								jQuery(o).css({'left' : -1 * (jQuery(gsp_main).outerWidth() + 2), 'top' : (jQuery(gsp_main).outerHeight() + 2)  });
							}break;
							
						case 'slide_L':
							{
								jQuery(o).css({'left' : -1 * (jQuery(gsp_main).outerWidth() + 2), 'top': 0 });
							}break;
							
						case 'slide_TL':
							{
								jQuery(o).css({'left' : -1 * (jQuery(gsp_main).outerWidth() + 2), 'top' : -1 * (jQuery(o).outerHeight() + 2) });
							}break;
							
						case 'slide_T':
							{
								jQuery(o).css({'left' : 0, 'top' : -1 * (jQuery(o).outerHeight() + 2) });
							}break;
							
						case 'slide_TR':
							{
								jQuery(o).css({'left' : (jQuery(gsp_main).outerWidth() + 2),'top' : -1 * (jQuery(o).outerHeight() + 2) });
							}break;
						
						case 'slide_R':
							{
								jQuery(o).css({'left' : (jQuery(gsp_main).outerWidth() + 2), 'top' : 0 });
							}break;
							
						case 'slide_BR':
							{
								jQuery(o).css({'left' : (jQuery(gsp_main).outerWidth() + 2), 'top' : (jQuery(gsp_main).outerHeight() + 2) } );
							}break;
							
						case 'slide_B':
							{
								jQuery(o).css({'left' : 0,'top' : (jQuery(gsp_main).outerHeight() + 2) });
							}break;
							
						case 'fade':
							{
								
								jQuery(o).css({'top' : -9999,'visibility' : 'visible'});
								jQuery(o).css({'opacity' : 0,'left': 0,'top' : 0});
							}break;
							
						case 'blindDown':
						case 'blindUp':
							{
								jQuery(o).css({'opacity' : 1,'left': 0,'top' : 0, 'display' : 'none', 'visibility': 'visible'});
							}
							
					}

			}
					
		/*
		==================================================================
				Do the in effect.
		==================================================================  */
		function effect_in(o)
			{
				// change the indicator.
				jQuery(gsp_pres).find('.indicator').removeClass('is_active');
				
				
				// do the animation.
				switch(jQuery.data(o).s_effect_in)
				{
					case 'fade':
						{
							jQuery(o).animate({'opacity': 1},(jQuery.data(o).s_effect_in_time * 1000), function() { post_effect(o); });
						}break
					case'blindDown':
						{
							jQuery(o).slideDown( jQuery.data(o).s_effect_in_time * 1000, function() { post_effect(o); });
						}break;
					case'blindUp':
						{
							jQuery(o).slideUp( jQuery.data(o).s_effect_in_time * 1000, function() { post_effect(o); });
						}break;
					default:
						{
							jQuery(o).css({'visibility' : 'visible','opacity' : 1});
							jQuery(o).animate({'left': 0, 'top': 0},(jQuery.data(o).s_effect_in_time * 1000), function() { post_effect(o); });
						}break;
				}
				
			}
			
		/*
		==================================================================
				post effect, set the indicator, and start the
				display delay
		==================================================================  */
		function post_effect(o)
			{
				// show the scene link image.
				show_scene_link(o);
				
				// change the indicator.
				jQuery(jQuery(gsp_pres).find('.indicator.scene_UID-'+jQuery.data(o).scene_UID).get(0)).addClass('is_active');
				t = setTimeout(function() { start_animation(o); },(jQuery.data(o).s_display_time * 1000));
				
			}
			
		/*
		==================================================================
				Do the out effect.
		==================================================================  */
		function effect_out(o)
			{
				// hide the scene link
				hide_scene_link(o);
				var left = 0;
				var top = 0;
				switch (jQuery.data(o).s_effect_out)
					{
						case 'slide_BL':
							{
								left 	= 	(-1 * (jQuery(gsp_main).outerWidth() + 2));
								top 	=	(jQuery(gsp_main).outerHeight() + 2);
							}break;
							
						case 'slide_L':
							{
								left	=	(-1 * (jQuery(gsp_main).outerWidth() + 2));
								top		=	0;
							}break;
							
						case 'slide_TL':
							{
								left	=	(-1 * (jQuery(gsp_main).outerWidth() + 2));
								top 	=	(-1 * (jQuery(o).outerHeight() + 2));
							}break;
							
						case 'slide_T':
							{
								left	=	0
								top 	= 	(-1 * (jQuery(o).outerHeight() + 2));
							}break;

						case 'slide_TR':
							{
								left 	=	(jQuery(gsp_main).outerWidth() + 2);
								top		=	(-1 * (jQuery(o).outerHeight() + 2));
							}break;
						
						case 'slide_R':
							{
								left	=	(jQuery(gsp_main).outerWidth() + 2);
								top		=	0;
							}break;
							
						case 'slide_BR':
							{
								left 	=	(jQuery(gsp_main).outerWidth() + 2);
								top		=	(jQuery(gsp_main).outerHeight() + 2);
							}break;
							
						case 'slide_B':
							{
								left 	=	0;
								top		=	(jQuery(gsp_main).outerHeight() + 2);
							}break;
							
						case'blindUp':
							{
								jQuery(o).slideUp( jQuery.data(o).s_effect_in_time * 1000, function() { jQuery(o).css({'visibility' : 'hidden'}); });
								return;
							}break;
						default:
							{
								// nothing, just do top, left opacity as configured.
							}break;
					}
					
					jQuery(o).animate({'left' : left,'top' : top, 'opacity' : 0 } ,jQuery.data(o).s_effect_in_time * 1000,function() { jQuery(o).css({'visibility' : 'hidden'}); });
				
			}
			
		/*
		==================================================================
			Show scene link
		==================================================================  */
		function show_scene_link(o)
			{
				jQuery(o).find('.gsp_scene_link').show('fast');
			}
			
		/*
		==================================================================
			Hide scene link
		==================================================================  */
		function hide_scene_link(o)
			{
				jQuery(o).find('.gsp_scene_link').hide(0);
			}
			
			
		/*
		==================================================================
				Hand a prev/next click .
		==================================================================  */
		function handle_prev_next_click(o)
			{
				var direction = jQuery(o).attr("role");
				var curr_o_UID = jQuery.data(active_o).scene_UID;
				
				// stop the current animation.
				 jQuery(active_o).stop();
				
				// cancle the timer.
				clearTimeout( t );
				
				// branch for prev or next. 
				if ( direction == 'L')
				{
					// move in the next slide
					start_animation(active_o);
				}
				else
				{
					// go to the prev slide.
					start_animation( active_o, -1 );
				}
				
			}
			
		/*
		==================================================================
				handle an indicator click, jump to page. 
		==================================================================  */
		function handle_indicator_click(o)
			{
				var scene_UID = jQuery(o).attr("role");
				
				if (scene_UID == jQuery.data(active_o).scene_UID ) { return; } // clicked active scene.
				
				// stop the current animation.
				jQuery(active_o).stop();
				
				// cancle the timer.
				clearTimeout( t );
				
				//move out the current slide.
				effect_out(active_o);
				
				// find the jquery object of the clicked icon.
				var clicked_o = jQuery(gsp_main).find('.gsp_scene.scene_UID-'+scene_UID).get(0);

				start_animation(clicked_o,0);
			}
			
		/*
			==================================================================
				handle scene click
			==================================================================  */
		function handle_scene_click()
			{
				if (typeof(jQuery.data(active_o).scene_callback) != 'undefined' && jQuery.data(active_o).scene_callback.length > 0 )
					{
						eval(unescape(jQuery.data(active_o).scene_callback));
					}
					
				if (typeof(jQuery.data(active_o).scene_link) != 'undefined' && jQuery.data(active_o).scene_link.length > 0 )
					{
						document.location = jQuery.data(active_o).scene_link;
					}
			}
			
		
	
	}