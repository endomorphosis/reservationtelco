<?php

add_action( 'widgets_init', 'register_ilg_member_acces_login' );

function register_ilg_member_acces_login() {
	register_widget( 'Ilg_Member_Access_Login' );
}

class Ilg_Member_Access_Login extends WP_Widget {
	
	function Ilg_Member_Access_Login() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => '[da_recent_posts]', 'description' => 'A widget to pull in the last N post from a specified category' );

		/* Widget control settings. */
		$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'da_recent_posts-widget' );

		/* Create the widget. */
		$this->WP_Widget( 'da_recent_posts-widget', 'ILG Member Access Login', $widget_ops, $control_ops );
	}
	
	function widget( $args, $instance ) {
		extract( $args );

		/* User-selected settings. */
		$title = apply_filters('widget_title', $instance['title'] );
		$name = $instance['name'];
		$sex = $instance['sex'];
		

		/* Before widget (defined by themes). */
		echo preg_replace("/\[da_recent_posts\]/",'da_recent_posts '.$instance['class'],$before_widget);

		/* Title of widget (before and after defined by themes). */
		if ( $title )
			echo $before_title . $instance['title'] . $after_title;

		
		$recent_posts = get_posts('numberposts=' . $instance['post_count'] .'&category=' . $instance['category']);
		if (is_array($recent_posts) && count($recent_posts) > 0)
			{
				foreach($recent_posts as $post)
					{
						$event_date = '';
						$post_meta = get_post_custom($post->ID);
						if ($instance['date_from'] == 'event_date' || $instance['date_from'] == 'both' && is_array($post_meta['event_date']) )
							{
								$event_date = $post_meta['event_date'][0];
							}
						elseif ($instance['date_from'] == 'post_date' || $instance['date_from'] == 'both')
							{
								$event_date = Date('M d Y',strtotime($post->post_date));
							}
							
						echo 	'<div class = "summary_wrap">
								<p class = "summary_date">'. $event_date .'</p>
								<p class = "summary_content">'.$post->$instance['content_source'].'&nbsp;<a href = "' . get_permalink($post->ID) . '">&raquo;</a></p>
								</div>';
					}
			}
		else
			{
				echo '<p class = "no_posts">No Posts</p>';
			}


		
		echo $after_widget;
	}
	
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Strip tags (if needed) and update the widget settings. */
		$instance['title'] = $new_instance['title'];
		$instance['class'] = strip_tags( $new_instance['class'] );
		$instance['category'] = $new_instance['category'];
		$instance['post_count'] = $new_instance['post_count'];
		$instance['content_source'] = $new_instance['content_source'];
		$instance['date_from'] = $new_instance['date_from'];

		return $instance;
	}
	
	function form( $instance ) {

		/* Set up some default widget settings. */
		$defaults = array( 'title' => 'Recent Posts');
		$instance = wp_parse_args( (array) $instance, $defaults );
		
		$args = array(
							'orderby'                  	=> 	'name',
							'order'                    	=> 	'ASC',
							'hide_empty'               	=> 	false,
							'hierarchical'             	=> 	1,
							'name'						=>	$this->get_field_name( 'category' ),
							'selected'					=>	$instance['category']
							);
		
		$content_sources = array(
							array('value' => 'post_excerpt', 		'display' 	=> 'Post Excerpt'),
							array('value' => 'post_title', 			'display'	=> 'Post Title')
							);
		
		$date_from	 = array(
							array('value' => 'publish_date', 		'display' 	=> 'Post Published Date'),
							array('value' => 'event_date', 			'display'	=> 'Post Meta event_date'),
							array('value' => 'both', 				'display'	=> 'Both')
							);
		
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label><br>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo htmlentities($instance['title']); ?>" style="width:90%;" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'date_from' ); ?>">Date From:</label><br>
			<select id="<?php echo $this->get_field_id( 'date_from' ); ?>" name="<?php echo $this->get_field_name( 'date_from' ); ?>" ><?php echo gs_build_option_selection($instance['date_from'],$date_from); ?></select>
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'class' ); ?>">CSS Class:</label><br>
			<input id="<?php echo $this->get_field_id( 'class' ); ?>" name="<?php echo $this->get_field_name( 'class' ); ?>" value="<?php echo $instance['class']; ?>" style="width:90%;" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'category' ); ?>">Category:</label><br>
			<?php wp_dropdown_categories($args); ?>
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'post_count' ); ?>">Post Count:</label><br>
			<input id="<?php echo $this->get_field_id( 'post_count' ); ?>" name="<?php echo $this->get_field_name( 'post_count' ); ?>" value="<?php echo $instance['post_count']; ?>" style="width:90%;" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'content_source' ); ?>">Content From:</label><br>
			<select id="<?php echo $this->get_field_id( 'content_source' ); ?>" name="<?php echo $this->get_field_name( 'content_source' ); ?>" ><?php echo gs_build_option_selection($instance['content_source'],$content_sources); ?></select>
		</p>
	<?php
	}

}
		