<?php

/*
Plugin Name: GoSeese Basic Functions
Plugin URI: http://www.goseese.com/
Description: This plugin doesn't do anything by it's self. It's a collection of common functions that are used through out my plugins. 
Version: alpha 0.8
Author: Jeff Seese
Author URI: http://www.goseese.com
License: Purchase only
*/
/*
	==================================================================
		Require Current Version of Jquery, this is necessary for
		parseJSON in 1.4.x
	==================================================================  */

/*
 .8 5/31/2010 changed jquery current version check/force to not execute on page/post edit. The tiny MCE does not funciton with jquery 1.4.2
*/


if (!function_exists('current_jquery') )
	{
		function current_jquery($version)
		{
			if ($_GET['action'] === 'edit' && is_admin() ) { return; }
			
				global $wp_scripts;
				if ( ( version_compare($version, $wp_scripts -> registered[jquery] -> ver) == 1 ) ) {
						wp_deregister_script('jquery'); 
		 
						wp_register_script('jquery',
								'http://ajax.googleapis.com/ajax/libs/jquery/'.$version.'/jquery.min.js',
								false, $version);
				}
		}
	}
	
add_action( 'wp_head', current_jquery( '1.4.2' ),0 );

	/*
		==================================================================
			get the parent of the current page, or any page by id. 
		==================================================================  */
	if(!function_exists('gs_get_page_parent'))
	{
	function gs_get_page_parent($ID = null,$param = null)
		{$idx = 0;
			if ($ID == null)
				{
					global $post;
					$ID = $post->ID;
				}
				
			do {
				$page = get_page($ID);
				$ID = $page->ID;
				if ($idx++ > 100) { print "recursion loop ".__file__." : ".__line__; break; }
				$return = (is_null($param)?  $page->post_name : $page->$param); 
				$ID = $page->post_parent;
			} while ($page->post_parent != 0);
			
			return $return;
		}
	}
	
	/*
		==================================================================
			Save Options.
		==================================================================  */
	if (!function_exists('gs_save_options') )
		{
			function gs_save_options($config_group = null,$args)
				{
					if (!is_array($args)) { return; }
					foreach ($args as $option_name => $option_value)
						{
							if (!is_null($config_group)) { $option_name = $config_group.'-'.$option_name; }
							update_option( $option_name, $option_value );
						}
				}
		}
		
	if (!function_exists('gs_get_options') )
		{
			function gs_get_options($config_group = null)
				{
					$options = array();
					if (is_null($config_group) ) { return; }
					
					global $wpdb;
					$sql = "select * from " . $wpdb->prefix."options " . "where option_name like '%s'";
					$results = $wpdb->get_results($wpdb->prepare($sql,'%'.$config_group.'%'));
					
					if (!is_array($results)) { return; }
					foreach($results as $result)
						{
							$options[$result->option_name] = $result;
						}
					return $options;
				}
		}
		
/*
	==================================================================
		build selection box, from gs_base.
	==================================================================  */
	
if (!function_exists ('gs_build_option_selection'))
	{
		function gs_build_option_selection($curr_value = null,$options)
			{
				// build and <option value = "1">test</option>
				// from an array.
				// [0] => array( [value] = 1, [display] => 'test')
				$option_html = '';
				if (!is_array($options)) { return false; }
				foreach ($options as $idx	=> $option)
					{
						
						if (is_array($option) ) { extract($option); }
						else
							{
								$value		= 	$idx;
								$display	=	$option;
							}
							
						$option_html .= '<option value = "' . htmlentities($value) . '" ' . ($value == $curr_value? 'selected' : '') . '>' . htmlentities($display) . '</option>'."\n";
					}
				return $option_html;
			}
	}
	
	/*
		==================================================================
			prevent reposts
		==================================================================  */
		
	if (!function_exists('gs_prevent_repost') )
		{
			function gs_prevent_repost($post_once)
				{
					
					if (strlen($post_once) < 1 ) return false; // no post once, 
					if ($_SESSION['post_once'] == $post_once) { return true; } // this is a reload_so ignore it. 
					
					$_SESSION['post_once'] = $post_once;
					return false; // not a reload.
				}
		}
		
/*
	==================================================================
		media selection dropdown.
	==================================================================  */
	if(!function_exists('gs_media_selector'))
		{
			function gs_wp_media_selector($curr_value = null,$field_name = null,$class_name = null)
				{
				$args = array(
										'post_type' => 'attachment',
										'numberposts' => -1,
										'post_status' => null,
										'post_parent' => null
										);
					$attachments = get_posts($args);
					$media_array = array();
					$media_array[] = array('value' => '', 'display' => 'Select an image from the WP Media Library');
					if ($attachments)
						{
							foreach($attachments as $attachment)
								{
									if (!preg_match("/image/",$attachment->post_mime_type) ) { continue; }
									$media_array[] = array('value' => $attachment->ID, 'display' => $attachment->post_title.'  ');
								}
						}
				return '<select name = "' . $field_name . '" class = "' . $class_name . '">'.gs_build_option_selection( $curr_value,$media_array).'</select>';
				}
		}
		
/*
	==================================================================
		post selector
	==================================================================  */
	if(!function_exists('gs_wp_post_selector'))
		{
			function gs_wp_post_selector($curr_value = null,$field_name = null,$class_name = null)
				{
						$args = array(
										'post_type' => 'post',
										'orderby' => 'title',
										);
					$all_posts = get_posts($args);
					$post_array = array();
					$post_array[] = array('value' => '', 'display' => 'Select a WP post title');
					if ($all_posts)
						{
							foreach($all_posts as $post)
								{
									$post_array[] = array('value' => $post->ID, 'display' => $post->post_title.'  ');
								}
						}
				return '<select name = "' . $field_name . '" class = "' . $class_name . '">'.gs_build_option_selection( $curr_value,$post_array).'</select>';
				}
		}
/*
	==================================================================
		page selector
	==================================================================  */
	if(!function_exists('gs_wp_page_selector'))
		{
			function gs_wp_page_selector($curr_value = null,$field_name = null,$class_name = null)
				{
					$post_array = array();
					$post_array[] = array('value' => '', 'display' => 'Select a WP Page by title');
					$all_posts	= gs_wp_page_heirarchy(0);
					if ($all_posts)
						{
							foreach($all_posts as $post)
								{
									$post_array[] = array('value' => $post['ID'], 'display' => $post['title'].'  ');
								}
						}
				return '<select name = "' . $field_name . '" class = "' . $class_name . '">'.gs_build_option_selection( $curr_value,$post_array).'</select>';
				}
		}
		
/*
	==================================================================
		Flatened page heirarchy
	==================================================================  */
	if(!function_exists('gs_wp_page_heirarchy'))
		{
			function gs_wp_page_heirarchy($ID = 0,$child_indicator = '-',$curr_indicator = '')
				{
					if (!is_numeric($ID)) { return; }
					$all_pages	= array();
					$args = array(
										'post_type' 	=> 'page',
										'orderby' 		=> 'title',
										'post_parent'	=>	$ID
										);
					$all_posts = get_posts($args);
					if (!is_array($all_posts) || count($all_posts) < 1) { return; }
					foreach ($all_posts as $page_post)
						{
							
							$page_post->post_title	=	$curr_indicator.$page_post->post_title;
							$all_pages[]	= array ('ID'	=> $page_post->ID, 'title'	=> $page_post->post_title);
							
							// check for children.
							$all_children = gs_wp_page_heirarchy($page_post->ID,$child_indicator,$child_indicator.$curr_indicator);
							if (!is_array($all_children) || count($all_children) < 1) { continue; }
							foreach($all_children as $child)
								{
									$all_pages[]	= $child;
								}
							
						}
						
					return $all_pages;
				}
		}
		
/*
	==================================================================
		cat selector
	==================================================================  */
	if(!function_exists('gs_wp_category_selector'))
		{
			function gs_wp_category_selector($curr_value = null,$field_name = null,$class_name = null)
				{
					$post_array = array();
					$post_array[] = array('value' => '', 'display' => 'Select a Category');
					$all_posts	= gs_wp_category_heirarchy(0);
					if ($all_posts)
						{
							foreach($all_posts as $post)
								{
									$post_array[] = array('value' => $post['ID'], 'display' => $post['cat_name'].'  ');
								}
						}
				return '<select name = "' . $field_name . '" class = "' . $class_name . '">'.gs_build_option_selection( $curr_value,$post_array).'</select>';
				}
		}
		
/*
	==================================================================
		Flatened cat heirarchy
	==================================================================  */
	if(!function_exists('gs_wp_category_heirarchy'))
		{
			function gs_wp_category_heirarchy($ID = 0,$child_indicator = '-',$curr_indicator = '')
				{
					if (!is_numeric($ID)) { return; }
					$all_categories	= array();
					$args = array(
										'post_type' 	=> 'post',
										'orderby' 		=> 'name',
										'parent'	=>	$ID,
										'hide_empty'	=>	false,
										'hierarchical'	=> true
										
										);
					$all_cats = get_categories($args);
					if (!is_array($all_cats) || count($all_cats) < 1) { return; }
					
					foreach ($all_cats as $cat)
						{
							
							$cat->cat_name	=	$curr_indicator.$cat->cat_name;
							$all_categories[]	= array ('ID'	=> $cat->cat_ID, 'cat_name'	=> $cat->cat_name);
							
							// check for children.
							$all_children = gs_wp_category_heirarchy($cat->cat_ID,$child_indicator,$child_indicator.$curr_indicator);
							if (!is_array($all_children) || count($all_children) < 1) { continue; }
							foreach($all_children as $child)
								{
									$all_categories[]	= $child;
								}
							
						}
						
					return $all_categories;
				}
		}
		
	/*
		==================================================================
			Build image, takes an argument, and determines if it
			is either a attatchment ID, or a URL.
			
			Formats the image to the correct size, returns the
			url and full html.
			
			size = From wp_get_attachment_image_src()
			(string|array) Size of the image shown for an image attachment:
			either a string keyword (thumbnail, medium, large or full)
			or a 2-item array representing width and height in pixels, e.g. array(32,32)
			
			as a bacground image.
			<div id="header" role="banner" style = "margin: 0; padding: 0; background-image: url(<?php gs_page_banner(null,'url') ?>);" >
		==================================================================  */
if(!function_exists('gs_basic_get_wp_image'))
		{
			function gs_basic_get_wp_image($img_resource, $size = 'full', $class = '')
				{
					if (preg_match("/^\d+$/",$img_resource))
						{
							list($img_url, $w, $h) = wp_get_attachment_image_src( $img_resource,$size,true);
						}
					else
						{
							$img_url	= $img_resource;
						}
					
					$img_html	= '<img src = "'.$img_url.'" class = "'.$class.'" '.(is_numeric($w)? "width='$w'" : '').(is_numeric($h)? "height='$h'" : '').' />';
					
					$image->url				= $img_url;
					$image->html			= $img_html;
					$image->w				= $w;
					$image->h				= $h;
					return $image;
				}
		}
		
		
/*
	==================================================================
		function to print pre an array or object
	==================================================================  */
if(!function_exists('_pre'))
		{
		function _pre($arg,$return = 0)
			{
				$rtn =  "<pre>".print_r($arg,1)."</pre>";
				if ($return == 0) { print $rtn; return;}
				return $rtn;
			}
		}
		
/*
	==================================================================
		a function to validate a email in the correct format. 
	==================================================================  */
	if (!function_exists('is_valid_email'))
		{
			function is_valid_email($email)
				{
					  if( !preg_match( "/^[\w|\.|-]+@[\w|\.|-]+$/", $email)) {return false;}
					  return true;
				}
		}
		
/*
	==================================================================
		start sessions.
	==================================================================  */
if (!function_exists('gs_start_session') )
	{
		function gs_start_session()
			{
				if (strlen($_SERVER['HTTP_HOST']) < 1) { return;} // this is not a web reqeust, probably a cron request.
				if (!session_id()) { session_start();}
			}
	}
	
if (!function_exists('gs_basic_flash_error'))
	{
		function gs_basic_flash_error($msg, $additional_class = '')
			{
				return '<div class = "error gs_basic_flash_message '. $additional_class .'" ><h3>Error:</h3><p>' . $msg . '</p></div>';
			}
		
		function gs_basic_flash_message($msg, $additional_class = '')
			{
				return '<div class = "message gs_basic_flash_message '. $additional_class .'" ><h3>Message:</h3><p>' . $msg . '</p></div>';
			}
			
		function gs_basic_flash_updated($msg, $additional_class = '')
			{
				return '<div class = "updated gs_basic_flash_message '. $additional_class .'" ><p>' . $msg . '</p></div>';
			}
	}
	
/*
	==================================================================
		Tiny MCE initer
		Usage.
		
		this must be some where in the head of the php.
		print gs_basic_que_tinymce();
		
		then call this
		gs_basic_tinymce('some_id'); after each text area.
		
		be sure to do this before you save the contents.
		tinyMCE.triggerSave();
		
	==================================================================  */
	
if (!function_exists('gs_basic_tinymce'))
	{
		
		function gs_basic_que_tinymce()
			{
				return '<script type="text/javascript" src="'. WP_PLUGIN_URL .'/gs-basic-functions/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>';
			}
			
		function gs_basic_tinymce($element_id,$button_group = null)
			{
				$return = '
				<!-- TinyMCE -->
				<!-- // this is moved into a seperate function <script type="text/javascript" src="'. WP_PLUGIN_URL .'/gs-basic-functions/tinymce/jscripts/tiny_mce/tiny_mce.js"></script> -->
				<script type="text/javascript">
					tinyMCE.init({
						// General options
						mode : "exact",
						elements : "'.$element_id.'",
						theme : "advanced",
						plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave",
				
						// Theme options
						';
						switch ($button_group)
						{
							case 'advanced':
								{
									$return .= '
						theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
						theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
						theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,ltr,rtl,|,fullscreen",
						theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking",
						theme_advanced_toolbar_location : "top",
						theme_advanced_toolbar_align : "left",
						theme_advanced_statusbar_location : "bottom",
						theme_advanced_resizing : true,
									';
								}break;
							default:
								{
									$return .= '
						theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
						theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
						theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
						theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
						theme_advanced_toolbar_location : "top",
						theme_advanced_toolbar_align : "left",
						theme_advanced_statusbar_location : "bottom",
						theme_advanced_resizing : true,
									';
								}break;
						}
						// Example content CSS (should be your site CSS)
						//content_css : "css/content.css",
				
						// Drop lists for link/image/media/template dialogs
						//template_external_list_url : "lists/template_list.js",
						//external_link_list_url : "lists/link_list.js",
						//external_image_list_url : "lists/image_list.js",
						//media_external_list_url : "lists/media_list.js",
				
						// Style formats
						$return .= '
						style_formats : [
							{title : "Bold text", inline : "b"},
							{title : "Red text", inline : "span", styles : {color : "#ff0000"}},
							{title : "Red header", block : "h1", styles : {color : "#ff0000"}},
							{title : "Example 1", inline : "span", classes : "example1"},
							{title : "Example 2", inline : "span", classes : "example2"},
							{title : "Table styles"},
							{title : "Table row 1", selector : "tr", classes : "tablerow1"}
						],
				
						// Replace values for the template plugin
						template_replace_values : {
							username : "Some User",
							staffid : "991234"
						}
					});
				</script>
				<!-- /TinyMCE -->';
				
			return $return;
			}
	}
	
/*
	==================================================================
		basic js and css
	==================================================================  */
	wp_enqueue_style( 'gs-basic-functions', WP_PLUGIN_URL.'/gs-basic-functions/gs-basic-functions.css');
	wp_enqueue_script('gs-basic-functions',WP_PLUGIN_URL.'/gs-basic-functions/gs-basic-functions.js', array('jquery'));
	wp_enqueue_script('jscolor',WP_PLUGIN_URL.'/gs-basic-functions/jscolor/jscolor.js');