	/*
		==================================================================
			gs-basic-functions.js
			- Basic functions that are re-used throughout the gs-plugins
			
		==================================================================  */
		
	
	/*
		==================================================================
			Ajax nav listners.
			- pass this a class name. the nav, result target adn flash
			target will be determined from that class name. 
			
		==================================================================  */
	function GsBasciSimpleAjaxNav(class_name)
	{
		var result_target 	= jQuery('.'+class_name+'.result_target').get(0);
		var flash_target	= jQuery('.'+class_name+'.flash_target').get(0);
		var params			= jQuery.parseJSON( jQuery(result_target).attr("role") ); //role = '{"effect_time":"10","uid":"1000"}'
		
		this.listen	= function() { init_listeners(); }
		
		function init_listeners()
			{
				jQuery('.gs_basic_nav.'+class_name+' .nav-tab').bind('click', function () { handle_click(this); });
				jQuery('.gs_basic_nav.'+class_name+' .nav-tab.is_init').trigger('click');
				jQuery(result_target).bind('do_update', function () { 	handle_click(jQuery('.gs_basic_nav.'+class_name+' .nav-tab.is_active').get(0),'do_update'); });
			}
			
		function handle_click(o,t)
			{
			// remove is_active from all tabs.
			jQuery('.gs_basic_nav.'+class_name+' .nav-tab').removeClass('is_active');
			
			// animate/hide the flash message.
			if ((typeof(t) == 'undefinded' || t != 'update_only') && jQuery(flash_target).html().lenght > 0)
				{
				jQuery(flash_target).hide('medium');
				}
			
			// activate the new tab
			jQuery(o).addClass('is_active');
			
			// get the data
			var a		= (t == 'do_update'? 'do_':'') + jQuery(o).attr("role"); // the action is in the role of the tab.
			
			if (a.match(/http:\/\//)) { window.location = a; return;}

			var post = {
					action: class_name, // this must have an action in your main plugins php ex add_action('wp_ajax_'.class_name, 'some_function',11);
					a: a
				};
				
			post 	=	serialize_toObj( jQuery(result_target).find(':input').serializeArray(), post );
			post 	=	ary_merge( params, post );
	
			jQuery.post(ajaxurl, post,
						function(data)
							{
								jQuery(result_target).html(data.result);
								if (typeof(data.flash_message) != 'undefined')
								{
									jQuery(flash_target).html(data.flash_message);
									jQuery(flash_target).show('fast');
									jQuery(flash_target).delay(2000).hide('slow');
								}
							},"json"
					);
			}
			
		/*
			==================================================================
				takes the jquery searializeArray style
				[0] = array (name = "ABC", value = "DEF")
				and flatens it into an object provided.
				o.[abc]	= "DEF"
			==================================================================  */
		function serialize_toObj(arr,o)
			{
				jQuery.each(arr, function(){ o[this.name]	= this.value; })
				return o;
			}
			
		/*
			==================================================================
				takes a 1 demensional object/array and merges it
				with the provided object. 
			==================================================================  */
		function ary_merge(src_ary,o)
			{
				jQuery.each(src_ary, function(k, v){ o[k]	= v; })
				return o;
			}
			
	}
	
	/*
	==================================================================
			Creates a hidden form and submits it. 
	==================================================================  */
	function GsBasicPost(post)
	{
		
		var form = '<form id = "GsBasicPost" style = "display: none;" method = "post"' + (typeof(post.action) != 'undefined'? ' action = "'+post.action+'" ' : '' )+ '>';
		//jQuery.('body').append('<form method = "post" >')
		
		jQuery.each( post,function(key,val) { if (key != 'action') { form += '<input name = "'+key+'" value = "' + val + '">'; } });
		
		form += '</form>';
		
		jQuery('body').append(form);
		
		jQuery('#GsBasicPost').submit();
		
	}
