<?php
/*
Plugin Name: GoSeese News Flash
Plugin URI: http://www.goseese.com/
Description: A plugin to allow you to create a news flash area/widget fully customizable
Version: alpha 1.1
Author: Jeff Seese
Author URI: http://www.goseese.com
License: Purchase only
*/

/*
 1.1 5/31/2010, changes to some wording/explanation on the styles tab.
*/

/*
	==================================================================
		Constants
	==================================================================  */
	$this_plugin_name	= 'gs-news-flash';
	if (!defined('PLUGIN_PATH')) { define('PLUGIN_PATH',dirname(__FILE__).'/../');}
	
/*
	==================================================================
		Enqued styles and scripts for the user facing pages.
	==================================================================  */
	wp_enqueue_style( $this_plugin_name, WP_PLUGIN_URL.'/'.$this_plugin_name.'/css/'.$this_plugin_name.'.css');
	wp_enqueue_script($this_plugin_name,WP_PLUGIN_URL.'/'.$this_plugin_name.'/js/'.$this_plugin_name.'.js', array('jquery'));

/*
	==================================================================
		includes.
	==================================================================  */
	include("includes/functions.php");					// function to interact with the class
	include("includes/GsNewsFlash.php");				// main class
	include("widgets/news_flash_widget.php");			// widgets
	
/*
	==================================================================
		Admin Menu && resources
	==================================================================  */
if (is_admin())
	{
	wp_enqueue_style( $this_plugin_name.'-admin', WP_PLUGIN_URL.'/'.$this_plugin_name.'/css/'.$this_plugin_name.'-admin.css');
	wp_enqueue_script($this_plugin_name.'-admin',WP_PLUGIN_URL.'/'.$this_plugin_name.'/js/'.$this_plugin_name.'-admin.js', array('jquery'));
	}
	
function gs_news_flash_admin() {
	add_submenu_page('tools.php', 'News Flash', 'News Flash',5, __file__,'show_news_flash_generator');
}	


/*
	==================================================================
		Actions && Hooks.
	==================================================================  */

add_action('admin_menu', 'gs_news_flash_admin',11);

/*
	==================================================================
		Short Codes
	==================================================================  */

add_shortcode('gs_news_flash', 'gs_news_flash');
