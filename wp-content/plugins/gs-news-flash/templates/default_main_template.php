<?php

/*
	==================================================================
		This is the main template that wraps around the news flash
		This content is static... does not change with each
		article of the news flash
	==================================================================  */
	

?>

<div class = "gs_news_flash_wrap [main_class]" id="[main_id]" style = "[main_style]">
<h3 class = "main_title" style = "[main_title_style]">[main_title]</h3>
[nwsfl_content]<!-- this is necessary for content do not delete this. -->
</div>