<?php

/*
	==================================================================
		This is the repeating content. This is populated and repeated
		for every news flash article
	==================================================================  */
	
	/* this is a sample of all fields that are available to you.
	  
	==== fields from the post ===
	[ID] => 1
    [post_author] => 1
    [post_date] => 2010-05-03 14:50:14
    [post_date_gmt] => 2010-05-03 14:50:14
    [post_content] => Welcome to WordPress. This is your first post. Edit or delete it, then start blogging!
    [post_title] => Hello world!
    [post_excerpt] => 
    [post_status] => publish
    [comment_status] => open
    [ping_status] => open
    [post_password] => 
    [post_name] => hello-world
    [to_ping] => 
    [pinged] => 
    [post_modified] => 2010-05-03 14:50:14
    [post_modified_gmt] => 2010-05-03 14:50:14
    [post_content_filtered] => 
    [post_parent] => 0
    [guid] => http://wpdev.goseese.com/?p=1
    [menu_order] => 0
    [post_type] => post
    [post_mime_type] => 
    [comment_count] => 1
    [filter] => raw
    
    === fields from the category ===
    [term_id] => 1
    [name] => Uncategorized
    [slug] => uncategorized
    [term_group] => 0
    [term_taxonomy_id] => 1
    [taxonomy] => category
    [description] => 
    [parent] => 0
    [count] => 2
    [object_id] => 1
    [cat_ID] => 1
    [category_count] => 2
    [category_description] => 
    [cat_name] => Uncategorized
    [category_nicename] => uncategorized
    [category_parent] => 0
    
    === fields from the plugin ===
    [nwsfl_ID] => 127362623204
    [title_source] => post_title
    [titlehover_source] => post_title
    [teaser_source] => post_excerpt
    [date_source] => post_date
    [effect] => slide_left_right
    [effect_time] => .5
    [pause_time] => 4
    [content_source] => 3,4,1
    [width] => 300px;
    [height] => 250px;
    [main_template] => default_main_template.php
    [content_template] => default_content_template.php
    [main_background] => #EEE;
    [content_background] => #CCC;
    [news_title_css] => color: #FF0000;
    [display_order] => post_date
    [post_count] => 5
    [category_display] => post_title
    [name] => Intial Test News Flash
    [class_name] => some_class
    [id_name] => some_id
    [border] => 1px solid #000;
    [background] => #fff
    [title] => margin: 0; padding: 0; text-align: center;
    [custom] => 
    [main_title] => Goseese News!
    [main_title_css] => font-size: 16px; margin: 0; padding: 0; text-align:center;
    [main_style] =>  width:300px; height:250px; border:1px solid #000; background:#EEE
    [more_anchor]	=> more &raquo;
    [inner_style]	=> padding: 10px;
    
    [nwsfl_date]			=>	Post Meta nwsfl_date
	[nwsfl_title]			=>	Post Meta nwsfl_title
	[nwsfl_description]		=>	Post Meta nwsfl_description
	[nwsfl_link]			=>	Post Meta nwsfl_link
	[nwsfl_custom]
		
	[debug_all]			=> dumps all data inside <pre> tags
	
	[guid]			=> the numerical link to the post.
	[perma_link]		=> the permalink to the post.
	[more_anchor]	=> will display what ever you have in there image, text etc as the more link.
						but if you display the post content and it has a <!--more--> tag in it, that will also become a more link.
						and the anchor text will be [more_anchor].
						
	[more_link]		=> build this in the script <a class = "nwsfl_more more-link" href = "[permalink]">[more_anchor]</a>
						and then the [more_anchor] will get swapped out.
						
						if you want to make a custom more link in this template. build your own <a> tag like this.
						<a href = "[guid] or [permalink]">[more_anchor]... or you could hard code the anchor text too.</a>
						
	including post meta.
	
	Any 
	*/

?>

<div class = "[content_classes]" style = "[content_style]" onclick = "document.location = '[perma_link]';"><!-- don't delete  content_classes or content_style codes -->
<div class = "nwsfl_inner_padding" style = "[inner_style]">
	<small class = "nwsfl_cat_name">[category_display]</small>
	<h3 class = "nwsfl_title" title = "[titlehover_source]" style = "[content_title_style]">[title_source]</h3>
	<p class = "nwsfl_teaser">[teaser_source]</p>
	<small class = "nwsfl_date">[date_source]</small>
	[more_link]
	<small style = "display: none;">[xdebug_all]</small>
</div><!-- end of nwsfl_inner_padding -->
</div><!-- end of this newsflash content wrap -->
