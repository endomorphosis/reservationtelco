<?php

/*
	==================================================================
		This is the repeating content. This is populated and repeated
		for every news flash article
	==================================================================  */
	?>

<div class = "[content_classes]" style = "[content_style]" onclick = "document.location = '[perma_link]';"><!-- don't delete  content_classes or content_style codes -->
<div class = "nwsfl_inner_padding" style = "[inner_style]">
	<small class = "nwsfl_cat_name">[category_display]</small>
	<div sytle = "clear: both;"></div>
	<h3 class = "nwsfl_title" title = "[titlehover_source]" style = "[content_title_style]">[title_source]&nbsp;[more_link]</h3>
	<small>[date_source]</small>
</div><!-- end of nwsfl_inner_padding -->
</div><!-- end of this newsflash content wrap -->
