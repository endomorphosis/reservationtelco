<?php

?>

<div class = "styles config_wrap">
	<p>Must be valid CSS for example 1px solid #000;</p>
	<table class = "gs_basic_datatable">
		<tr><td colspan = "2"><i>Main Content CSS</i></td></tr>
		<tr><td class = "labeltd">border:</td><td><input type = "text" name = "nwsfl[border]" class = "px150" value = "<?php echo htmlentities($gs_newsflash['border']); ?>" onChange = "validate_css(this)"  role = "border"></td></tr>
		<tr><td class = "labeltd">width:</td><td><input type = "text" name = "nwsfl[width]" class = "px90" value = "<?php echo $gs_newsflash['width']; ?>" onChange = "validate_css(this)" role = "width"></td></tr>
		<tr><td class = "labeltd">height:</td><td><input type = "text" name = "nwsfl[height]" class = "px90" value = "<?php echo $gs_newsflash['height']; ?>" role = "height"></td></tr>
		<tr><td class = "labeltd">background:</td><td><input type = "text" name = "nwsfl[main_background]" class = "px150" value = "<?php echo htmlentities($gs_newsflash['main_background']); ?>" onChange = "validate_css(this)"  role = "background"></td></tr>
		<tr><td class = "labeltd">Main Title<small><br>this must be fully valid css ex:<br>font-size: 15px; font-weight: bold;</small></td><td><input type = "text" name = "nwsfl[main_title_style]" class = "px150" value = "<?php echo htmlentities($gs_newsflash['main_title_style']); ?>" onChange = "validate_css(this)"  role = "style"></td></tr>
		
		<tr><td colspan = "2"><i>Dynamic Content CSS</i></td></tr>
		<tr><td class = "labeltd"><span class = "gs_info" title = "This is a div that is inside the inner content div, but contains all the content. This is where you would create an inner padding, or an inner border;">inner:</span></td><td><input type = "text" name = "nwsfl[inner_style]" class = "px300" value = "<?php echo $gs_newsflash['inner_style']; ?>" onChange = "validate_css(this)"  role = "background"></td></tr>
		<tr><td class = "labeltd">background:</td><td><input type = "text" name = "nwsfl[content_background]" class = "px150" value = "<?php echo htmlentities($gs_newsflash['content_background']); ?>" onChange = "validate_css(this)"  role = "background"></td></tr>
		<tr><td class = "labeltd">News Title<small><br>this must be fully valid css ex:<br>font-size: 15px; font-weight: bold;</small></td><td><input type = "text" name = "nwsfl[content_title_style]" class = "px150" value = "<?php echo htmlentities($gs_newsflash['content_title_style']); ?>"onChange = "validate_css(this)"  role = "title"></td></tr>
	</table>
	<div id = "nwsfl_diagram"></div>
</div>