<?php

?>

<div class = "content_display config_wrap">
Choose the type of content to display, and what component of the source the content comes from:
	<table class = "gs_basic_datatable">
		<tr><td class = "labeltd">Category Display</td><td><select name = "nwsfl[category_display]"><?php echo gs_build_option_selection($gs_newsflash['category_display'],$gs_nwsfl->content_source); ?></select></td></tr>
		<tr><td class = "labeltd">Title</td><td><select name = "nwsfl[title_source]"><?php echo gs_build_option_selection($gs_newsflash['title_source'],$gs_nwsfl->content_source); ?></select></td></tr>
		<tr><td class = "labeltd">Title Hover</td><td><select name = "nwsfl[titlehover_source]"><?php echo gs_build_option_selection($gs_newsflash['titlehover_source'],$gs_nwsfl->content_source); ?></select></td></tr>
		<tr><td class = "labeltd">Teaser/Content</td><td><select name = "nwsfl[teaser_source]"><?php echo gs_build_option_selection($gs_newsflash['teaser_source'],$gs_nwsfl->content_source); ?></select></td></tr>
		<tr><td class = "labeltd">Date</td><td><select name = "nwsfl[date_source]"><?php echo gs_build_option_selection($gs_newsflash['date_source'],$gs_nwsfl->content_source); ?></select></td></tr>
		<tr><td class = "labeltd"><span class = "gs_info" title = "this content will appear in the post content more tags, or it will appear in the [more_anchor] or [more_link] codes see the content default template for more explanation">More Anchor</span></td><td><input type = "text" name = "nwsfl[more_anchor]" class = "px300" value = "<?php echo htmlentities($gs_newsflash['more_anchor']); ?>"></td></tr>
</table>
</div>

