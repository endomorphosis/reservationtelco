<?php

?>

<div class = "docs config_wrap">
<p>A news flash short code is included in a page by copying the short code from the first tab into your page, however a news flash will generally be used as a sidebar widget. To use a news flash that you have configured as a widget simply drag the news flash widget to the widget aware sidebar/area from the appearnce =&gt; widgets setting. Then Add a widget title if you want one to display, and select your news flash from the drop down selection.</p>
<p>News Flash</p>

<blockquote>
<p style="text-align: left;">To build a news flash short code start by giving it a Name that you will easily recognize. This name does not display anyplace it&#8217;s only to help you keep track of your news flash short codes.</p>
<p style="text-align: left;">Next Give your widget a title if you want a title to display in the main template wrapping template.</p>
</blockquote>
<p>Styles</p>
<blockquote><p>From the &#8220;Styles&#8221; tab you can see that you have control over many of the most common components of the news flash, however, it&#8217;s not necessary to use any of these fields. You can also control all of the css of the news flash via custom css.</p>
<p>The CSS you do put in those fields must be valid CSS. In other words entering 300 for the width field is not valid CSS it must be 300px; Another example is the border field. Putting 1px in there won&#8217;t due. You can put in the full CSS short hand for the border here for example. 1px dotted #333;</p>

<p>In most cases you will want the border of the main background and the border of the dynamic content (the actual news flashes) to be the same, otherwise the main background may show in gaps/margins around the dynamic content.</p>
<p>If you do not specify a background color for the dynamic content IE might have trouble displaying fonts clearly especially if you are using the fade effect.</p></blockquote>
<p>Content Display</p>
<blockquote><p>Some pre-defined content types are here for your convenience, however you built a custom template to display any content you wish.</p></blockquote>
<blockquote><p>Category Title: This is if you want the category title to display in the news flash. Select content from any of the fields.</p></blockquote>
<blockquote><p>Title: This is usually the title of the post, however you can make it pull in data from any argument of the post.</p></blockquote>
<blockquote><p>Teaser/Content: This is intended to be some short teaser of the article. Depending on the width and height of your news flash you may not have room for much content. You can use the excerpt field to have better control of the content that displays in that area.</p></blockquote>
<blockquote><p>Date: The displayed date can come from any field as well. Usually the actual post date is not really what you want to show, or the format is not desireable. You can use the custom field nwsfl_date to put a more appropriate date in that field.</p></blockquote>
<blockquote><p>More Anchor: This is the anchor text for the more or read more link. Any valid html can go here, so it is possible to put in an image tag and have your more tag be an actual image. You can also customize your template to a hard coded text or image.</p></blockquote>

<p>Templates</p>
<blockquote><p>This plugin comes with 1 main template and two content templates. However, you can build custom templates to suit your needs. Use the provided templates as a guide. Basically, any field of a post or category (for example cat_name is the field of the post categories name) can be included in the template by wrapping it in code brackets. [cat_name] would get replaced with the post category name. There is a list of most of the fields available to you in the default content template. you can also insert this field [debug_all] to see all the post, category and news flash fields that are available to you.</p></blockquote>
<p>Effects</p>
<blockquote><p>Choose the transition effect either a slide or fade from the drop down. Configure the time the effect takes and the pause time that the news flash is displayed before the next slide is shown.</p></blockquote>

</div>

