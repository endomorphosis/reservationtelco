<?php

?>

<div class = "transitions config_wrap">
Choose the transition type, and transition/pause speeds
	<table class = "gs_basic_datatable">
			<tr><td class = "labeltd">Transition Effect</td><td><select name = "nwsfl[effect]"><?php echo gs_build_option_selection($gs_newsflash['effect'],$gs_nwsfl->effects); ?></select></td></tr>
			<tr><td class = "labeltd">Effect time (s)</td><td><input type = "text" name = "nwsfl[effect_time]" class = "px90" value = "<?php echo $gs_newsflash['effect_time']; ?>"></td></tr>
			<tr><td class = "labeltd">Pause time (s)</td><td><input type = "text" name = "nwsfl[pause_time]" class = "px90" value = "<?php echo $gs_newsflash['pause_time']; ?>"></td></tr>
	</table>
</div>