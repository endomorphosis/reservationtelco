<?php

?>

<div class = "templates config_wrap">
Choose the files to use for the main wrapping template and the content template
	<table class = "gs_basic_datatable">
		<tr><td class = "labeltd">Main Template</td><td><select name = "nwsfl[main_template]"><?php echo gs_build_option_selection($gs_newsflash['main_template'],$gs_nwsfl->templates()); ?></select></td></tr>
		<tr><td class = "labeltd">Content Template</td><td><select name = "nwsfl[content_template]"><?php echo gs_build_option_selection((strlen($gs_newsflash['content_template']) > 0? $gs_newsflash['content_template'] : 'default_content_template.php'),$gs_nwsfl->templates()); ?></select></td></tr>
	</table>
	<p>There are two templates required to build a news flash. One is the main wrapping div. The second is the dynamic content template.
	<br><br>
	Both of these can be completely customized.
	<br><br>
	They are located in plugins/gs-news-flash/templates/
	</p>
</div>

