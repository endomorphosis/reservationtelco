<?php
	$all_cats			= gs_wp_category_heirarchy(0);
	$content_sources	= split(',',$gs_newsflash['content_source']);
?>

<div class = "content_source config_wrap">
Choose news flash source categories
	<table class = "gs_basic_datatable">
		<?php
			if (is_array($all_cats) )
			{
				foreach($all_cats as $cat)
					{
						print "\n".'<tr><td><input type = "checkbox" name = "nwsfl[content_source][]" value = "'.$cat['ID'].'" '.(in_array($cat['ID'],$content_sources)? 'checked = "checked"': '').'></td><td>'.htmlentities($cat['cat_name']).'</td></tr>';
					}
				
			}// end of is_array all_cats.
			else
			{
				print "\n<tr><td >No Categories To Choose From</td></tr>";
			}
			?>
	</table>
	
	<table class = "gs_basic_datatable">
		<tr><td class = "labeltd">Sort posts by</td><td><select name = "nwsfl[display_order]"><?php echo gs_build_option_selection($gs_newsflash['display_order'],$gs_nwsfl->display_order); ?></select></td></tr>
		<tr><td class = "labeltd">Number of posts to show</td><td><input type = "text" name = "nwsfl[post_count]" class = "px90" value = "<?php echo htmlentities($gs_newsflash['post_count']); ?>"></td></tr>
	</table>
	
</div>