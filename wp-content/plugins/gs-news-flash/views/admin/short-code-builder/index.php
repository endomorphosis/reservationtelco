<?php

	if (!is_admin()) { die(); }
	
	$gs_nwsfl	= new GsNewsFlash();
	
	// handle save/form post.
	
	if (is_array($_POST['nwsfl']) && $_POST['do_delete'] !== 'true')
		{
			$_POST['nwsfl_ID']	=	$gs_nwsfl->update($_POST['nwsfl']);
		}
		
	elseif($_POST['do_delete'] === 'true')
		{
			$gs_nwsfl->delete($_POST['nwsfl']['nwsfl_ID']);
			unset($_POST);
		}
		
	
	
	
	$gs_newsflash	=	$gs_nwsfl->get($_POST['nwsfl_ID']);
	
?>

<div style ="display:none;">
		<br>view:<?php print __FILE__;?>
		<br>this is only to force the comment to dislpay. This div serves no purpose.
</div>

<div class="wrap gsnwsfl_settings">
<div id="icon-options-general" class="icon32"></div><h2>News Flash Short Code Generator</h2>
<p>Use these settings to generate a news flash short code to paste into your web page.</p>

<?php
	$get_all_existing	= $gs_nwsfl->get_all();
	if (count($get_all_existing) > 0 )
	{
		print '<form name = "existing_nwfs" method = "post"><select name = "nwsfl_ID" id = "existing_news_flashes"><option value = "">Select An Existing News Flash</option>' . gs_build_option_selection($_POST['nwsfl_ID'],$get_all_existing) . '</select></form>';
	}
?>
	<ul class = "gs_basic_nav">
		<li class = "nav-tab <?php echo ($_POST['gs_basic_nav_current_panel'] === 'docs'? 'is_init' : ''); ?>" role = "docs">Docs/Usage</li>
		<li class = "nav-tab <?php echo ((!isset($_POST['gs_basic_nav_current_panel']) || $_POST['gs_basic_nav_current_panel'] === 'main_info')? 'is_init' : ''); ?>" role = "main_info">News Flash</li>
		<li class = "nav-tab <?php echo ($_POST['gs_basic_nav_current_panel'] === 'styles'? 'is_init' : ''); ?>" role = "styles">Style</li>
		<li class = "nav-tab <?php echo ($_POST['gs_basic_nav_current_panel'] === 'content_display'? 'is_init' : ''); ?>" role = "content_display">Content Display</li>
		<li class = "nav-tab <?php echo ($_POST['gs_basic_nav_current_panel'] === 'templates'? 'is_init' : ''); ?>" role = "templates">Templates</li>
		<li class = "nav-tab <?php echo ($_POST['gs_basic_nav_current_panel'] === 'content_source'? 'is_init' : ''); ?>" role = "content_source">Content Source</li>
		<li class = "nav-tab <?php echo ($_POST['gs_basic_nav_current_panel'] === 'transitions'? 'is_init' : ''); ?>" role = "transitions">Transition</li>
	</ul>
<form method = "post" id = "news_flash_form">
	<input type = "hidden" id = "curr_nwsfl_ID" name = "nwsfl[nwsfl_ID]" value = "<?php echo (((strlen($_POST['nwsfl_ID']) > 0) && (is_numeric($_POST['nwsfl_ID'])) )? $_POST['nwsfl_ID'] : 'add'); ?>" />
	<input type = "hidden" id = "gs_basic_nav-current_panel" name = "gs_basic_nav_current_panel" value = "" />
	<input type = "hidden" id = "do_delete" name = "do_delete" value = "" />
	<div style = "clear: both;"></div>
<?php 
include_once(PLUGIN_PATH.'/gs-news-flash/views/admin/short-code-builder/main_info.php');
include_once(PLUGIN_PATH.'/gs-news-flash/views/admin/short-code-builder/styles.php');
include_once(PLUGIN_PATH.'/gs-news-flash/views/admin/short-code-builder/content_display.php');
include_once(PLUGIN_PATH.'/gs-news-flash/views/admin/short-code-builder/content_source.php');
include_once(PLUGIN_PATH.'/gs-news-flash/views/admin/short-code-builder/transitions.php');
include_once(PLUGIN_PATH.'/gs-news-flash/views/admin/short-code-builder/news_flash_preview.php');
include_once(PLUGIN_PATH.'/gs-news-flash/views/admin/short-code-builder/templates.php');
include_once(PLUGIN_PATH.'/gs-news-flash/views/admin/short-code-builder/docs.php');
?>

</div>
 <input type="submit" class="button-primary" style = "margin-top: 10px;" value="<?php _e('Save Changes') ?>" />
 <input type="button" class="button-secondary" style = "margin-top: 10px;" value="<?php _e('Delete News Flash') ?>" onclick = "confirm_delete_newsflash()"/>
</form>

<script>
	jQuery(document).ready( function () {
		gsnwsfl_nav_listeners();
		jQuery('#existing_news_flashes').change( function () { document.forms['existing_nwfs'].submit(); } ); });
	
	
</script>