<?php

?>

<div class = "main_info config_wrap">

	<table class = "gs_basic_datatable">
		<tr><td class = "labeltd"><span class = "gs_info" title = "This is for your identification only, this has no effect on the news flash">Name</span></td><td><input type = "text" name = "nwsfl[name]" class = "px150" value = "<?php echo htmlentities($gs_newsflash['name']); ?>"></td></tr>
		<tr><td class = "labeltd"><span class = "gs_info" title = "This the static title name">Main Title</span></td><td><input type = "text" name = "nwsfl[main_title]" class = "px150" value = "<?php echo htmlentities($gs_newsflash['main_title']); ?>"></td></tr>
		<tr><td class = "labeltd">class</td><td><input type = "text" name = "nwsfl[main_class]" class = "px150" value = "<?php echo htmlentities($gs_newsflash['main_class']); ?>"></td></tr>
		<tr><td class = "labeltd"><span class = "gs_info" title = "Warning: setting this parameter will cause an error if your have the same news flash shortcode on a web page more then once. ID's must be unique">id</span></td><td><input type = "text" name = "nwsfl[main_id]" class = "px150" value = "<?php echo htmlentities($gs_newsflash['main_id']); ?>"></td></tr>
		<tr><td class = "labeltd">Short Code</td><td>[gs_news_flash id=<?php echo $_POST['nwsfl_ID']; ?>]</td></tr>
		<tr><td class = "labeltd">PHP Code</td><td>&lt;?php gs_news_flash(<?php echo $_POST['nwsfl_ID'];?>)&#59; ?&gt;</td></tr>
	</table>
	<div id = "nwsfl_diagram"></div>
</div>