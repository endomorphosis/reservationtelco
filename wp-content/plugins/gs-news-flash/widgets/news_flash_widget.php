<?php

add_action( 'widgets_init', 'register_gs_news_flash_widget' );

function register_gs_news_flash_widget() {
	register_widget( 'Gs_News_Flash_Widget' );
}

class Gs_News_Flash_Widget extends WP_Widget {
	
	function Gs_News_Flash_Widget() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'gs_news_flash_widget', 'description' => 'A widget of a pre-configured News Flash Shortcode' );

		/* Widget control settings. */
		$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'gs_news_flash-widget' );

		/* Create the widget. */
		$this->WP_Widget( 'gs_news_flash-widget', 'News Flash', $widget_ops, $control_ops );
	}
	
	function widget( $args, $instance ) {
		extract( $args );

		/* User-selected settings. */
		$title = apply_filters('widget_title', $instance['title'] );
		

		/* Before widget (defined by themes). */
		echo $before_widget;

		/* Title of widget (before and after defined by themes). */
		if ( $title )
			echo $before_title . $instance['title'] . $after_title;

		
		$gs_nwsfl	= new GsNewsFlash();
		echo $gs_nwsfl->build($instance['nwsfl_ID']);

		echo $after_widget;
	}
	
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Strip tags (if needed) and update the widget settings. */
		$instance['title'] = $new_instance['title'];
		$instance['nwsfl_ID'] = $new_instance['nwsfl_ID'];

		return $instance;
	}
	
	function form( $instance ) {

		/* Set up some default widget settings. */
		$gs_nwsfl	= new GsNewsFlash();
		$instance = wp_parse_args( (array) $instance, $defaults );
		$get_all_existing	= $gs_nwsfl->get_all();
		
?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label><br>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo htmlentities($instance['title']); ?>" style="width:90%;" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'date_from' ); ?>">News Flash:</label><br>
			<select id="<?php echo $this->get_field_id( 'nwsfl_ID' ); ?>" name="<?php echo $this->get_field_name( 'nwsfl_ID' ); ?>" ><?php echo gs_build_option_selection($instance['nwsfl_ID'],$get_all_existing) ?></select>
		</p>
	<?php
	}

}
		