/*
	==================================================================
		Admin function for the News Flash Builder
		
	==================================================================  */
	
	/*
		==================================================================
			Nav Tab Listners. 
		==================================================================  */
		
	function gsnwsfl_nav_listeners()
		{
			jQuery('.gs_basic_nav .nav-tab').bind('click', function () { handle_gsnwsfl_nav_click(this); });
			
			jQuery('.gs_basic_nav .nav-tab.is_init').trigger('click');
		}
		
	function handle_gsnwsfl_nav_click(o)
		{
			// hide visible panel.
			var panel	= jQuery(o).attr("role");
			
			if( jQuery('.gsnwsfl_settings .config_wrap.is_active').length > 0 )
				{
					
					jQuery('.gsnwsfl_settings .config_wrap.is_active').slideToggle('fast',function () { jQuery('.'+panel+'.config_wrap').slideToggle('medium'); } );
					
					jQuery('.gsnwsfl_settings .config_wrap.is_active').removeClass('is_active');
					
					jQuery('.gs_basic_nav .nav-tab').removeClass('is_active');
				}
			else
				{
					jQuery('.'+panel+'.config_wrap').slideToggle('medium');
				}
				
					jQuery(o).addClass('is_active');
					
					jQuery('.'+panel+'.config_wrap').addClass('is_active');
					
					jQuery('#gs_basic_nav-current_panel').val(panel);
			
		}
		
	function confirm_delete_newsflash(o)
		{
			if (!confirm("Are you sure you want to delete this news flash. This can not be undone")) { return; }
			
			jQuery('#do_delete').val('true');
			
			jQuery('#news_flash_form').trigger('submit');
		}
		
	function validate_css(o)
		{
			var val_type	= jQuery(o).attr("role");
			var cur_val		= jQuery(o).val();
			
			switch (val_type)
				{
					case 'width':
					case 'height':
						{
							cur_val = cur_val.replace(/\D+/,'');
							if (cur_val.length > 0)
								{
									cur_val = cur_val+'px;';
								}
								
						jQuery(o).val(cur_val);
						}
					default:
						{
							if (!cur_val.match(/;$/) && cur_val.length > 0) { alert( cur_val + ' is not valid CSS. please fix this entery so that it is valid CSS markup. '); }
						}
					
					
					
					
				}
		}