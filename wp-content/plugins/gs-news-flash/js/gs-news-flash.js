/*
	==================================================================
		javascript to control the timing and display of the
		news flash. 
	==================================================================  */

	jQuery(document).ready(
		function ()
			{
				
				jQuery('.gs_news_flash_wrap').each( function () { jQuery.data(this,"gsnwsfl",new nwsfl_animation(this)); jQuery.data(this,"gsnwsfl").start(); } );
				
				//jQuery('.gs_news_flash_wrap').each( function () { var ct	= new nwsfl_animation(this); ct.start(); } );
			
			});

	function nwsfl_animation(o) {
		
		var main_container	= o;
		
		var main_title = jQuery(o).find('.main_title').html();
		
		var _effect	= jQuery.parseJSON( jQuery(o).find('.nwfsl_content-wrap').attr("role") );
		
		this.start	= function() { start_animation(); }
		
		function start_animation(current_o)
			{
				if (typeof(current_o) == 'undefined' || jQuery(current_o).next('.nwsfl_content').length == 0)
					{
						var next_o	=	jQuery(o).find('.nwsfl_content.is_init').get(0);
					}
					
				else
					{
						var next_o	=	jQuery(current_o).next('.nwsfl_content');
					}
					
				// get the initial start object. is_init
				pre_animate(next_o);
				
				// do the initial animation.
				if (typeof(current_o) != 'undefined') { do_unanimate(current_o); }
				do_animate(next_o);
				
				var delay	= _effect.pause_time * 1000;
				//setInterval(function() { alert(main_title + ' timeout - ' + delay);},delay);
			};
			
		function pre_animate(o)
			{
				switch(_effect.effect)
					{
						case 'slide_left_right':
							{
								jQuery(o).css({'margin-left' : -1 * (jQuery(main_container).outerWidth() + 2) });
								jQuery.data(o,"unanimate",{property: "margin-left",value: (jQuery(main_container).outerWidth() + 2) });
							}break;
							
						case 'slide_right_left':
							{
								jQuery(o).css({'margin-left' : (jQuery(main_container).outerWidth() + 2) });
								jQuery.data(o,"unanimate",{property: "margin-left",value: -1 * (jQuery(main_container).outerWidth() + 2) });
							}break;
							
						case 'slide_top_bottom':
							{
								jQuery(o).css({'margin-top' : -1 * (jQuery(o).outerHeight() + 2) });
								jQuery.data(o,"unanimate",{property: "margin-top",value: (jQuery(main_container).outerHeight() + 2) });
							}break;
							
						case 'slide_bottom_top':
							{
								jQuery(o).css({'margin-top' : (jQuery(main_container).outerHeight() + 2) });
								jQuery.data(o,"unanimate",{property: "margin-top",value: -1 * (jQuery(o).outerHeight() + 2) });
							}break;
							
						case 'fade':
							{
								
								jQuery(o).css({'top' : -9999,'visibility' : 'visible'});
								jQuery(o).css({'opacity' : 0,'margin' : 0, 'left': 0,'top' : 0});
								jQuery.data(o,"unanimate",{property: "opacity",value: 0 });
							}break;
					}
			
			}
			
		function do_animate(o)
			{
				if (_effect.effect == 'fade')
				{
					jQuery(o).animate({'opacity': 1},(_effect.effect_time * 1000), function() { setTimeout(function() { start_animation(o); },(_effect.pause_time * 1000)); });
				}
				else
				{
					jQuery(o).css({'visibility' : 'visible'});
					jQuery(o).animate({'margin-left': 0, 'margin-top': 0},(_effect.effect_time * 1000), function() { setTimeout(function() { start_animation(o); },(_effect.pause_time * 1000)); });
				}
				
			}
			
		function do_unanimate(o)
			{
				var property	= jQuery.parseJSON( '{"'+jQuery.data(o,"unanimate").property+'":"'+jQuery.data(o,"unanimate").value+'"}' );
				//var value		= ;
				jQuery(o).animate(property,(_effect.effect_time * 1000),function() { jQuery(o).css({'visibility' : 'hidden'}); });
			}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
	