<?php


function show_news_flash_generator()
	{
		include_once(PLUGIN_PATH.'/gs-news-flash/views/admin/short-code-builder/index.php');
	}
	
function gs_news_flash($args)
	{
		$gs_nwsfl	= new GsNewsFlash();
		$is_print = false; 
		if (!is_array($args) && is_numeric($args) ) { $args['id'] = $args; $is_print = true;}
		
		if (!is_numeric($args['id'])) { return "<p>No News Flash Requested</p>"; }
		
		$return =  $gs_nwsfl->build($args['id']);
		
		if ($is_print)
			{
				print $return;
			}
		return $return;
		
	}
