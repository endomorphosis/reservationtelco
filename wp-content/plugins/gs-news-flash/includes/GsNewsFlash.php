<?php

class GsNewsFlash {
	
	/*
		==================================================================
			Public Vars
		==================================================================  */
	
	public $content_source	= array(
		''			=> 	'None',
		'cat_name'		=>	'Category Name',
		'description'	=> 'Category Description',
		'post_title'	=>	'Post Title',
		'post_content'	=>	'Post Content',
		'post_excerpt'	=>	'Excerpt',
		'post_date'		=>	'Publish date',
		'nwsfl_date'		=>	'Post Meta nwsfl_date',
		'nwsfl_title'		=>	'Post Meta nwsfl_title',
		'nwsfl_description'	=>	'Post Meta nwsfl_description',
		'nwsfl_link'		=>	'Post Meta nwsfl_link',
		'nwsfl_custom'		=>	'Post Meta nwsfl_custom'
	);
	
	
	public $display_order	= array(
		'post_date'						=>	'Post Date',
		'random_all'					=>	'Randomize All Posts',
		'random_recent'					=>	'Randomize Of Recent Posts',
		'non_consecutive_categories'	=>	'Non Consecutive Categories'
	);
	
	public	$effects	=	array(
		'slide_left_right'	=>	'Slide Left to Right',
		'slide_right_left'	=>	'Slide Right to Left',
		'slide_top_bottom'	=>	'Slide Top to Bottom',
		'slide_bottom_top'	=>	'Slide Bottom to top',
		'fade'				=>	'Fade'
	);
	
	public $default_post_count	= 10;
		
	/*
		==================================================================
			main function
		==================================================================  */
	public function GsNewsFlash()
		{
			
			// nothing?
			
		}
		
	/*
		==================================================================
			a function to get a list of all existing
			news flash configs. in an array 
		==================================================================  */
	public function get_all()
		{
			
			global $wpdb;
			$all_nwsfl	= gs_get_options('gs_nwsfl');
			
			if (!is_array($all_nwsfl) || count($all_nwsfl) < 1 ) { return; }
			
			$list = array();
			
			foreach ($all_nwsfl as $option)
				{
					// getting only a list of ID's and names.
					unset($matches);
					if (!preg_match("/gs_nwsfl-(\d+)-name/",$option->option_name,$matches) ) { continue; }
					
					$list[] = array( 'value'	=> $matches[1],	'display'	=> $option->option_value.' : '.$matches[1]);
					
				}
			return $list;
			
		}
		
	/*
		==================================================================
			A function to return an array of a news flash object. 
		==================================================================  */
	public function get($nwsfl_ID = null)
		{
			if (is_null($nwsfl_ID) )  { return; }
			
			$nwfs_options	= gs_get_options('gs_nwsfl-'.$nwsfl_ID.'-');
			if (!is_array($nwfs_options)) { return; }
			
			$news_flash['nwsfl_ID']	= $nwsfl_ID;
			foreach ($nwfs_options	as $option)
				{
					$key	= preg_replace("/gs_nwsfl-".$nwsfl_ID."-/",'',$option->option_name);
					
					$news_flash[$key]	= $option->option_value;
				}
			return $news_flash;
		}
		
	/*
		==================================================================
			A function to delete the news flash
		==================================================================  */
	public function delete($nwsfl_ID)
		{
			if (!is_numeric($nwsfl_ID)) { return; }
			
			$news_flash	= $this->get($nwsfl_ID);
			
			if (!is_array($news_flash)) { return; }
			
			foreach($news_flash as $meta_key	=> $meta_value)
				{
					delete_option ('gs_nwsfl-'.$nwsfl_ID.'-'.$meta_key);
				}
		}
		
		
	/*
		==================================================================
			A function to create a unique ID and update the
			newsflash with curretn data. 
		==================================================================  */
	public function add()
		{
			return time().(int)rand(0,9).(int)rand(0,9); // a hackish unique id.
		}
		
	/*
		==================================================================
			Save/update the current data. 
		==================================================================  */
	public function update($nwsfl)
		{
			
			if ($nwsfl['nwsfl_ID'] === 'add') { $nwsfl['nwsfl_ID'] = $this->add(); }
			
			if(!is_numeric($nwsfl['nwsfl_ID'])) { return; }
			
			$nwsfl_ID	= $nwsfl['nwsfl_ID'];
			
			foreach ($nwsfl as $key	=> $value)
				{
					
					if (preg_match("/nwsfl_ID/",$key)) { continue; }
					
					if (is_array($value)) { $value = join(',',$value); }
					
					update_option( "gs_nwsfl-".$nwsfl_ID."-".$key, stripcslashes($value) );
					
				}
				
			return $nwsfl_ID;
		}
		
		
	/*
		==================================================================
			A function to look in the templates directory
			and build a list. 
		==================================================================  */
	public function templates()
		{
			
			$dir	= opendir(PLUGIN_PATH.'gs-news-flash/templates/');
			$temps	= array();
			while( $dir_name	= readdir($dir) )
				{
					if (preg_match("/^\./",$dir_name)) { continue; }
					$temps[$dir_name]	=	$dir_name;
				}
			return $temps;
		}
		
	
	/*
		==================================================================
			build the news flash
		==================================================================  */
	public function build($id)
		{
			
			$NewsFlash	=	$this->get($id);
			
			if (strlen($NewsFlash['main_template']) < 1 || strlen($NewsFlash['content_template']) < 1)
				{
					return "Template Error: You must configure the main template and content template";
				}
			if ($NewsFlash['main_template'] === $NewsFlash['content_template'])
				{
					return "Template Error: Main template and content template can not be the same";
				}
			if (!file_exists(PLUGIN_PATH.'gs-news-flash/templates/'.$NewsFlash['main_template'])) { return "Template Error: main template file is not readable or does not exist"; }
			if (!file_exists(PLUGIN_PATH.'gs-news-flash/templates/'.$NewsFlash['content_template'])) { return "Template Error: content template file is not readable or does not exist"; }
			
			$NewsFlash['main_style']	=
				(strlen($NewsFlash['width']) > 0? ' width:'.$NewsFlash['width'] : '').
				(strlen($NewsFlash['height']) > 0? ' height:'.$NewsFlash['height'] : '').
				(strlen($NewsFlash['border']) > 0? ' border:'.$NewsFlash['border'] : '').
				(strlen($NewsFlash['main_background']) > 0? ' background:'.$NewsFlash['main_background'] : '');
				
			if (strlen($NewsFlash['main_title']) < 1) { $NewsFlash['main_title_style'] = 'display: none;'; }
				
				
			ob_start();
			include(PLUGIN_PATH.'gs-news-flash/templates/'.$NewsFlash['main_template']);
			$main_template	= ob_get_contents();
			ob_clean();
			
			$NewsFlash ['nwsfl_content']	= '<div style = "' . (strlen($NewsFlash['width']) > 0? ' width:'.$NewsFlash['width'] : '').(strlen($NewsFlash['height']) > 0? ' height:'.$NewsFlash['height'] : '') . '" class = "nwfsl_content-wrap" role = \'{"effect_time":"' .$NewsFlash['effect_time'].'","pause_time":"'.$NewsFlash['pause_time']. '","effect":"'. $NewsFlash['effect'] .'"}\'>'.join("\n",$this->get_posts($id)).'</div>';
			foreach ($NewsFlash as $key => $value)
				{
					$main_template	= preg_replace("/\[".$key."\]/",$value,$main_template);
				}
				
			
			
			return ($main_template);
			
		}
		
	public function get_posts($id, $historical_ids = null,$last_category = null)
		{
			$NewsFlash	=	$this->get($id);
			
			//default post_count.
			if (!is_numeric($NewsFlash['post_count'])) { $NewsFlash['post_count'] = $this->default_post_count;}
			
			// get the post per rules.
			switch($NewsFlash[display_order])
				{
					case 'non_consecutive_categories':
						{
							foreach(split(",",$NewsFlash['content_source']) as $cat_ID )
								{
									$args['orderby']		= 'post_date';
									$args['category']		= $cat_ID;
									$args['numberposts']	= $NewsFlash['post_count'];
									
									$category_posts[$cat_ID]	= get_posts($args);
								}
								
								
							$index = 0;
							while ($index < $NewsFlash['post_count'])
								{
									foreach($category_posts as $cat_ID	=>	$this_category_posts)
										{
											if (!is_array($this_category_posts) || count($this_category_posts) < 1) { continue; }
											$all_posts[]				= array_shift( $this_category_posts );
											$category_posts[$cat_ID]	= $this_category_posts;
										}
									
									$index++;
								}
								
						}break;
						
					case 'random_all':
						{
							$args['orderby']		= 'random';
							$args['category']		= $NewsFlash['content_source'];
							$args['numberposts']	= $NewsFlash['post_count'];
							
							$all_posts	= get_posts($args);
						}break;
						
					case 'random_recent':
						{
							$args['orderby']		= 'post_date';
							$args['category']		= $NewsFlash['content_source'];
							$args['numberposts']	= $NewsFlash['post_count'];
							
							$all_posts	= get_posts($args);
							
							shuffle($all_posts);
						}break;
						
					case 'post_date':
						{
							$args['orderby']		= 'post_date';
							$args['category']		= $NewsFlash['content_source'];
							$args['numberposts']	= $NewsFlash['post_count'];
							
							$all_posts	= get_posts($args);
						}break;
				}
				
			if (!is_array($all_posts)) { return array("<p>No Recent Posts Available<p>"); }
			
			/*
			post_count
			content_source
			display_order
			*/
			
			ob_start();
			include(PLUGIN_PATH.'gs-news-flash/templates/'.$NewsFlash['content_template']);
			$content_template	= ob_get_contents();
			ob_clean();
			
			$is_init	= 'is_init ';
			// cause I am messing with some globals
			// put them back when done. 
			foreach($all_posts as $post)
				{
					$post_html	= $content_template;
					
					$category	= get_the_category($post->ID);
					
					$content_classes = 'nwsfl_content '.$is_init.$category[0]->category_nicename.' '.$post->post_name.' post_ID-'.$post->ID;
					$is_init	= '';
					
					$post	= $this->simulate_get_the_content($post,$NewsFlash['more_anchor']);
					
					$post_meta	= get_post_custom($post->ID);
					
					if (is_array($post_meta))
						{
							foreach ($post_meta as $meta_name => $meta_array)
								{
									$post->$meta_name	= $meta_array[0];
								}
						}
					
					$content_style = 	(strlen($NewsFlash['width']) > 0? ' width:'.$NewsFlash['width'] : '').
										// no height, the main wrap will cut this off where needed.
										(strlen($NewsFlash['content_background']) > 0? ' background:'.$NewsFlash['content_background'] : '');
					// merge the category and post.
					
					if ($NewsFlash['teaser_source']	=== 'post_content')
						{
							$post_html	= preg_replace("/\[more_link\]/",'',$post_html); // no more link it comes from the post_content. 
						}
					else
						{
							$post_html	= preg_replace("/\[more_link\]/",'<a class = "nwsfl_more more-link" href = "[perma_link]">[more_anchor]</a>',$post_html); // no more link it comes from the post_content.
						}
						
					$post_html	= preg_replace("/\[more_anchor\]/",$NewsFlash['more_anchor'],$post_html);
					$post_html	= preg_replace("/\[content_classes\]/",$content_classes,$post_html);
					$post_html	= preg_replace("/\[content_style\]/",$content_style,$post_html);
					$post_html	= preg_replace("/\[inner_style\]/",$NewsFlash['inner_style'],$post_html);
					$post_html	= preg_replace("/\[content_title_style\]/",$NewsFlash['content_title_style'],$post_html);
	
					
					foreach($category[0] as $key	=> $value )
						{
							if (!isset($post->$key)) { $post->$key	= $value; }
						}
						
					foreach($post as $key	=> $value)
						{
							$post_html	= preg_replace("/\[".$key."\]/",$value,$post_html);
						}
					
					foreach($NewsFlash as $key	=> $value)
						{
							if (strlen($value) < 1) { continue; }
							$post_html	= preg_replace("/\[".$key."\]/",$post->$value,$post_html);
						}
						
					// debug
					$post_html	= preg_replace("/\[debug_all\]/",preg_replace("/\[|\]/",'',"\n<br>plugin"._pre($NewsFlash,1)."\n<br>post and category"._pre($post,1)),$post_html);
						
					// delete blank spaces.
					$post_html	= preg_replace("/\[\w+\]{1}/",'',$post_html);
					
					$post_ary[]	= $post_html;
				}
			return $post_ary;

		}
		
	public function simulate_get_the_content($post,$more_link_text = null)
		{
			if ( null === $more_link_text ) { $more_link_text = __( '(more...)' ); }
			
			$post->perma_link	= get_permalink($post->ID);
		
			if ( preg_match('/<!--more(.*?)?-->/', $post->post_content, $matches) )
				{
					list($post->post_content, $dump)	=	explode($matches[0], $post->post_content, 2);
					//$post->post_content		=	force_balance_tags($post->post_content);
					$post->post_content		.=	'<a href="' . $post->perma_link . ' class="more-link">'. $more_link_text .'</a>';
				}
				
			
				
			return $post;
		}
	
	
	
}